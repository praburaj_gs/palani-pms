  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <?php $meter_id =$this->uri->segment(3); ?>
  <input type="hidden" id="voltage_value" value="<?=$totVolt;?>">   
<input type="hidden" name="kva_value" id="kva_value" value="<?=$kvaVal;?>"> 
<input type="hidden" name="amp_value" id="amp_value" value="<?=$ampVal;?>"> 
<input type="hidden" name="fre_value" id="fre_value" value="<?=$freVal;?>">
<input type="hidden" name="meter_id" id="main_meter_id" value="<?=$meter_id;?>">
<input type="hidden" name="dg_Max" id="dg_Max" value="<?=$dgMax;?>">
<input type="hidden" name="dg_Load" id="dg_Load" value="<?=$dgLoad?>">
<input type="hidden" name="dgStatus" id="dgStatus" value="<?=$dgStatus;?>">
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">
          <?php 
          $CI =& get_instance();
          $meter =  $CI->iot_rest->getmeterName($meter_id); 
          echo $meter_name = $meter['data']; ?>
        </h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li>
          <div class="button-list">
            <div class="btn-group">
              <!--<div class="make-switch m-t-5 m-r-10">
                <input type="checkbox" checked="true" class="toggleDG" id="toggleDG" />
              </div>-->
                <!--<a href="javascript:void(0);" class="btn btn-sm btn-danger toggleDG" id="toggleDG" data-val="eb">Off DG</a>-->
				<?php if(has_accessable("eb_report")): ?>
                <a type="link" target="_blank" class="btn btn-sm btn-primary" href="<?php echo  site_url('reports/ebreport/'.$meter_id); ?>">EB Report</a>
                <?php endif; ?>
			    <?php if(has_accessable("dg_report")): ?>
 			    <a type="link" target="_blank" class="btn btn-sm btn-primary" href="<?php echo  site_url('reports/dgreport/'.$meter_id); ?>">DG Rport</a>
                <?php endif; ?>
				<?php if(has_accessable("dg_running")): ?>
				<a type="link" target="_blank" class="btn btn-sm btn-primary" href="<?php echo  site_url('dashboard/get_dg_running_hrs/'.$meter_id); ?>">DG Running</a>
                <?php endif; ?>
				<?php if(has_accessable("kwh_report")): ?>
				<a type="link" target="_blank" class="btn btn-sm btn-primary" href="<?php echo  site_url('reports/kwhreport/'.$meter_id); ?>">KWH Report</a>
               <?php endif; ?>
			</div>
        </div>            
          </li>
          <!--<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>-->
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid" id="sortable-area"> 
      <!-- Start Page Content -->
      <div class="row sortable-grid">
      <?php //print_r($result); exit();

      foreach ($result as $row) {
        //echo 'widget_'.$row->callBack;
          //call_user_func_array($this->iot_rest->'widget_'.$row->callBack,array());
          call_user_func_array(array($this->iot_rest, 'widget_'.$row->callBack), array($row->id,$meter_id));

        } ?>
      
      <!-- End PAge Content --> 
    </div>
    <div class="row">
      <?php 
         call_user_func_array(array($this->iot_rest, 'widget_consumption'),array('',$meter_id));
      ?>
      
    </div>

    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
<!-- Delete Modal -->
<div class="modal" id="livegraph-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Live Consumption</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        </div>
        <div class="modal-body">
        <div class="col-md-12 livegraph_consumption">
          <div class="card">
            <div class="card-content">

            <div class="graph-preloader">
              <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
              </svg>
            </div>
          <!--<div class="button-list">
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1d">1d</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="7d">7d</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1m">1m</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1y">1Y</button>
        </div>-->
              <div id="livechart" style="height: 400px"></div>
            </div>
          </div>
        </div>          
        </div>
        <div class="modal-footer" >
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
    </div>
</div>
</div>
<!-- DG ON/OFF status Modal -->
<div class="modal" id="switch-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Status</h5>
            
        </div>
        <div class="modal-body">
            <p>
               Oops, there is some problem with connecting the device, Please try again !
            </p>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="rid" id="delete_rid"/>
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('include/footer');	?>
