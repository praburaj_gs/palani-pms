  <!-- Page wrapper  -->
  <?php 
  $page_no = $this->uri->segment('4');
  ?>
  <input type="hidden" name="meter_id" id="main_meter_id" value="<?=$meter_id;?>">
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary"><?php echo $title; ?></h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $title; ?></li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="col-md-12">
									<div class="dt-buttons">
											<h4 class="card-title">Data Export</h4>
		                                	<h6 class="card-subtitle">Export data to CSV, Excel,Xlsx and PDF </h6>
											<?php 
												$params   = $_SERVER['QUERY_STRING'];
														if($params){
															$params = "&".$params;
														}
												$downloadURL = base_url()."reports/download/".$meter_id;

												if($title=="EB Reports"){
														
												?>
												<input type="hidden" name="dgstatus" id="dgstatus" value="0">
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=0&download=xls<?php echo $params;?>"><span>Excel</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=0&download=csv<?php echo $params;?>"><span>Csv</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=0&download=xlsx<?php echo $params;?>"><span>Xlsx</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=0&download=pdf<?php echo $params;?>"><span>PDF</span></a>
											<?php } else{ ?>
												<input type="hidden" name="dgstatus" id="dgstatus" value="1">
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank"aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=1&download=xls<?php echo $params;?>"><span>Excel</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank"aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=1&download=csv<?php echo $params;?>"><span>Csv</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank"aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=1&download=xlsx<?php echo $params;?>"><span>Xlsx</span></a>
												<a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank"aria-controls="report-table" href="<?php echo $downloadURL; ?>/?filter-type=1&download=pdf<?php echo $params;?>"><span>PDF</span></a>
											<?php }?>	
										</div>
									<?php if(chk_rst_cache() && $page_no==""){ 
										if($title=="EB Reports"){ ?>	  
									<a href="<?php echo base_url();?>reports/removecache?table=power-meter&type=eb&meter_id=<?php echo $meter_id; ?>" class="btn btn-danger m-b-10 m-l-5 pull-right" title="Go to live data" id="liverecord"><i class="fa fa-refresh" aria-hidden="true" id="liverecord"></i> 0 new records</a>
									<?php } else{ ?>
									<a href="<?php echo base_url();?>reports/removecache?table=power-meter&type=dg&meter_id=<?php echo $meter_id; ?>" class="btn btn-danger m-b-10 m-l-5 pull-right" title="Go to live data" id="liverecord"><i class="fa fa-refresh" aria-hidden="true" id="liverecord"></i> 0 new records</a>
									<?php } }?>
                                	<form name="filter-form" id="filter-form" 
                                	action="<?php 
                                	if($page_no){
                                		$currUrl = current_url();
                                		$url = explode('/', $currUrl);
										array_pop($url);
										echo implode('/', $url);
                                	} 
                                	else{
                                	echo current_url(); } ?>" class="form-inline p-t-20">
                   
											<div class="form-group m-r-10">
												<label class="control-label m-r-10">From Date</label>
												<input type="text" id="min-date" name="min_date" class="form-control date-picker calendar date-range-min " />
											</div>
											<div class="form-group m-r-10">
												<label class="control-label m-r-10">To Date</label>
												<input type="text" id="max-date" name="max_date" class="form-control date-picker calendar date-range-max"/>
											</div>
											<div class="form-group m-r-10">
												<label class="control-label m-r-10">Hr</label>
												<select class="form-control" name="hr">
													<option value="">Select</option>
													<?php for ($i=1; $i <= 24 ; $i++) { ?>
													<option value="<?=$i?>"><?=$i?></option>
												<?php } ?>
                                                </select>
											</div>											
											<input  type="submit" id="submit-filter" class="btn btn-primary" value="Submit">
											<input type="hidden" name="meter_id" value="<?=$meter_id?>">
											<?php if($title=="EB Reports"){?>
											<input type="hidden" name="filter-type" value="0">
											<?php } else{?>
											<input type="hidden" name="filter-type" value="1">
										<?php } ?>
									</form>		
								
									</div>                              	


								<div class="table-responsive m-t-40" id="table-responsive-report"> 

									<?php 
									//print_r($aggs); exit();
									if(!empty($aggs)){ ?>										
									 <table class="table table-striped table-bordered no-footer" role="grid"  id="agg-report-table" >
										<thead>
											<tr>
												<th>Date - Time</th>
												<th>Power Factor </th>
												<th>Frequency</th>
												<th>Total single Phase Voltage</th>
												<th>Total Three Phase Voltage</th>
												<th>TOTAL Current </th>
												<th>Total POWER (KW) </th>
												<th>KVA</th>
												<th>Kilowatt-hours</th>
											</tr>
										</thead>										
										<tbody>

										<?php //fb_pr($aggs); 
											foreach($aggs['result_set'] as $row){
												$inc = 0;
											foreach($row as $src){
												$inc++;
											$source = $src['_source'];
											if($inc==1 && $page_no==""){
												echo '<input type="hidden" name="createdtime" id="reportcreatedtime" value="'.$source["createdtime"].'">';
											}
											?>
                                            <tr>
												<td><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
												<td><?php echo $source["pf"]; ?></td>
												<td><?php echo $source["fre"]; ?></td>
                                                <td><?php echo $source["tv"]; ?></td>
												<td><?php echo $source["tpv"]; ?></td>
												<td><?php echo $source["tc"]; ?></td>
												<td><?php echo $source["tp"]; ?></td>
												<td><?php echo $source["kva"]; ?></td>
												<td><?php 
													if($source["kwh"])
													 echo $source["kwh"];
													else
													  echo "nil";
													?> 	
												</td>
                                            </tr>
											<?php } } ?>
										</tbody>
									 </table>

									<?php } else{ ?>
									 <table class="table table-striped table-bordered no-footer" role="grid" <?php if(!empty($aggs)){?> id="agg-report-table" <?php } else { ?>id="report-table"<?php } ?>>
										<thead>
											  <tr role="row">
												<th>Date - Time</th>
												<th>Power Factor </th>
												<th>Frequency</th>
												<th>Total single Phase Voltage</th>
												<th>Total Three Phase Voltage</th>
												<th>TOTAL Current </th>
												<th>Total POWER (KW) </th>
												<th>KVA</th>
												<th>Kilowatt-hours</th>
										   </tr>
										</thead>
										<tbody>

										<?php //fb_pr($aggs); 
											//$this->benchmark->mark('code_start');
												$inc =0;
												foreach($result_set as $key=>$row) {
													$inc++;
												$source = $row['_source'];
												if($inc==1 && $page_no==""){
												echo '<input type="hidden" name="createdtime" id="reportcreatedtime" value="'.$source["createdtime"].'">';
												}
											?>
                                            <tr>
												<td><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
												<td><?php echo $source["pf"]; ?></td>
												<td><?php echo $source["fre"]; ?></td>
                                                <td><?php echo $source["tv"]; ?></td>
												<td><?php echo $source["tpv"]; ?></td>
												<td><?php echo $source["tc"]; ?></td>
												<td><?php echo $source["tp"]; ?></td>
												<td><?php echo $source["kva"]; ?></td>
												<td><?php 
													if($source["kwh"])
													 echo $source["kwh"];
													else
													  echo "nil";
													?> 
												</td>
                                            </tr>
										   <?php }  
										   //$this->benchmark->mark('code_end');	?>
										</tbody>
									 </table>
									<?php } ?>
                                </div>
                            </div>

                           <div class="row">
						  <div class="col-sm-12 col-md-5">
							 <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
						  </div>
						  <div class="col-sm-12 col-md-7">
							 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
							    <?php echo $page_links; ?>

							 </div>
						  </div>
					   </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>

