  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->

<!-- Page wrapper  -->
<div class="page-wrapper">

 <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Access Denied</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active">Access Denied</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb -->
	
	<!-- Container fluid  -->
	<div class="container-fluid">
		
		<!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
					
					  <div class="row">
						  <div class="col-12">
							 <div class="card">
								 <div class="card-body">
									
									<div class="alert-1">
										<div class="alert alert-danger alert-dismissible fade show" role="alert">
											<strong>You don't have permission to access this page.</strong>
											<button type="button" class="close" data-dismiss="alert" aria-lSource Sans Pro="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										 </div>
									</div>
				  
									
								 </div>
							  </div>
						   </div>
					   </div>
					
					</div>
                 </div>
		
    </div>
  
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<?php $this->load->view('include/footer');	?>