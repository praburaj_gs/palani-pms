  <!-- Page wrapper  -->
  <?php
  $page_no = $this->uri->segment('4');
  ?>
  <input type="hidden" name="meter_id" id="main_meter_id" value="<?=$meter_id;?>">
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">DG Running Reports</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">DG Running Hours</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <div class="col-md-12">
                                 <div class="dt-buttons">
                                       <h4 class="card-title">Data Export</h4>
                                             <h6 class="card-subtitle">Export data to CSV, Excel,Xlsx and PDF </h6>
                                          <?php 
                                            $params   = $_SERVER['QUERY_STRING'];
                                            if($params){
                                               $params = "&".$params;
                                            }
                                            $meter_id = $this->uri->segment('3');
                                            $downloadURL = base_url()."dashboard/dghrs_download/".$meter_id;
                                          ?>
                                          <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" data-type="xls" href="<?php echo $downloadURL; ?>?download=xls<?php echo $params;?>"><span>Excel</span></a>
                                          <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" data-type="csv" href="<?php echo $downloadURL; ?>?download=csv<?php echo $params;?>"><span>Csv</span></a>
                                          <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" data-type="xlsx" href="<?php echo $downloadURL; ?>?download=xlsx<?php echo $params;?>"><span>Xlsx</span></a>
                                          <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" data-type="pdf" href="<?php echo $downloadURL; ?>?download=pdf<?php echo $params;?>"><span>PDF</span></a>
                                          <input type="hidden" name="downloadURL" id="downloadURL" value="<?php echo $downloadURL; ?>">
                                    </div>
                                    <?php if(chk_rst_cache()){ ?>
                                    <a href="<?php echo base_url();?>dashboard/removecache?table=genset_activity&meter_id=<?php echo $meter_id; ?>" class="btn btn-danger m-b-10 m-l-5 pull-right" title="Go to live data" id="liverecord"><i class="fa fa-refresh" aria-hidden="true"></i> 0 new records</a>
                                  <?php } ?>
                                 <form name="filter-form" id="filter-form" 
                                 action="<?php 
	                                 if($page_no){
	                                    $currUrl = current_url();
	                                    $url = explode('/', $currUrl);
	                                 array_pop($url);
	                                 echo implode('/', $url);
	                                 } 
	                                 else{
	                                 echo current_url(); }
                                  ?>" class="form-inline p-t-20">
                   
                                 <div class="form-group m-r-10">
                                    <label class="control-label m-r-10">From Date</label>
                                    <input type="text" id="min-date" name="min_date" class="form-control date-picker dg-calendar date-range-min " />
                                 </div>
                                 <div class="form-group m-r-10">
                                    <label class="control-label m-r-10">To Date</label>
                                    <input type="text" id="max-date" name="max_date" class="form-control date-picker dg-calendar date-range-max"/>
                                 </div>                               
                                 <input  type="submit" id="submit-filter" class="btn btn-primary" value="Submit">
        
	                           </form>     
	                           </div> 
								<div class="table-responsive m-t-40">
                                    <table id="dg-report-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
									<thead>
										<tr>
											 <?php echo $theader; ?>
										</tr>
									</thead>
            
                                        <tbody>
										<?php //fb_pr($aggs);
										$CI =& get_instance();
                    $meterList = $CI->iot_rest->getMeters();
										$meterNames = $meterList['data'];
										//print_r($result_set); exit();
										//$this->benchmark->mark('code_start');
                    $inc=0;
											foreach($result_set as $row){
                        $inc++;
											 	$source = $row['_source'];
                        if($inc==1 && $page_no==""){
                        echo '<input type="hidden" name="createdtime" id="dgcreatedtime" value="'.$source["last_on_time"].'">';
                      }
												if(!empty($source["last_off_time"])){
											?>
											<tr>
												<td><?php echo fb_convert_jsdate($source["last_on_time"]); ?></td>
												<td><?php echo fb_convert_jsdate($source["last_off_time"]); ?></td>
                                                <td><?php 
                                                		
														$meterId = $source["device_id"];
														echo $meter_name = $meterNames[$meterId];
														?></td>
												
												<td><?php 
														echo hourFormat($source["last_on_time"],$source["last_off_time"]);
												//echo gmdate('H:i:s',$diff_in_hrs); ?></td>
												<td><?php 
														//$CI =& get_instance();
														//$fuel =$CI->iot_rest->dgFuel($source["device_id"],$source["last_on_time"],$source["last_off_time"]);
														$fuel=$source['fuel'];
														echo round($fuel,3);
														  ?> </td>
                                            </tr>
    										<?php }  }
    										//$this->benchmark->mark('code_end');
    										?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
		                     <div class="row">
		                       <div class="col-sm-12 col-md-5">
		                         <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
		                       </div>
		                       <div class="col-sm-12 col-md-7">
		                         <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
		                            <?php echo $page_links; ?>
		                         </div>
		                       </div>
		                  </div>                             
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>
