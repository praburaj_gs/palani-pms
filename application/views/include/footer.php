    <!-- All Jquery -->
<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/jquery-ui/jquery.ui.touch-punch.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
<script src="<?php echo base_url();?>assets/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/moment/moment.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/underscore/underscore-min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/handlebar/handlebars.min.js"></script>

    <!--stickey kit -->
<script src="<?php echo base_url();?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datepicker/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/form-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/echart/echarts.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/highchart/highstock.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/highchart/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/d3/d3.v5.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/datetime-moment.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/odometer/jquery-countTo.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url();?>assets/js/liquidFillGauge.js"></script>
<script src="<?php echo base_url();?>assets/js/clock.js"></script>
<script src="<?php echo base_url();?>assets/js/energy.js"></script>
<script src="<?php echo base_url();?>assets/js/genset.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validation.js"></script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127035545-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127035545-1');
</script>

</body>

</html>