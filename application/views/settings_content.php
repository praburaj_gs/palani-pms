  <!-- header header  -->
  <?php $this->load->view('include/header_view');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
                <!-- Start Page Content -->
		<div class="row justify-content">
		
			<div class="col-md-12">
			  <div class="card" id="alertstatus-card">
				<div class="card-title">Alert Status</div>
				<div class="card-body">
				  
					<?php if($this->session->flashdata('alert_status_update_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('alert_status_update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('alert_status_update_failed')) { ?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('alert_status_update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>  
					
					
					<?php if(1): ?>
					
					<div class="table-responsive-sm">
					
						<table class="table table-bordered" role="grid" id="settings-table">
							<thead>
								<tr>
									<td>Alert name</td>
									<td>Actions</td>
								</tr>
							</thead>
							 
							<tbody>
								<?php foreach($alert_status_result_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								   ?>
								<tr>
								
									<th><?php echo $source["alert_name"]; ?></th>									
									<td>
									
										<?php if(1): ?>
										<?php 
										if($source["alert_flag"]=="true")
										{ 
										?>
										<a href="<?php echo base_url().'settings/updateAlertflag/'.$rkey.'/false';?>" title="De-activate" data-id="<?php echo $rkey; ?>"><i 
                            class="fa fa-eye"></i></a>&nbsp;
										<?php } else { ?>
										<a href="<?php echo base_url().'settings/updateAlertflag/'.$rkey.'/true';?>" title="Activate" data-id="<?php echo $rkey; ?>">
                            <i class="fa fa-eye-slash"></i></a>&nbsp;
										<?php } ?> 
										<?php endif; ?>
				
									
									</td>
								
								</tr>
								<?php endforeach; ?>
							</tbody>
							
								
							
						</table>
					</div>        
				    
					<?php endif; ?>
				  
				</div>
			  </div>
			</div>
		
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Notification Settings</div>
				<div class="card-body">
				  
				    <?php if( has_accessable('add_alert_settings') ): ?>
					<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#notify-card">Add New</button>
					<?php endif; ?>
					
					<?php if($this->session->flashdata('a_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('a_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					<?php if($this->session->flashdata('update_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('update_failed')) { ?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>  
					<?php if($this->session->flashdata('failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					
					<?php if( has_accessable('list_alert_settings') ): ?>
					
					<div class="table-responsive-sm">
					
						<table class="table table-bordered" role="grid" id="settings-table">
							<thead>
								<tr>
									<td>Name</td>
									<td>Phone No.</td>
									<td>Email</td>
									<td>Actions</td>
								</tr>
							</thead>
							 
							<tbody>
								<?php foreach($result_set1 as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
									  
									 
								   ?>
								<tr>
								
									<th><?php echo $source["name"]; ?></th>
									<td><?php echo $source["ph_no"]; ?></td>
									<td><?php echo $source["email"]; ?></td>
									<td>
									
									<a href="javascript:void(0);" data-id="<?=$rkey?>" title="Edit" class="edit-notify" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
									<a href="#" data-id="<?=$rkey?>" class="delete-modal" title="Delete"data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;
								
									</td>
								
								</tr>
								<?php endforeach; ?>
							</tbody>
							
								
							
						</table>
					</div>        
				    
					<?php endif; ?>
				  
				</div>
			  </div>
			</div>
				
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Settings - EB </div>
				<div class="card-body">
				  <form name="alert" id="alert-settings-form" method="post" action="<?php echo base_url('settings/create'); ?>">
					<?php if($this->session->flashdata('c_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('c_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('c_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					<?php foreach($result_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								
					?>
					<input type="hidden" name="ebalert_id" value="<?=$rkey?>">
					<div class="table-responsive-sm">
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rspmin" id="rspmin" value="<?php 
									if(empty($source['rspmin']))
									echo "0";
									else
									echo $source['rspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rspmax" id="rspmax" value="<?php 
									if(empty($source['rspmax']))
									echo "0";
									else
									echo $source['rspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="bspmin" id="bspmin" value="<?php 
									if(empty($source['bspmin']))
									echo "0";
									else
									echo $source['bspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bspmax" id="bspmax" value="<?php echo $source['bspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="yspmin" id="yspmin" value="<?php 
									if(empty($source['yspmin']))
									echo "0";
									else
									echo $source['yspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yspmax" id="yspmax" value="<?php 
									if(empty($source['yspmax']))
									echo "0";
									else
									echo $source['yspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="spmin" id="spmin" value="<?php 
									if(empty($source['spmin']))
									echo "0";
									else
									echo $source['spmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="spmax" id="spmax" value="<?php 
									if(empty($source['spmax']))
									echo "0";
									else
									echo $source['spmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rtpmin" id="rtpmin" value="<?php 
									if(empty($source['rtpmin']))
									echo "0";
									else
									echo $source['rtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rtpmax" id="rtpmax" value="<?php 
									if(empty($source['rtpmax']))
									echo "0";
									else
									echo $source['rtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="btpmin" id="btpmin" value="<?php 
									if(empty($source['btpmin']))
									echo "0";
									else
									echo $source['btpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="btpmax" id="btpmax" value="<?php 
									if(empty($source['btpmax']))
									echo "0";
									else
									echo $source['btpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="ytpmin" id="ytpmin" value="<?php 
									if(empty($source['ytpmin']))
									echo "0";
									else
									echo $source['ytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ytpmax" id="ytpmax" value="<?php 
									if(empty($source['ytpmax']))
									echo "0";
									else
									echo $source['ytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="tpmin" id="tpmin" value="<?php 
									if(empty($source['tpmin']))
									echo "0";
									else
									echo $source['tpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tpmax" id="tpmax" value="<?php 
									if(empty($source['tpmax']))
									echo "0";
									else
									echo $source['tpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="rcpmin" id="rcpmin" value="<?php 
									if(empty($source['rcpmin']))
									echo "0";
									else
									echo $source['rcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rcpmax" id="rcpmax" value="<?php 
									if(empty($source['rcpmax']))
									echo "0";
									else
									echo $source['rcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="bcpmin" id="bcpmin" value="<?php 
									if(empty($source['bcpmin']))
									echo "0";
									else
									echo $source['bcpmin'];
									?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bcpmax" id="bcpmax" value="<?php 
									if(empty($source['bcpmax']))
									echo "0";
									else
									echo $source['bcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="ycpmin" id="ycpmin" value="<?php 
									if(empty($source['ycpmin']))
									echo "0";
									else
									echo $source['ycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ycpmax" id="ycpmax" value="<?php 
									if(empty($source['ycpmax']))
									echo "0";
									else
									echo $source['ycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="tcpmin" id="tcpmin" value="<?php 
									if(empty($source['tcpmin']))
									echo "0";
									else
									echo $source['tcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tcpmax" id="tcpmax" value="<?php 
									if(empty($source['tcpmax']))
									echo "0";
									else
									echo $source['tcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="rppmin" id="rppmin" value="<?php 
									if(empty($source['rppmin']))
									echo "0";
									else
									echo $source['rppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rppmax" id="rppmax" value="<?php 
									if(empty($source['rppmax']))
									echo "0";
									else
									echo $source['rppmax'];?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="bppmin" id="bppmin" value="<?php 
									if(empty($source['bppmin']))
									echo "0";
									else
									echo $source['bppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bppmax" id="bppmax" value="<?php 
									if(empty($source['bppmax']))
									echo "0";
									else
									echo $source['bppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="yppmin" id="yppmin" value="<?php 
									if(empty($source['yppmin']))
									echo "0";
									else
									echo $source['yppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yppmax" id="yppmax" value="<?php 
									if(empty($source['yppmax']))
									echo "0";
									else
									echo $source['yppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="tppmin" id="tppmin" value="<?php 
									if(empty($source['tppmin']))
									echo "0";
									else
									echo $source['tppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tppmax" id="tppmax" value="<?php 
									if(empty($source['tppmax']))
									echo "0";
									else
									echo $source['tppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="fremin" id="fremin" value="<?php 
									if(empty($source['fremin']))
									echo "0";
									else
									echo $source['fremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="fremax" id="fremax" value="<?php 
									if(empty($source['fremax']))
									echo "0";
									else
									echo $source['fremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="pfmin" id="pfmin" value="<?php 
									if(empty($source['pfmin']))
									echo "0";
									else
									echo $source['pfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="pfmax" id="pfmax" value="<?php 
									if(empty($source['pfmax']))
									echo "0";
									else
									echo $source['pfmax']; ?>"></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="kvamin" id="kvamin" value="<?php 
									if(empty($source['kvamin']))
									echo "0";
									else
									echo $source['kvamin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="kvamax" id="kvamax" value="<?php 
									if(empty($source['kvamax']))
									echo "0";
									else
									echo $source['kvamax']; ?>"></td>
								</tr>								
							</tbody>
							
							<?php endforeach; ?>	
							
						</table>
					
						<button type="submit" class="btn btn-primary">Save</button>
						<button class="btn btn-secondary reset-btn">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>

			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Setings - Genset</div>
				<div class="card-body">
				  <form name="dg-alert" id="alert-settings-dg-form" method="post" action="<?php echo base_url('settings/create_genset'); ?>">
					<?php if($this->session->flashdata('c_dg_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_dg_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('dg_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('dg_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">Ã—</span></button>
					</div> 
					<?php } ?>   
					<?php 
					//print_r($dgresult_set);
					foreach($dgresult_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								
									  ?>
					<div class="table-responsive-sm">
					
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrspmin" id="dgrspmin" value="<?php 
									if(empty($source['dgrspmin']))
									echo "0";
									else
									echo $source['dgrspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrspmax" id="dgrspmax" value="<?php 
									if(empty($source['dgrspmax']))
									echo "0";
									else
									echo $source['dgrspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbspmin" id="dgbspmin" value="<?php  
									if(empty($source['dgbspmin']))
									echo "0";
									else
									echo $source['dgbspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbspmax" id="dgbspmax" value="<?php 
									if(empty($source['dgbspmax']))
									echo "0";
									else
									echo $source['dgbspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgyspmin" id="dgyspmin" value="<?php 
									if(empty($source['dgyspmin']))
									echo "0";
									else
									echo $source['dgyspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyspmax" id="dgyspmax" value="<?php 
									if(empty($source['dgyspmax']))
									echo "0";
									else
									echo $source['dgyspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgspmin" id="dgspmin" value="<?php 
									if(empty($source['dgspmin']))
									echo "0";
									else
									echo $source['dgspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgspmax" id="dgspmax" value="<?php echo $source['dgspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmin" id="dgrtpmin" value="<?php 
									if(empty($source['dgrtpmin']))
									echo "0";
									else
									echo $source['dgrtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmax" id="dgrtpmax" value="<?php echo $source['dgrtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmin" id="dgbtpmin" value="<?php 
									if(empty($source['dgbtpmin']))
									echo "0";
									else
									echo $source['dgbtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmax" id="dgbtpmax" value="<?php echo $source['dgbtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgytpmin" id="dgytpmin" value="<?php 
									if(empty($source['dgytpmin']))
									echo "0";
									else
									echo $source['dgytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgytpmax" id="dgytpmax" value="<?php 
									if(empty($source['dgytpmax']))
									echo "0";
									else
									echo $source['dgytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgtpmin" id="dgtpmin" value="<?php 
									if(empty($source['dgtpmin']))
									echo "0";
									else
									echo $source['dgtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtpmax" id="dgtpmax" value="<?php 
									if(empty($source['dgtpmax']))
									echo "0";
									else
									echo $source['dgtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmin" id="dgrcpmin" value="<?php 
									if(empty($source['dgrcpmin']))
									echo "0";
									else
									echo $source['dgrcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmax" id="dgrcpmax" value="<?php 
									if(empty($source['dgrcpmax']))
									echo "0";
									else
									echo $source['dgrcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmin" id="dgbcpmin" value="<?php 
									if(empty($source['dgbcpmin']))
									echo "0";
									else
									echo $source['dgbcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmax" id="dgbcpmax" value="<?php 
									if(empty($source['dgbcpmax']))
									echo "0";
									else
									echo $source['dgbcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgycpmin" id="dgycpmin" value="<?php 
									if(empty($source['dgycpmin']))
									echo "0";
									else
									echo $source['dgycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgycpmax" id="dgycpmax" value="<?php 
									if(empty($source['dgycpmax']))
									echo "0";
									else
									echo $source['dgycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmin" id="dgtcpmin" value="<?php 
									if(empty($source['dgtcpmin']))
									echo "0";
									else
									echo $source['dgtcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmax" id="dgtcpmax" value="<?php 
									if(empty($source['dgtcpmax']))
									echo "0";
									else
									echo $source['dgtcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgrppmin" id="dgrppmin" value="<?php 
									if(empty($source['dgrppmin']))
									echo "0";
									else
									echo $source['dgrppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrppmax" id="dgrppmax" value="<?php 
									if(empty($source['dgrppmax']))
									echo "0";
									else
									echo $source['dgrppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgbppmin" id="dgbppmin" value="<?php 
									if(empty($source['dgbppmin']))
									echo "0";
									else
									echo $source['dgbppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbppmax" id="dgbppmax" value="<?php 
									if(empty($source['dgbppmax']))
									echo "0";
									else
									echo $source['dgbppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgyppmin" id="dgyppmin" value="<?php 
									if(empty($source['dgyppmin']))
									echo "0";
									else
									echo $source['dgyppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyppmax" id="dgyppmax" value="<?php 
									if(empty($source['dgyppmax']))
									echo "0";
									else
									echo $source['dgyppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgtppmin" id="dgtppmin" value="<?php 
									if(empty($source['dgtppmin']))
									echo "0";
									else
									echo $source['dgtppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtppmax" id="dgtppmax" value="<?php 
									if(empty($source['dgtppmax']))
									echo "0";
									else
									echo $source['dgtppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="dgfremin" id="dgfremin" value="<?php 
									if(empty($source['dgfremin']))
									echo "0";
									else
									echo $source['dgfremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgfremax" id="dgfremax" value="<?php 
									if(empty($source['dgfremax']))
									echo "0";
									else
									echo $source['dgfremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="dgpfmin" id="dgpfmin" value="<?php 
									if(empty($source['dgpfmin']))
									echo "0";
									else
									echo $source['dgpfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgpfmax" id="dgpfmax" value="<?php 
									if(empty($source['dgpfmax']))
									echo "0";
									else
									echo $source['dgpfmax']; ?>"></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="dgkvamin" id="dgkvamin" value="<?php 
									if(empty($source['dgkvamin']))
									echo "0";
									else
									echo $source['dgkvamin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgkvamax" id="dgkvamax" value="<?php 
									if(empty($source['dgkvamax']))
									echo "0";
									else
									echo $source['dgkvamax']; ?>"></td>
								</tr>
							</tbody>
							
							<?php endforeach; ?>	
							
						</table>
					
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
		</div>
                <!-- End PAge Content -->
    </div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<div class="modal" id="notify-card" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form name="notify" id="notify-form" method="post" action="<?php echo base_url('settings/add_record');?>">
			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add New</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Name:</label>
						<input type="text" id="name" name="name"  class="form-control" placeholder="Name"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Phone Number:</label>
						(Ex: 987654321)
						<input type="text" id="ph_no" name="ph_no" class="form-control" placeholder="Phone Number"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Email:</label>
					<input type="text" id="email" name="email" class="form-control" placeholder="Email Id"/>
				  </div>
					</div>
					
				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Member</button>
					<button type="reset" class="btn btn-secondary clear">Clear</button>
				</div>
				
			</form>
		</div>
	</div>
</div>


<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are You Sure Want to Delete?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('settings/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view('include/footer');	?>
