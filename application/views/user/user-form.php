<!-- header header  -->
<?php $this->load->view('include/header');	?>
<!-- End header header --> 
<!-- Left Sidebar  -->
<?php $this->load->view('include/left-sidebar');	?>
<!-- End Left Sidebar  --> 
<!-- Page wrapper  -->
<div class="page-wrapper"> 

	 <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Add User</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard"); ?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php echo site_url("/user/manage_users"); ?>">Manage User</a></li>
		  <li class="breadcrumb-item active">Add Users</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb -->
	
	<!-- Container fluid  -->
	<div class="container-fluid">
		
		<?php if(isset($error_msg) && !empty($error_msg)): ?>
		  <div class="row">
			<div class="alert alert-danger col-12" role="alert">
			  <?php echo $error_msg; ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<!-- Start Page Content -->
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo current_url(); ?>" method="post">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="firstname">First name <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" autocomplete="off" class="form-control" id="firstname" name="firstname" 
											value="<?php echo set_value('firstname'); ?>" placeholder="Enter a Firstname.." />
												<?php echo form_error('firstname', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="lastname">Last name</label>
                                            <div class="col-lg-6">
                                                <input type="text" autocomplete="off" class="form-control" id="lastname" name="lastname" 
										  value="<?php echo set_value('lastname'); ?>" placeholder="Enter a Lastname.." />
												<?php echo form_error('lastname', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="email">Email <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" autocomplete="off" class="form-control" id="email" name="email" 
											value="<?php echo set_value('email'); ?>" placeholder="Your valid email.." />
												<?php echo form_error('email', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="password">Password <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Choose a safe one..">
												<?php echo form_error('password', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="passconf">Confirm Password <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="password" autocomplete="off" autocomplete="off"class="form-control" id="passconf" name="passconf" placeholder="..and confirm it!">
												<?php echo form_error('passconf', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="mobile">Mobile</label>
                                            <div class="col-lg-6">
                                                <input type="text" autocomplete="off" class="form-control" id="mobile" name="mobile" 
											value="<?php echo set_value('mobile'); ?>" placeholder="Enter a mobile no" />
												<small>Ex : 9123456780</small>
												<?php echo form_error('mobile', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="user_role">User Role</label>
                                            <div class="col-lg-6">
                                        <?php $ropt = array("id" => "user_role", "class" => "form-control"); ?>
										<?php echo form_dropdown('user_role', $role_copt, set_value('user_role'), $ropt ); ?>
												
												<?php echo form_error('user_role', '<p class="text-danger">', '</p>'); ?>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
		
	</div>

 <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

  
<?php $this->load->view('include/footer');	?>