<!-- header header  -->
<?php $this->load->view('include/header_view');	?>
<!-- End header header --> 
<!-- Left Sidebar  -->
<?php $this->load->view('include/left-sidebar');	?>
<!-- End Left Sidebar  --> 
<!-- Page wrapper  -->


<!-- Page wrapper  -->
<div class="page-wrapper">

 <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Manage Permissions</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard"); ?>">Home</a></li>
          <li class="breadcrumb-item active">Manage Permissions</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb -->

	<!-- Container fluid  -->
	<div class="container-fluid">
		<!-- Start Page Content -->
		
		
		<?php if($this->session->flashdata("success_msg")): ?>
		  <div class="row">
			<div class="alert alert-success text-white col-12" role="alert">
			  <?php echo $this->session->flashdata("success_msg"); ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<?php if(isset($success_msg) && $success_msg != ""): ?>
		  <div class="row">
			<div class="alert alert-success text-white col-12" role="alert">
			  <?php echo $success_msg; ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<?php if($this->session->flashdata("error_msg")): ?>
		  <div class="row">
			<div class="alert alert-danger text-white col-12" role="alert">
			  <?php echo $this->session->flashdata("error_msg"); ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                         <div class="form-validation">
							        
									<form class="form-valide" action="" method="post">
									
									 <div class="row">
										  <div class="col-12">
											 <div class="card">
												 <div class="card-body">
													 <div class="text-right">
														<button type="submit" class="btn radius btn-primary" name="save">Save Changes</button>
														<input type="hidden" name="mng_per_page" value="manage_permission">
													  </div>
												 </div>
											  </div>
										   </div>
									   </div>
									
										  <?php foreach($modules_list as $mrow): 
										          $mname = $mrow["module_name"]; $mtxt = $mrow["module_text"];
										    ?>
										    <div class="row">
												<div class="col-12">
													<div class="card">
													    <div class="card-title"> <h4><?php echo $mtxt; ?></h4> </div>
														<div class="card-body">
														    <div class="table-responsive">
																<table class="table">
																	<thead>
																		<tr>
																			<th>#</th>
																			<?php foreach($pgroups as $pgroup): ?>
																			  <th scope="col"><?php echo $pgroup["role_text"]; ?></th>
																			<?php endforeach; ?>
																		</tr>
																	</thead>
																	<tbody>
																	  
																	     <?php $pcnt = 1; ?>
																	   <?php foreach($amplist[$mname] as $amprow): ?>
																	   <tr>
																	        <td><?php echo $pcnt++; ?></td>
																	      <?php foreach($pgroups as $pgroup1): 		  										   
							$rm = $pgroup1["role_name"];
							$pn = $amprow["pname"];
							$inpid = $rm.'-'.$pn.'-'.$pcnt;
							$ckd = isset($acpm[$rm][$pn])? ' checked' : '';
							$inp = "<input id=\"$inpid\" class=\"form-check-input mt-2\" type=\"checkbox\" name=\"upm[$rm][$pn][]\" value=\"1\" $ckd />";
																		  
																		  
																		  ?>
																			 <td>
																			  <div class="form-check">
																			  <?php echo $inp; ?>
									<label class="form-check-label" for="<?php echo $inpid; ?>" > <?php echo $amprow["ptext"]; ?></label>
																			   </div>
																			 </td>
																		  <?php endforeach; ?>
																		  </tr>
																	   <?php endforeach; ?>
																	  
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										  <?php endforeach; ?>
										  
											   <div class="row">
												  <div class="col-12">
													 <div class="card">
													     <div class="card-body">
														     <div class="text-right">
																<button type="submit" class="btn radius btn-primary" name="save">Save Changes</button>
															  </div>
														 </div>
													  </div>
												   </div>
											   </div>
									</form>
							</div>
                        </div>
                 </div>
                <!-- End PAge Content -->
									
    </div>

 <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

  
<?php $this->load->view('include/footer');	?>