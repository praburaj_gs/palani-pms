  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Manage DG</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Manage DG</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <?php
       if($this->session->flashdata('delete_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Meter has been deleted succesfully
      </div>
      <?php } if ($this->session->flashdata('delete_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>
      <?php if($this->session->flashdata('update_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> <?php echo $this->session->flashdata('update_success'); ?>
      </div>
      <?php } if ($this->session->flashdata('update_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> <?php echo $this->session->flashdata('update_failed'); ?>
      </div>
      <?php }    ?> 
      <?php if($this->session->flashdata('meter_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Meter has been added succesfully
      </div>
      <?php } if ($this->session->flashdata('meter_failed')) { ?>
        <div class="alert alert-danger alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>      
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                                              
                                <!--<h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>-->

                                <div class="">
			<?php if(has_accessable("add_dg") ): ?>
                <button type="button" class="btn btn-primary btn-sm m-r-20 pull-right btn-add-meter" data-toggle="modal" data-target="#meter-modal">Add DG</button>
		   <?php endif; ?>				
                <div class="table-responsive m-t-40">
                
                                    <table id="meters-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td>DG ID</td>
                    <td>Name</td>
                    <td>KVA</td>
                    <td>Running Hours</td>
                    <td>Description</td>
                    <td>Image</td>
                    <td>Status </td>
                    <td>Action</td>
                  </tr>
                </thead>

                  <tbody>
                    <?php //fb_pr($presult_data);
                    if(count($presult_data)>0){
                    foreach($presult_data as $row){
                      $source = $row['_source'];
                      $source = $row['_source'];
                      $rkey = $row['_id'];
                      $status= $source["status"]
                      ?>
                                            <tr>
                        <td><?php echo $source["meter_id"]; ?></td>
                        <td><?php echo $source["name"]; ?></td>
                        <td><?php echo $source["kva"]; ?></td>
                        <td><?php 
                        if($source['running_hrs'])
                          echo $source["running_hrs"];
                        else{
                          echo "0";
                        } ?></td>
                        <td><?php echo $source["description"]; ?></td>
                        <td><img height="100px" width="100px" src="<?php echo base_url().'/meter-images/'.$source["image"]; ?>"></td>
                        <td><?php 
                        if($source["status"]=="true")
                        {
                          echo "Active";
                        }else{
                          echo "In active";
                        } ?></td>
                        <td>
						<?php if(has_accessable("status_dg")): ?>
                        <?php 
                        if($source["status"]=="true")
                        { 
                        ?>
                          <a href="<?php echo base_url().'meters/updateStatus/'.$rkey.'/false';?>" title="De-activate" data-id="<?php echo $rkey; ?>"><i 
                            class="fa fa-eye"></i></a>&nbsp;
                        <?php } else { ?>
                          <a href="<?php echo base_url().'meters/updateStatus/'.$rkey.'/true';?>" title="Activate" data-id="<?php echo $rkey; ?>">
                            <i class="fa fa-eye-slash"
                            ></i></a>&nbsp;
                          <?php } ?> 
						  <?php endif; ?>
<?php if(has_accessable("delete_dg") ): ?>						  
                            <a href="#" data-id="<?php echo $rkey; ?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal" title="Delete"><i class="fa fa-trash"></i></a>&nbsp;
<?php endif; ?>							
<?php if(has_accessable("edit_dg") ): ?>
                            <a href="#" data-id="<?php echo $rkey; ?>" class="edit-meter" data-toggle="modal" data-target="#edit-modal" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
<?php endif; ?>
<?php if(has_accessable("b_check") ): ?>
<a href="#" data-id="<?php echo $rkey; ?>" data-meter="<?php echo $source["meter_id"];?>" data-toggle="modal" data-target="#bcheck-modal" class="btn btn-sm btn-primary bcheck" title="Set B-Check">B-check</a>
<?php endif; ?>
<?php if(has_accessable("fuel_settings") ): ?>
               <a href="#" data-id="<?php echo $rkey; ?>" data-meter="<?php echo $source["meter_id"];?>" data-toggle="modal" data-target="#fuel-modal" class="btn btn-sm btn-danger fuel-setting" title="Set Fuel settings">Fuel settings</a>
<?php endif; ?>
                        </td>
                                            </tr>
                    <?php  } } else{?>
                      <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>No Record found</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      </tr>
                                         <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
<!-- Delete Modal -->
<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            
        </div>
        <div class="modal-body">
            <p>
               <?php echo "Are you sure to delete this? "; ?>
            </p>
        </div>
        <div class="modal-footer" data-id="<?php echo $rkey; ?>">
          <form method="post"  action="<?php echo base_url('meters/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>

<!--Meter Form modal-->
<div class="modal" id="meter-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form name="device" id="meter-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('meters/add_meter');?>">
      
        <div class="modal-header">
          <h5 class="modal-title" id="deviceLabel"><b>Add DG</b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
          
        </div>
        <div class="modal-body">
        <div class="modal-loader">
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>DG Id:</label>
            <input type="text" id="meter_id" name="meter_id"  class="form-control" placeholder="first letter of DG and unique number"/>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>DG Name:</label>
            <input type="text" id="meter_name" name="meter_name" class="form-control" placeholder="Name of the DG"/>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>KVA:</label>
            <input type="text" id="meter_kva" name="kva" class="form-control" placeholder="KVA of the DG"/>
          </div>
          </div> 
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Current Running Hours:</label>
            <input type="text" id="meter_running_hrs" name="running_hrs" class="form-control" placeholder="Current Running Hours"/>
          </div>
          </div>        
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Description:</label>  
          <input type="text" id="meter_desc" name="meter_desc" class="form-control" placeholder="Description">        
          
          </div>
          </div>          
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Image:</label>
          <input type="file" name="meter_image" id="meter_imagefile" class="form-control">
          <div id="meter_image"></div>
          </div>
          </div>
          
        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary title-btn">Add DG</button>
          <button type="button" class="btn btn-secondary clear">Clear</button>
        </div>
      </form>
      </div>

  </div>
</div>

<!-- Delete Modal -->
<div class="modal" id="bcheck-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-bg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">B-Check</h5>
            
        </div>
        <form name="bcheck-form" id="bcheck-form" action="#" class="">
        <div class="graph-preloader" style="height: 200px; position: relative;top: 10px; display: none;">
            <svg class="circular" viewBox="25 25 50 50">
              <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
           </svg>
        </div>          
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 alert alert-danger" id="bcheck-error" style="display: none;">
            Oops, there is something wrong, try again!
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Next Service Date</label>
              <input type="text" id="next_date" name="next_date" autocomplete="off" class="form-control date-picker " />
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Next Service Hours</label>
              <input type="text" id="next_hours" name="next_hours" autocomplete="off" class="form-control date-picker"/>
            </div>
          </div> 
          <!--<div class="col-md-12">
            <div class="form-group m-t-35">
              <select class="form-control" name="bcheck_action">
                <option value="add">Add</option>
                <option value="update">Update</option>
              </select>
            </div>
          </div>-->
              
        </div>
           
        </div>
          <div class="modal-footer">
            <div class="form-group">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input  type="submit" id="submit-filter" class="btn btn-primary" value="Submit">
               <input type="hidden" name="meter_id" id="bcheck_meterid">
            </div>
          </div>  
      </form>        
    </div>
</div>
</div>

<!-- Fuel modal -->

<div class="modal" id="fuel-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Genset Fuel settings(Approximate)</h5>
            
        </div>
      <form name="fuelsettings-form" id="fuelsettings-form" action="#" class=""> 
        <div class="graph-preloader" style="height: 200px; position: relative;top: 10px; display: none;">
            <svg class="circular" viewBox="25 25 50 50">
              <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
           </svg>
        </div>      
   
        <div class="modal-body">
            <div class="sufee-alert alert with-close alert-success alert-dismissible hide" id="fulesuccess">
          Fuel settings updated!
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div> 
           <div class="sufee-alert alert with-close alert-danger alert-dismissible  hide" id="fuelerror">
          Oops! there is something wrong, please try again
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">0-25% Load (litres/hr)</label>
                  <input type="text" id="qtr_ltr" name="qtr_ltr" class="form-control " value=""/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">25-50% Load (litres/hr)</label>
                  <input type="text" id="half_ltr" name="half_ltr" class="form-control " value="" />
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">50-75% Load (litres/hr)</label>
                  <input type="text" id="threefour_ltr" name="threefour_ltr" class="form-control" value=""/>
                </div>
              </div>  


              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">75-100% Load (litres/hr)</label>
                  <input type="text" id="full_ltr" name="full_ltr" class="form-control" value=""/>
                </div>
              </div>  
              </div>
              <div class="row">  
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">100-110% Load (litres/hr)</label>
                  <input type="text" id="fullover_ltr" name="fullover_ltr" class="form-control" value=""/>
                </div>
              </div>                       
           </div>
           
        </div>
          <div class="modal-footer">
            <div class="form-group">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input  type="submit" id="submit-filter" class="btn btn-primary" value="Submit">
               <input type="hidden" name="updateId" id="fuel_rid" value="">
               <input type="hidden" name="fuel_meterid" id="fuel_meterid" value="">
            </div>
          </div>  
      </form>        
    </div>
</div>
</div>
</div>
