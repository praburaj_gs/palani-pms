  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Genset</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Genset</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->

        <div class="row">
        <!-- /# column -->
        <div class="col-lg-9">
          <div class="card">
            <div class="card-title">
              Load
            </div>
            <div class="card-content text-center">
              <div class="meter progress progress-striped active">
                <div class="meter__text">0%</div>
                <div class="meter__range progress-bar bg-success wow animated progress-animated" style="width: 0%"></div>
              </div>
                
            </div>
          </div>
        </div>
            <div class="col-lg-3">
                <div class="card">
            <div class="card-title">
              Status - 100 W
            </div>
            <div class="card-content text-center">
                <div class="generator-list">
                    <div class="generator">
                      <div class="generator__box">
                        <i class="fa fa-bolt"></i>
                      </div>
                      <?php if(count($result_set)==0){ ?>
                      <button class="generator__switch switch" data-volt="100" data-threshold="90">OFF</button>
                    <?php } else {
                      ?>
                      <button class="generator__switch switch" data-volt="100" data-threshold="90">ON</button>
                    <?php } ?>
                    </div>
                  </div>
                </div>
                </div>
            </div>
      </div> 
    
        <div class="row bulb-list">
                        <?php 

                        foreach($result_set as $row): 
                          $source = $row["_source"];
                          $rkey = $row["_id"];                        
                          $d_id = $source["device_id"]; 
                          $state = $source["status"];
                          $watts = $source["watts"];
                          $device_type = $source['device_type'];
                          $CI =& get_instance();
                          $CI->config->load('fb_boodskap', TRUE);
                          $devicesArr = $CI->config->item('devicesArr', 'fb_boodskap');
                          $iconImg= $devicesArr[$device_type]['icon']; 
                       ?>                    
				        <div class="col-md-4">
                          <div class="card">
                            <div class="card-title">
                              <?php echo $source["device_name"]; ?>
                                
                            </div>
                            <div class="card-content text-center">
                            <div class="bulb">
                                <?php if($state==1) { ?>
                                <img class="active_device" alt="<?php $source["device_name"]; ?>" src="<?php 
                                $iconImg = str_replace("idle", "on", $iconImg);
                                echo base_url()."assets/images/icons/".$iconImg;?>">
                                <?php } else{?>
                              <img class="active_device" alt="<?php $source["device_name"]; ?>" src="<?php echo base_url()."assets/images/icons/".$iconImg;?>">
                                <?php } ?>
                              <div class="bulb__consumption"><?php echo round($watts,2); ?> KW</div>
                                <button class="bulb__switch switch" data-volt="<?php echo round($watts,2); ?>">OFF</button>
                            <?php
                                    /* if($state == "true"){
                                        echo "<label class='switch'>";
                                        echo "<input type='checkbox' data-id='$rkey' data-device_id='$d_id' class='status-switch' checked>";
                                        echo "<span class='slider round'></span>";
                                        echo "</label>";
                                    }else{
                                        echo "<label class='switch'>";
                                        echo "<input type='checkbox' data-id='$rkey' data-device_id='$d_id' class='status-switch'>";
                                        echo "<span class='slider round'></span>";
                                        echo "</label>";
                                    } */
                                    ?> 
                                
                            </div>              
                            </div>
                              <div class="button-list">
                                  <!--<a href="<?php echo base_url()."devices/details/".$d_id?>" class="btn btn-info btn-xs m-b-10 m-l-5">Details</a>-->
                                  <button data-id="<?= $rkey?>" data-name="<?=$source["device_name"]; ?>" data-watts="<?=$watts; ?>" class="btn btn-danger btn-xs m-b-10 m-l-5 pull-right remove-genset">Delete</button>
                              </div>
                          </div>
                        </div>
                    <?php endforeach; ?>                   
        </div>
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
<!-- End Wrapper -->
