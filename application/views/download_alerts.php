  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary"><?php echo $title; ?></h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $title; ?></li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div> 
            		<?php 
            			$download_file = $this->My_download->report_processDownload($result_set,$format,$min_date,$max_date);
            			$downloadfilename = $download_file.".".$format;
            		?>
            		<div class="alert alert-dark">
                    <h4 class="alert-heading">Well done!</h4>
                    Your file is ready to download
                    <hr>
                    <a href="<?php echo base_url()."reports_upload/".$downloadfilename; ?>">Click Here to download</a>
                </div> 
        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>
