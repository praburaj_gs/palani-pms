  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Manage Notes/Remainder</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Manage Notes/Remainder</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <?php
       if($this->session->flashdata('delete_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Notes/Remainder has been deleted succesfully
      </div>
      <?php } if ($this->session->flashdata('delete_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>
      <?php if($this->session->flashdata('update_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> <?php echo $this->session->flashdata('update_success'); ?>
      </div>
      <?php } if ($this->session->flashdata('update_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> <?php echo $this->session->flashdata('update_failed'); ?>
      </div>
      <?php }    ?> 
      <?php if($this->session->flashdata('notes_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Notes/Remainder has been added/updated succesfully
      </div>
      <?php } if ($this->session->flashdata('notes_failed')) { ?>
        <div class="alert alert-danger alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>      
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                                              
                                <!--<h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>-->

                                <div class="">
			<?php if(has_accessable("add_notes") ): ?>
                <button type="button" class="btn btn-primary btn-sm m-r-20 pull-right btn-add-notes" data-toggle="modal" style="margin-bottom:10px;" data-target="#notes-modal">Add Notes/Remainder</button>
		   <?php endif; ?>				
                <div class="table-responsive m-t-40">
                
                <table id="notes-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <td>Title</td>
                    <td>Description</td>
                    <td>Date and time</td>
					<td>Notification mail</td>
                    <td>Status </td>
					<td>Mail send status </td>
                    <td>Action</td>
                  </tr>
                </thead>
				
				
                  <tbody>
                    <?php //fb_pr($presult_data);
                    if(count($presult_data)>0){
                    foreach($presult_data as $row){
                      $source = $row['_source'];
                      $source = $row['_source'];
                      $rkey = $row['_id'];
                      $status= $source["status"]
                      ?>
                    <tr>
                        <td><?php echo $source["notes_title"]; ?></td>
                        <td><?php echo nl2br($source["notes_description"]); ?></td>
                        <td><?php echo fb_convert_date_time_format($source["date"]); ?></td>
                        <td><?php echo $source["email"]; ?></td>
						<td><?php 
                        if($source["status"]=="true")
                        {
                          echo "Active";
                        }else{
                          echo "In active";
                        } ?></td>
						<td><?php echo $source["send_status"]; ?></td>
						<td>
						<?php if(has_accessable("status_dg")): ?>
                        <?php 
                        if($source["status"]=="true")
                        { 
                        ?>
                          <a href="<?php echo base_url().'notes/updateStatus/'.$rkey.'/false';?>" title="De-activate" data-id="<?php echo $rkey; ?>"><i 
                            class="fa fa-eye"></i></a>&nbsp;
                        <?php } else { ?>
                          <a href="<?php echo base_url().'notes/updateStatus/'.$rkey.'/true';?>" title="Activate" data-id="<?php echo $rkey; ?>">
                            <i class="fa fa-eye-slash"
                            ></i></a>&nbsp;
                          <?php } ?> 
						  <?php endif; ?>
						  
						  <?php if(1): ?>						  
                            <a href="#" data-id="<?php echo $rkey; ?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal" title="Delete"><i class="fa fa-trash"></i></a>&nbsp;
						  <?php endif; ?>							
						  <?php if(1): ?>
                            <a href="#" data-id="<?php echo $rkey; ?>" class="edit-notes" data-toggle="modal" data-target="#edit-modal" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
						  <?php endif; ?>

						</td>
                    </tr>
                    <?php  } } else{?>
                      <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>No Record found</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      </tr>
                                         <?php } ?>
                                        </tbody>

                  
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
<!-- Delete Modal -->
<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            
        </div>
        <div class="modal-body">
            <p>
               <?php echo "Are you sure to delete this? "; ?>
            </p>
        </div>
        <div class="modal-footer" data-id="<?php echo $rkey; ?>">
          <form method="post"  action="<?php echo base_url('notes/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>

<!--Notes Form modal-->
<div class="modal" id="notes-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form name="device" id="notes-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('notes/add_notes');?>">
      
        <div class="modal-header">
          <h5 class="modal-title" id="deviceLabel"><b>Add Notes/Remainder</b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
          
        </div>
        <div class="modal-body">
        <div class="modal-loader">
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Title:</label>
            <input type="text" id="notes_title" name="notes_title"  class="form-control" placeholder="Title for the notes/remainder"/>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Description:</label>
            <!--<input type="text" id="notes_description" name="notes_description" class="form-control"  style="height:100px; width:435px;" placeholder="Description"/>-->
			<textarea id="notes_description"  name="notes_description" class="form-control" style="height:100px;"  placeholder="Description"></textarea>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Remainder date/time:</label>
            <input type="text" id="notes_date" name="notes_date" autocomplete="off" class="form-control date-picker " />
          </div>
          </div> 
		  	  
		  
        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary title-btn">Add Notes/Remainder</button>
          <button type="button" class="btn btn-secondary clear">Clear</button>
        </div>
      </form>
      </div>

  </div>
</div>
</div>
