{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "__orderfld__": {
              "gte": "__lowv__",
              "lte": "__topv__",
              "boost": 2.0
            }
          }
        },
        {
          "match": {
            "device_id": __meter_id__
          }
        }
      ]
    }
  },
      "aggs": {
        "runtime": {
            "terms": {
                "script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay"
            },
            "aggs": {
                "tops": {
                  "top_hits": {
                    "size": 100000
                  }
                }
            }
        }

    }
}