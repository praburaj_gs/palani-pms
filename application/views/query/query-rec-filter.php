{
  "query": {
    "bool": {
      "must": [
      <?php if(!empty($min_date) && !empty($max_date) ): ?>
        {
          "range": {
            "__orderfld__": {
              "gte": "__min_date__",
              "lte": "__max_date__",
              "boost": 2.0
            }
          }
        },
      <?php endif; ?>
        {
          "match": {
            "gen": __val__
          }
        },
        {
          "match": {
            "meter_id": __meter_id__
          }
        }
      ]
    }
  },
  "size": __size__,
  "from": __from__,
  "sort": {
    "__orderfld__": {
      "order": "__orderdir__"
    }
  }
}