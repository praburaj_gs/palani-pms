{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "__orderfld__": {
              "gte": "__min_date__",
              "lte": "__max_date__",
              "boost": 2.0
            }
          }
        },
<?php if(!empty($minval) && (!empty($maxval)) ): ?>        
        {
          "range": {
            "kva": {
              "gte":__minval__,
              "lte": __maxval__,
              "boost": 2.0
            }
          }
        },  
<?php else: ?>  
        {
          "range": {
            "kva": {
              "gte":__minval__,
              "boost": 2.0
            }
          }
        }, 
<?php endif; ?>
        {
          "match": {
            "gen": "__val__"
          }
        },
        {
          "match": {
            "meter_id": __meter_id__
          }
        }
      ]
    }
  },
    "aggs": {
    "runtime": {
        "sum": {
            "field": "kva"
        }
    }

},
  "size": __size__,
  "from": __from__,
  "sort": {
    "__orderfld__": {
      "order": "__orderdir__"
    }
  }
}