{
	<?php if(!empty($min_date) && !empty($max_date) ): ?>
	"query": {
		 "range" : {
            "__orderfld__": {
              "gte": "__min_date__",
              "lte": "__max_date__",
              "boost": 2.0
            }
		 }
    }, 
	<?php else: ?>
	"query": { "match_all":{} },
	<?php endif; ?>	
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}