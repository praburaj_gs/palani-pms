<!-- Page wrapper  -->
<div class="page-wrapper">
   <!-- Bread crumb -->
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-primary">Reports</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
            <li class="breadcrumb-item active">Reports</li>
         </ol>
      </div>
   </div>
   <!-- End Bread crumb --> 
   <!-- Container fluid  -->
   <div class="container-fluid">
      <!-- Start Page Content -->
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                                <div class="col-md-12">
                           <div class="dt-buttons">
                                 <h4 class="card-title">Data Export</h4>
                                       <h6 class="card-subtitle">Export data to Copy, CSV, Excel and PDF </h6>
                                 <?php 
                            
                                          $params   = $_SERVER['QUERY_STRING'];
                                          if($params){
                                             $params = "&".$params;
                                          }
                                          $downloadURL = base_url()."alerts/download/";
                                    ?>
                                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>?download=xls<?php echo $params;?>"><span>Excel</span></a>
                                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>?download=csv<?php echo $params;?>"><span>Csv</span></a>
                                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>?download=xlsx<?php echo $params;?>"><span>Xlsx</span></a>
                                    <a class="dt-button buttons-excel buttons-html5" tabindex="0" target="_blank" aria-controls="report-table" href="<?php echo $downloadURL; ?>?download=pdf<?php echo $params;?>"><span>PDF</span></a>
                              </div>  
                                 <form name="filter-form" id="filter-form" 
                                 action="<?php 
                                 $page_no = $this->uri->segment('3');
                                 if($page_no){
                                    $currUrl = current_url();
                                    $url = explode('/', $currUrl);
                                 array_pop($url);
                                 echo implode('/', $url);
                                 } 
                                 else{
                                 echo current_url(); } ?>" class="form-inline p-t-20">
                   
                                 <div class="form-group m-r-10">
                                    <label class="control-label m-r-10">From Date</label>
                                    <input type="text" id="min-date" name="min_date" class="form-control date-picker calendar date-range-min " />
                                 </div>
                                 <div class="form-group m-r-10">
                                    <label class="control-label m-r-10">To Date</label>
                                    <input type="text" id="max-date" name="max_date" class="form-control date-picker calendar date-range-max"/>
                                 </div>                               
                                 <input  type="submit" id="submit-filter" class="btn btn-primary" value="Submit">
        
                           </form>     
                        
                           </div> 
                     <div class="table-responsive m-t-40">
                        <table id="alert-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                               <?php echo $theader; ?>
                              </tr>
                           </thead>
   
                           <tbody>
                              <?php //fb_pr($result_set);  exit();
                                 foreach($result_set as $row){
                                  
                                  $source = $row['_source'];
                                  $custom_pms = json_decode($source['custom_pms']);
                                  //print_r($custom_pms); exit();
                                  $meter_id = $custom_pms->meter_id;
                                  $rkey = $row['_id'];
                                  if($source['nstate'] == "true"){
                                  ?>
                              <tr role="row">
                                 <td bgcolor="#fff"><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
                                 <td bgcolor="#fff"><?php 
                                 $obj =& get_instance();
                                 $meter =  $obj->iot_rest->getmeterName($meter_id); 
                                 echo $meter_name = $meter['data'];
                                  ?></td>
                                 <td bgcolor="#fff"><?php echo $source["ntype"]; ?></td>
                                 <td bgcolor="#fff"><?php echo $source["nmsg"]; ?></td>
                                 <td bgcolor="#fff"><a href="#" data-id="<?php echo $rkey; ?>" class="view-modal" data-toggle="modal" data-target="#view-modal" title="View"><i class="fa fa-eye"></i></a>
                                    <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal" title="Delete"><i class="fa fa-trash"></i></a>
                                 </td>
                              </tr>
                              <?php }
                                 else{ ?>
                              <tr role="row">
                                 <td bgcolor="#f8f9fa"><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
                                 <td bgcolor="#f8f9fa"><?php echo $source["ntype"]; ?></td>
                                 <td bgcolor="#f8f9fa"><?php echo $source["nmsg"]; ?></td>
                                 <td bgcolor="#f8f9fa"><a href="#" data-id="<?php echo $rkey; ?>" class="view-modal" data-toggle="modal" data-target="#view-modal" title="View"><i class="fa fa-eye"></i></a>
                                    <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal" title="Delete"><i class="fa fa-trash"></i></a>
                                 </td>
                              </tr>
                              <?php } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
                     <div class="row">
                       <div class="col-sm-12 col-md-5">
                         <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
                       </div>
                       <div class="col-sm-12 col-md-7">
                         <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
                            <?php echo $page_links; ?>
                         </div>
                       </div>
                  </div>                  
               </div>
            </div>
            <!-- End PAge Content --> 
         </div>
         <!-- End Container fluid  --> 
         <!-- footer --> 
         <!-- End footer --> 
      </div>
      <!-- End Page wrapper  --> 
   </div>
   <!-- End Wrapper -->
   <!-- View Modal -->
   <div class="modal" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="alertmodel">Alert </h5>
            </div>
            <div class="modal-body">
               <p><label>Created Time:</label> &nbsp;&nbsp; <span id="timemodel"></span></p>
               <p><label>Meter:</label> &nbsp;&nbsp; <span id="metermodel"></span></p>
               <p><label>Alert Type:</label>&nbsp;&nbsp; <span id="idmodel"></span></p>
               <p><label>Alert Message:</label>&nbsp;&nbsp; <span id="alertmsgmodel"></span></p>
            </div>
            <div class="modal-footer">
               <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
   <!-- Delete Modal -->
   <div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            </div>
            <div class="modal-body">
               <p>
                  <?php echo "Are You Sure Want to delete ? "; ?>
               </p>
            </div>
            <div class="modal-footer" data-id="<?php echo $rkey; ?>">
               <form method="post"  action="<?php echo base_url('alerts/delete');?>">
                  <input type="hidden" name="rid" id="delete_rid"/>
                  <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">close</button>
                  <button type="submit" class="btn btn-primary">Confirm</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>