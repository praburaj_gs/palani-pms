<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface DownloadInterface {
    public function processDownload($data,$download,$meter_id,$filter_type,$minDate,$maxDate,$hr);
}