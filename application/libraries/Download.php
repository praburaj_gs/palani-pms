<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set("pcre.backtrack_limit", "500000000");
get_instance()->load->iface('DownloadInterface');


require_once(APPPATH .'third_party/phpspreadsheet/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\RichText\RichText;

error_reporting(0);

class Download implements DownloadInterface
{

	private  $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		ini_set('memory_limit','2048M');
	}
	


	public function processDownload($data,$download,$meter_id,$filter_type,$minDate,$maxDate,$hr){
		$meter =  $this->CI->iot_rest->getmeterName($meter_id); 
		$meter_name = $meter['data'];

		$spreadsheet = new Spreadsheet();
		$richText = new RichText();
		$curreDate = date("m/d/Y h:i:s a", time());
		//$spreadsheet->setActiveSheetIndex(0);
		if($filter_type==0)
			$heading = $meter_name." - EB Report";
		else
			$heading = $meter_name." - DG Report";	

		 $currUsername = $this->CI->session->userdata("first_name");
		 $totRecord = count($data);
		//$spreadsheet->getActiveSheet()->setCellValue('H1', $heading);
		//$spreadsheet->getActiveSheet()->setCellValue('H2', $curreDate);

		$spreadsheet->setActiveSheetIndex(0);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', 'Date')
		->setCellValue('B3', 'Power Factor')
		->setCellValue('C3', 'Frequency')
		->setCellValue('D3', 'Total Single Phase Voltage')
		 ->setCellValue('E3', 'Total Three Phase Voltage')
		  ->setCellValue('F3', 'Total Current')
		   ->setCellValue('G3', 'Total Power')
		   ->setCellValue('H3', 'KVA')
		    ->setCellValue('I3', 'KWH');


			$richText->createText('');

			$payable = $richText->createTextRun($heading."\n");
			$payable->getFont()->setBold(true);
			$payable->getFont()->setItalic(true);
			$payable->getFont()->setSize(15);
			$payable->getFont()->setColor(new Color(Color::COLOR_DARKRED));
			$richText->createText($curreDate."\n");
			$richText->createText("By: ".strtoupper($currUsername)."\n");
	

			if($minDate && $maxDate && $hr){
					$richText->createText("From Date: ".$minDate."\n");
					$richText->createText("To Date: ".$maxDate."\n");
					$richText->createText("Hours:" .$hr."\n");
				}else if($minDate && $maxDate){
					$richText->createText("From Date: ".$minDate."\n");
					$richText->createText("To Date: ".$maxDate."\n");
				}
					$richText->createText("Rows: ".$totRecord."\n");
			$spreadsheet->getActiveSheet()->getCell('G1')->setValue($richText);
			// Merge cells
			$spreadsheet->getActiveSheet()->mergeCells('G1:I1');

			$spreadsheet->getActiveSheet()->mergeCells('G2:I2');

		    	//set height for 1st row
		    	$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(120);
				// Protect cells
				//$spreadsheet->getActiveSheet()->getProtection()->setSheet(true); // Needs to be set to true in order to enable any worksheet protection!

				// Set column widths
				$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(30);

				// Set fonts
				$spreadsheet->getActiveSheet()->getStyle('B1')->getFont()->setName('Candara');
				$spreadsheet->getActiveSheet()->getStyle('B1')->getFont()->setSize(20);
				$spreadsheet->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
				$spreadsheet->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);

				$spreadsheet->getActiveSheet()->getStyle('D1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);
				$spreadsheet->getActiveSheet()->getStyle('E1')->getFont()->getColor()->setARGB(Color::COLOR_BLACK);

				// Set fills
				$spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('FFECECEC');

				$spreadsheet->getActiveSheet()->getStyle('A2:I2')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A2:I2')->getFill()->getStartColor()->setARGB('FFECECEC');				

				//remove row
				//$spreadsheet->getActiveSheet()->removeRow(2);

				//set logo
				$logoImg = FCPATH.'assets/images/fourbends_logo.png';
				$drawing = new Drawing();
				$drawing->setName('Logo');
				$drawing->setDescription('Logo');
				$drawing->setPath($logoImg);
				$drawing->setHeight(80);
				$drawing->setWorksheet($spreadsheet->getActiveSheet());

				// Set style for header row using alternative method
				$spreadsheet->getActiveSheet()->getStyle('A3:I3')->applyFromArray(
				    [
				            'font' => [
				                'bold' => true,
				            ],
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_RIGHT,
				            ],
				            'borders' => [
				                'top' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'bottom' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'right' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'left' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				            ],
				            'fill' => [
				                'fillType' => Fill::FILL_GRADIENT_LINEAR,
				                'rotation' => 90,
				                'startColor' => [
				                    'argb' => 'FFA0A0A0',
				                ],
				                'endColor' => [
				                    'argb' => 'FFFFFFFF',
				                ],
				            ],
				        ]
				);

				$spreadsheet->getActiveSheet()->getStyle('A3')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				            'borders' => [
				                'left' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				            ],
				        ]
				);


				$spreadsheet->getActiveSheet()->getStyle('G1')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::VERTICAL_JUSTIFY,
				            ],
				        ]
				);



				$spreadsheet->getActiveSheet()->getStyle('G1')->getAlignment()->setWrapText(true);
				// Set page orientation and size

				$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
				$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);


			//echo count($data['result_set']); exit();
			$spreadsheet->getActiveSheet()->fromArray($data,null,'A4',true);

			$writer = new Xlsx($spreadsheet);
			$helper = new Sample();
			
			$meter_name = strtolower($meter['data']);
			$meter_name = str_replace(' ', '_', $meter_name);

			if($filter_type==0)
			$filename = $meter_name.'_ebreport'.date('d_m_Y_h_i_a');
		else
			$filename = $meter_name.'_dgreport'.date('d_m_Y_h_i_a');

		$savepath = './reports_upload/';

		$success=false;

		if($download=="xls"){
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
			$result = $writer->save($savepath.$filename.".xls");
			$success = true;
		}else if($download=="pdf"){

			$spreadsheet->setActiveSheetIndex(0);
			$spreadsheet->getActiveSheet()->setShowGridLines(true);

			/*for($col = 'A'; $col !== 'G'; $col++) {
			    $spreadsheet->getActiveSheet()
			        ->getColumnDimension($col)
			        ->setAutoSize(true);
			}*/

			
			$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);

			$className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class;
			IOFactory::registerWriter('Pdf', $className);

			$writer = IOFactory::createWriter($spreadsheet, 'Pdf');
			$result = $writer->save($savepath.$filename.".pdf");

			$success = true;
		}else if($download=="csv"){
			$writer = IOFactory::createWriter($spreadsheet, 'Csv')->setDelimiter(',')
								   								 ->setEnclosure('"')
								   								 ->setSheetIndex(0);
			$writer->save($filename);	

			$reader = IOFactory::createReader('Csv')->setDelimiter(',')
														 ->setEnclosure('"')
														 ->setSheetIndex(0);
			$spreadsheetFromCSV = $reader->load($filename);
			$writerCSV = IOFactory::createWriter($spreadsheetFromCSV, 'Csv');
			$writerCSV->setExcelCompatibility(true);						 								   								 
			$result = $writer->save($savepath.$filename.".csv");
			$success = true;
		}else {

			$result = $writer->save($savepath.$filename.".xlsx"); 
			$success = true;
		}

		return $filename;

	}  


	public function report_processDownload($data,$download,$minDate,$maxDate){

		//$meter =  $this->CI->iot_rest->getmeterName($meter_id); 
		//$meter_name = $meter['data'];

		$spreadsheet = new Spreadsheet();
		$richText = new RichText();
		$curreDate = date("m/d/Y h:i:s a", time());
		//$spreadsheet->setActiveSheetIndex(0);
		//if($filter_type==0)
			//$heading = $meter_name." - Alerts Report";
		//else
			$heading = "Alerts Report";	

		 $currUsername = $this->CI->session->userdata("first_name");
		 $totRecord = count($data);
		//$spreadsheet->getActiveSheet()->setCellValue('H1', $heading);
		//$spreadsheet->getActiveSheet()->setCellValue('H2', $curreDate);

		$spreadsheet->setActiveSheetIndex(0);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', 'Date')
		->setCellValue('B3', 'Meter Name')
		->setCellValue('C3', 'Message');

			$richText->createText('');

			$payable = $richText->createTextRun($heading."\n");
			$payable->getFont()->setBold(true);
			$payable->getFont()->setItalic(true);
			$payable->getFont()->setSize(15);
			$payable->getFont()->setColor(new Color(Color::COLOR_DARKRED));
			$richText->createText($curreDate."\n");
			$richText->createText("By: ".strtoupper($currUsername)."\n");
	

			 if($minDate && $maxDate){
					$richText->createText("From Date: ".$minDate."\n");
					$richText->createText("To Date: ".$maxDate."\n");
				}
					$richText->createText("Rows: ".$totRecord."\n");
			$spreadsheet->getActiveSheet()->getCell('C1')->setValue($richText);

				if($download=="xls"){
		    	//set height for 1st row
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(120);
		    	}else{
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(100);
		    	}
				// Protect cells
				//$spreadsheet->getActiveSheet()->getProtection()->setSheet(true); // Needs to be set to true in order to enable any worksheet protection!

				// Set column widths
				$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(100);

				// Set fills
				$spreadsheet->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('FFECECEC');

				$spreadsheet->getActiveSheet()->getStyle('A2:C2')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A2:C2')->getFill()->getStartColor()->setARGB('FFECECEC');				

				//remove row
				//$spreadsheet->getActiveSheet()->removeRow(2);

				//set logo
				$logoImg = FCPATH.'assets/images/fourbends_logo.png';
				$drawing = new Drawing();
				$drawing->setName('Logo');
				$drawing->setDescription('Logo');
				$drawing->setPath($logoImg);
				$drawing->setHeight(80);
				$drawing->setWorksheet($spreadsheet->getActiveSheet());

				// Set style for header row using alternative method
				$spreadsheet->getActiveSheet()->getStyle('A3:C3')->applyFromArray(
				    [
				            'font' => [
				                'bold' => true,
				            ],
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				            'borders' => [
				                'top' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'bottom' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'right' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'left' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				            ],
				            'fill' => [
				                'fillType' => Fill::FILL_GRADIENT_LINEAR,
				                'rotation' => 90,
				                'startColor' => [
				                    'argb' => 'FFA0A0A0',
				                ],
				                'endColor' => [
				                    'argb' => 'FFFFFFFF',
				                ],
				            ],
				        ]
				);


				$spreadsheet->getActiveSheet()->getStyle('C1')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_RIGHT,
				            ],
				        ]
				);


				$spreadsheet->getActiveSheet()->getStyle('C1')->getAlignment()->setWrapText(true);
				// Set page orientation and size

				$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
				$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);


			//echo count($data['result_set']); exit();
			$spreadsheet->getActiveSheet()->fromArray($data,null,'A4',true);

			$writer = new Xlsx($spreadsheet);
			$helper = new Sample();

			//if($filter_type==0)
			//$filename = $meter_name.'_ebreport'.date('d_m_Y_h_i_a');
		//else
			$filename = "Alert report-".date('d_m_Y_h_i_a');

		$savepath = './reports_upload/';

		$success=false;

		if($download=="xls"){
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
			$result = $writer->save($savepath.$filename.".xls");
			$success = true;
		}else if($download=="pdf"){

			$spreadsheet->setActiveSheetIndex(0);
			$spreadsheet->getActiveSheet()->setShowGridLines(true);

			/*for($col = 'A'; $col !== 'G'; $col++) {
			    $spreadsheet->getActiveSheet()
			        ->getColumnDimension($col)
			        ->setAutoSize(true);
			}*/

			
			$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);

			$className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class;
			IOFactory::registerWriter('Pdf', $className);

			$writer = IOFactory::createWriter($spreadsheet, 'Pdf');
			$result = $writer->save($savepath.$filename.".pdf");

			$success = true;
		}else if($download=="csv"){
			$writer = IOFactory::createWriter($spreadsheet, 'Csv')->setDelimiter(',')
								   								 ->setEnclosure('"')
								   								 ->setSheetIndex(0);
			$writer->save($filename);	

			$reader = IOFactory::createReader('Csv')->setDelimiter(',')
														 ->setEnclosure('"')
														 ->setSheetIndex(0);
			$spreadsheetFromCSV = $reader->load($filename);
			$writerCSV = IOFactory::createWriter($spreadsheetFromCSV, 'Csv');
			$writerCSV->setExcelCompatibility(true);						 								   								 
			$result = $writer->save($savepath.$filename.".csv");
			$success = true;
		}else {

			$result = $writer->save($savepath.$filename.".xlsx"); 
			$success = true;
		}

		return $filename;

	} 

public function dgrunning_processDownload($meter_id,$data,$download,$minDate,$maxDate){

		$meter =  $this->CI->iot_rest->getmeterName($meter_id); 
		$meter_name = strtolower($meter['data']);
		$meter_name = str_replace(' ', '_', $meter_name);

		$spreadsheet = new Spreadsheet();
		$richText = new RichText();
		$curreDate = date("m/d/Y h:i:s a", time());

		$heading = "DG Running Hours";	

		 $currUsername = $this->CI->session->userdata("first_name");
		 $totRecord = count($data);
		//$spreadsheet->getActiveSheet()->setCellValue('H1', $heading);
		//$spreadsheet->getActiveSheet()->setCellValue('H2', $curreDate);

		$spreadsheet->setActiveSheetIndex(0);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', 'On Time')
		->setCellValue('B3', 'Off Time')
		->setCellValue('C3', 'Meter Name')
		->setCellValue('D3', 'Running Time')
		->setCellValue('E3', 'Fuel consumed in ltr');

			$richText->createText('');

			$payable = $richText->createTextRun($heading."\n");
			$payable->getFont()->setBold(true);
			$payable->getFont()->setItalic(true);
			$payable->getFont()->setSize(15);
			$payable->getFont()->setColor(new Color(Color::COLOR_DARKRED));
			$richText->createText($curreDate."\n");
			$richText->createText("By: ".strtoupper($currUsername)."\n");
	

			 if($minDate && $maxDate){
					$richText->createText("From Date: ".$minDate."\n");
					$richText->createText("To Date: ".$maxDate."\n");
				}
					$richText->createText("Rows: ".$totRecord."\n");
			$spreadsheet->getActiveSheet()->getCell('E1')->setValue($richText);

				if($download=="xls"){
		    	//set height for 1st row
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(120);
		    	}else{
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(100);
		    	}
				// Protect cells
				//$spreadsheet->getActiveSheet()->getProtection()->setSheet(true); // Needs to be set to true in order to enable any worksheet protection!

				// Set column widths
				$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(35);
				$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(45);

				// Set fills
				$spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FFECECEC');

				$spreadsheet->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('FFECECEC');				

				//remove row
				//$spreadsheet->getActiveSheet()->removeRow(2);

				//set logo
				$logoImg = FCPATH.'assets/images/fourbends_logo.png';
				$drawing = new Drawing();
				$drawing->setName('Logo');
				$drawing->setDescription('Logo');
				$drawing->setPath($logoImg);
				$drawing->setHeight(80);
				$drawing->setWorksheet($spreadsheet->getActiveSheet());

				// Set style for header row using alternative method
				$spreadsheet->getActiveSheet()->getStyle('A3:E3')->applyFromArray(
				    [
				            'font' => [
				                'bold' => true,
				            ],
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				            'borders' => [
				                'top' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'bottom' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'right' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'left' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				            ],
				            'fill' => [
				                'fillType' => Fill::FILL_GRADIENT_LINEAR,
				                'rotation' => 90,
				                'startColor' => [
				                    'argb' => 'FFA0A0A0',
				                ],
				                'endColor' => [
				                    'argb' => 'FFFFFFFF',
				                ],
				            ],
				        ]
				);


				$spreadsheet->getActiveSheet()->getStyle('E1')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::VERTICAL_JUSTIFY,
				            ],
				        ]
				);


				$spreadsheet->getActiveSheet()->getStyle('E3')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				        ]
				);				


				$spreadsheet->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);
				// Set page orientation and size

				$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
				$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);


			//echo count($data['result_set']); exit();
			$spreadsheet->getActiveSheet()->fromArray($data,null,'A4',true);

			$writer = new Xlsx($spreadsheet);
			$helper = new Sample();

			$meter_name = strtolower($meter['data']);
			$meter_name = str_replace(' ', '_', $meter_name);

			$filename = $meter_name.'_dgrunnnighrs'.date('d_m_Y_h_i_a');
	

		$savepath = './reports_upload/';

		$success=false;

		if($download=="xls"){
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
			$result = $writer->save($savepath.$filename.".xls");
			$success = true;
		}else if($download=="pdf"){

			$spreadsheet->setActiveSheetIndex(0);
			$spreadsheet->getActiveSheet()->setShowGridLines(true);

			$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);

			$className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class;
			IOFactory::registerWriter('Pdf', $className);

			$writer = IOFactory::createWriter($spreadsheet, 'Pdf');
			$result = $writer->save($savepath.$filename.".pdf");

			$success = true;
		}else if($download=="csv"){
			$writer = IOFactory::createWriter($spreadsheet, 'Csv')->setDelimiter(',')
								   								 ->setEnclosure('"')
								   								 ->setSheetIndex(0);
			$writer->save($filename);	

			$reader = IOFactory::createReader('Csv')->setDelimiter(',')
														 ->setEnclosure('"')
														 ->setSheetIndex(0);
			$spreadsheetFromCSV = $reader->load($filename);
			$writerCSV = IOFactory::createWriter($spreadsheetFromCSV, 'Csv');
			$writerCSV->setExcelCompatibility(true);						 								   								 
			$result = $writer->save($savepath.$filename.".csv");
			$success = true;
		}else {

			$result = $writer->save($savepath.$filename.".xlsx"); 
			$success = true;
		}

		return $filename;

	}	

public function kwh_processDownload($meter_id,$data,$download,$minDate,$maxDate){

		$meter =  $this->CI->iot_rest->getmeterName($meter_id); 
		$meter_name = strtolower($meter['data']);
		$meter_name = str_replace(' ', '_', $meter_name);

		$spreadsheet = new Spreadsheet();
		$richText = new RichText();
		$curreDate = date("m/d/Y h:i:s a", time());

		$heading = "KWH Report";	

		 $currUsername = $this->CI->session->userdata("first_name");
		 $totRecord = count($data);
		//$spreadsheet->getActiveSheet()->setCellValue('H1', $heading);
		//$spreadsheet->getActiveSheet()->setCellValue('H2', $curreDate);

		$spreadsheet->setActiveSheetIndex(0);
		$spreadsheet->setActiveSheetIndex(0)->setCellValue('A3', 'On Time')
		->setCellValue('B3', 'Off Time')
		->setCellValue('C3', 'Meter Name')
		->setCellValue('D3', 'Status')
		->setCellValue('E3', 'Duration')
		->setCellValue('F3', 'KWH Start')
		->setCellValue('G3', 'Consumed')
		->setCellValue('H3', 'Total');

			$richText->createText('');

			$payable = $richText->createTextRun($heading."\n");
			$payable->getFont()->setBold(true);
			$payable->getFont()->setItalic(true);
			$payable->getFont()->setSize(15);
			$payable->getFont()->setColor(new Color(Color::COLOR_DARKRED));
			$richText->createText($curreDate."\n");
			$richText->createText("By: ".strtoupper($currUsername)."\n");
	

			 if($minDate && $maxDate){
					$richText->createText("From Date: ".$minDate."\n");
					$richText->createText("To Date: ".$maxDate."\n");
				}
					$richText->createText("Rows: ".$totRecord."\n");

				$spreadsheet->getActiveSheet()->getCell('H1')->setValue($richText);

				if($download=="xls"){
		    	//set height for 1st row
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(120);
		    	}else{
		    		$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(100);
		    	}
				// Protect cells
				//$spreadsheet->getActiveSheet()->getProtection()->setSheet(true); // Needs to be set to true in order to enable any worksheet protection!

				// Set column widths
				$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
				$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(45);

				// Set fills
				$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setARGB('FFECECEC');

				$spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(Fill::FILL_SOLID);
				$spreadsheet->getActiveSheet()->getStyle('A2:H2')->getFill()->getStartColor()->setARGB('FFECECEC');				

				//remove row
				//$spreadsheet->getActiveSheet()->removeRow(2);

				//set logo
				$logoImg = FCPATH.'assets/images/fourbends_logo.png';
				$drawing = new Drawing();
				$drawing->setName('Logo');
				$drawing->setDescription('Logo');
				$drawing->setPath($logoImg);
				$drawing->setHeight(80);
				$drawing->setWorksheet($spreadsheet->getActiveSheet());

				// Set style for header row using alternative method
				$spreadsheet->getActiveSheet()->getStyle('A3:H3')->applyFromArray(
				    [
				            'font' => [
				                'bold' => true,
				            ],
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				            'borders' => [
				                'top' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'bottom' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'right' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				                'left' => [
				                    'borderStyle' => Border::BORDER_THIN,
				                ],
				            ],
				            'fill' => [
				                'fillType' => Fill::FILL_GRADIENT_LINEAR,
				                'rotation' => 90,
				                'startColor' => [
				                    'argb' => 'FFA0A0A0',
				                ],
				                'endColor' => [
				                    'argb' => 'FFFFFFFF',
				                ],
				            ],
				        ]
				);

				$spreadsheet->getActiveSheet()->getStyle('H1')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::VERTICAL_JUSTIFY,
				            ],
				        ]
				);

				$spreadsheet->getActiveSheet()->getStyle('F:H')->applyFromArray(
				    [
				            'alignment' => [
				                'horizontal' => Alignment::HORIZONTAL_LEFT,
				            ],
				        ]
				);

	

				$spreadsheet->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);
				// Set page orientation and size

				$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
				$spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);


			//echo count($data['result_set']); exit();
			$spreadsheet->getActiveSheet()->fromArray($data, null, 'A4', true);

			$writer = new Xlsx($spreadsheet);
			$helper = new Sample();

			$meter_name = strtolower($meter['data']);
			$meter_name = str_replace(' ', '_', $meter_name);

			$filename = $meter_name.'_kwh'.date('d_m_Y_h_i_a');
	

		$savepath = './reports_upload/';

		$success=false;

		if($download=="xls"){
			$writer = IOFactory::createWriter($spreadsheet, 'Xls');
			$result = $writer->save($savepath.$filename.".xls");
			$success = true;
		}else if($download=="pdf"){

			$spreadsheet->setActiveSheetIndex(0);
			$spreadsheet->getActiveSheet()->setShowGridLines(true);

			$spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);

			$className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class;
			IOFactory::registerWriter('Pdf', $className);

			$writer = IOFactory::createWriter($spreadsheet, 'Pdf');
			$result = $writer->save($savepath.$filename.".pdf");

			$success = true;
		}else if($download=="csv"){
			$writer = IOFactory::createWriter($spreadsheet, 'Csv')->setDelimiter(',')
								   								 ->setEnclosure('"')
								   								 ->setSheetIndex(0);
			$writer->save($filename);	

			$reader = IOFactory::createReader('Csv')->setDelimiter(',')
														 ->setEnclosure('"')
														 ->setSheetIndex(0);
			$spreadsheetFromCSV = $reader->load($filename);
			$writerCSV = IOFactory::createWriter($spreadsheetFromCSV, 'Csv');
			$writerCSV->setExcelCompatibility(true);						 								   								 
			$result = $writer->save($savepath.$filename.".csv");
			$success = true;
		}else {

			$result = $writer->save($savepath.$filename.".xlsx"); 
			$success = true;
		}

		return $filename;

	}		
}
