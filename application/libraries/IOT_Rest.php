<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('IotRestInterface');

class IOT_Rest implements IotRestInterface
{

	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		require_once( APPPATH . 'third_party/boodskap/vendor/autoload.php');

	}
	
	public function login($email, $password){
		
		$api_instance = new Swagger\Client\Api\LoginApi();
		
		try {
			$result = $api_instance->login($email, $password);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			$this->CI->session->set_userdata($udata); // Set the session data
			$msg = array("status" => "success", "message" => "Successfully Loggedin");
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$smsg);
			$msg = array("status" => "fail", "message" => "Invalid Login");
			return $msg;
		}
	}
	
	public function isloggedin(){
		$status = $this->CI->session->has_userdata("user_token");
		return $status;
	}
	
	public function get_fbuser_data($name){
		$udata = $this->CI->session->has_userdata($name);
		if($udata){
			return $this->CI->session->userdata($name);
		}else{
			return "";
		}
	}
	
	public function get_message($msg_id){
		//$msg_id = "7777";
		$apiInstance = new Swagger\Client\Api\ListDeviceMessagesApi(
			// If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
			// This is optional, `GuzzleHttp\Client` will be used as default.
			new GuzzleHttp\Client()
		);
		$atoken = $this->get_fbuser_data("user_token"); // string | Auth token of the logged in user
		$page_size = 100; // int | Maximum number of messages to be listed
		$direction = ""; // string | If direction is specified, **muid** is required
		$muid = $msg_id; // string | Last or First message uuid of the previous list operation, **required** if **direction** is specified

		try {
			$result = $apiInstance->listDeviceMessages($atoken, $page_size, $direction, $muid);
			
			$mobj = isset($result["0"]) ? $result["0"] : "";
			if(!empty($mobj)){
				$mdata = $mobj->getData();
				$nuuid = $mobj->getNodeUuid();
				$amdata = json_decode($mdata, true);
				$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => array_merge($amdata, array("nuuid" => $nuuid)));
				return $msg;
				
			}else{
				$msg = array("status" => "fail", "message" => "Message Data did not fetch", 
				 "data" => array());
				return $msg;
			}
			
		} catch (Exception $e) {
			//echo 'Exception when calling ListDeviceMessagesApi->listDeviceMessages: ', $e->getMessage(), PHP_EOL;
			$msg = array("status" => "fail", "message" => "API Issue: ".$e->getMessage() , 
				 "data" => array());
			return $msg;
		}
	}
	
   public function get_query_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->SearchByQuery($atoken, $type, $query, $tbl_id);
	   //fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,
	   "result_set" => $result_set);
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }
	
	public function get_power_detail($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
    
	public function get_power_list($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 20;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}


	public function get_records(){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
    
	public function calc_kwh_eb($meter_id){
		$rec_id = fb_fetch_id("power-meter"); // "350412";
		$result_set = $this->get_power_detail($meter_id);
		//fb_pr($result);exit;
		
		$time = $this->get_KWH();
		
		if($time["status"]=="success" && $result_set["status"]=="success"){
				$aggs["aggs"] = $time["aggs"];
				
				foreach($aggs as $row){
					$min_time=$row['min_time']['value'];
					$max_time=$row['max_time']['value'];
				}
				$diff = $max_time - $min_time;
				$runtime = gmdate('H',$diff);
				$tot_power = $result_set["data"]["tp"];
				$kwh = floatval($tot_power* $runtime);
				return $kwh;
				
		}
		else{
			$kwh = 1;
			return $kwh;
		}
	}
	public function calc_kwh_dg($meter_id){
		// $tbl_id = 350409;
		$tbl_id = fb_fetch_id("genset_activity");
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";

			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->CI->parser->parse('query/script_fields', $qpms, true);
			$result = $this->get_query_agg_result($tbl_id, $query_str);
			
			$rec_id = fb_fetch_id("power-meter"); // "350412";
			$result_set = $this->get_power_detail($meter_id);
			//print_r($result_set); exit();
		if($result["status"]=="success" && $result_set["status"]=="success"){
			$aggs["aggs"] = $result["aggs"];
			foreach($aggs as $row){
				
				foreach($row as $result){
					$runtime=$result["key"];
					
				}
				$tot_power = $result_set["data"]["tp"];
				$no_of_hrs = gmdate('H',$runtime);
				$kwh = floatval($tot_power * $no_of_hrs);
				
			}
			return $kwh;
		}
		else{
			$kwh = 1;
			return $kwh;
		}
	}
	
	public function get_totVolt($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tpv"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  
    
    public function get_singleVolt($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tv"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  
    
    public function get_totAmp($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tc"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	} 
    public function getPowerfactor($meter_id){
            $tbl_id = fb_fetch_id("power-meter"); // "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
			$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["pf"] : array();
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        }   
    
        public function gettotWatts($meter_id){
            $tbl_id = fb_fetch_id("power-meter"); // "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
			$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = $result["result_set"][0]["_source"]; 
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        }     

    
		public function getDG($meter_id){
            $tbl_id = fb_fetch_id("power-meter"); // "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
            $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
            $query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = $result["result_set"][0]["_source"];
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        } 	
	public function getdg_record($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("val" => "1", "size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
		$query_str = $this->CI->parser->parse('query/query-rec', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result);
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				"data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
	public function geteb_record($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 10000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("val" => "0", "size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
		 $query_str = $this->CI->parser->parse('query/query-rec', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result); exit();
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully","data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}

    public function usePowerfactor($meter_id){
        $result = $this->getPowerfactor($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }

     public function getAmpValue($meter_id){
        $result = $this->get_totAmp($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }

    public function getVoltValue($meter_id){
        $result = $this->get_totVolt($meter_id);

        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }          	

    public function formulaKVA($meter_id){
        $PF = $this->usePowerfactor($meter_id);
        $A = $this->getAmpValue($meter_id);
        $V = $this->getVoltValue($meter_id);
        $kVA = round($A * $V * 1.732 / 1000,2); 
        return $kVA;
    }

    public function gettotPower($meter_id)
    {
        $result = $this->gettotWatts($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
			$tp = $result['data']['tp'];
        	return $tp;
		}else{
			return 0;
		}
    }

    public function getsinglewattsValue($meter_id)
    {
        
        $result = $this->gettotWatts($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rp=  $result['data']['rp'];
			$bp= $result['data']['bp'];
			$yp = $result['data']['yp'];
			$tp = $result['data']['tp'];
        	$arrPower = array();
        	array_push($arrPower, $rp,$bp,$yp,$tp);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }        	

	public function widget_3Voltage($row_id,$meter_id){

		 $result = $this->get_totVolt($meter_id);

        if($result["status"] == "success" && !(empty($result['data']))){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Three Phase Voltage</p>
                <h2 class="color-white odometer" id="avgtot3">
                '.$result['data'].'
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';

		}else{
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Three Phase Voltage</p>
                <h2 class="color-white odometer" id="avgtot3">
                 0
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';
		}
	}

	public function widget_1Voltage($row_id,$meter_id){

		 $result = $this->get_singleVolt($meter_id);

        if($result["status"] == "success" && !(empty($result['data']))){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white odometer" id="total-volt-r">
                '.$result['data'].'
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';

		}else{
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white odometer" id="total-volt-r">
                0
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';
		}
	}

	public function widget_3KVA($row_id,$meter_id){
		$kva = $this->getkva($meter_id); 
		$totalkva = $kva['kva'];
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-warning  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KVA</p>
                <h2 class="color-white odometer" id="three-kva">
                    '.$totalkva.'</h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>'; 
	}

	public function widget_1KVA($row_id){
		$kva = $this->getkva(); 
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Single Phase KVA</p>
                <h2 class="color-white odometer" id="single-kva">
                    '.$kva.'</h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>'; 
	}

	public function widget_TotalPower($row_id,$meter_id){

		$totPower = $this->gettotPower($meter_id);
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Three Phase Power</p>
                <h2 class="color-white ">
                   '.$totPower.'  (KW)</h2>
              </div>
            </div>
          </div>
        </div>'; 		
	}

	/*public function widget_KWH($row_id,$meter_id){

		$kwh = $this->getkwh($meter_id);
	
	        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
	          <div class="card bg-danger p-20 drag-widget">
	            <div class="media widget-ten">
	              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
	              <div class="media-body media-text-right">
	                  <p class="m-b-0 m-l-20 media-text-left">EB KWH</p>
	                <h2 class="color-white odometer" id="eb-kva">
	                   '.$kwh['kwh'].' </h2>
	              </div>
	            </div>
	          </div>
	        </div>'; 	
	       	
	}*/
	public function widget_KWH($row_id,$meter_id){

		$kwh = $this->getkwh($meter_id);
	        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
	          <div class="card bg-danger p-20 drag-widget">
	            <div class="media widget-ten">
	              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
	              <div class="media-body media-text-right">
	                  <p class="m-b-0 m-l-20 media-text-left">Total KWH</p>
	                <h2 class="color-white odometer" id="dg-kva">
	                   '.$kwh['kwh'].' </h2>
	              </div>
	            </div>
	          </div>
	        </div>'; 	
	       	
	}
	
	public function widget_bcheck_time($row_id,$meter_id){
		///get bcheck settings
		$result = $this->getBcheck($meter_id);
		$date=$result['data']['date']/1000;
		$updatedtime = $result['data']['updatedtime']*1000;
		//echo '<br>'.date("m/d/Y h:i:s a",$updatedtime);
		$next_service_hour = $result['data']['hours'];
		//Calculate difference
		$diff=$date-time();//time returns current time in seconds
		$days=floor($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
		if($days<0)
			$days = 0;
		$hours=round(($diff-$days*60*60*24)/(60*60));
		
		///get old running hours
			$qry = '{  "query": { "match":{"meter_id":'.$meter_id.'} },  "size" : 1000,  "from": 0,  "sort": { "createdtime" : {"order" : "asc"} } }';
			$tbl_id = fb_fetch_id("meters"); // "350414";
			$result = $this->get_query_result($tbl_id, $qry);
			$old_running_hrs = $result['result_set'][0]['_source']['running_hrs'];
			//fb_pr($result);
			
		///get current running hours
			$crunning_hrs = 0;
			$now=time()*1000;
//			$qry = '{ "query": { "bool": { "must": [ { "range": { "last_on_time": { "gte": "0", "boost": 2.0 } } }, { "match": { "device_id": '.$meter_id.' } } ] } }, "aggs": { "runtime": { "terms": { "script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay" }, "aggs": { "tops": { "top_hits": { "size": 100000 } } } } } }';
			$qry = '{ "query": { "bool": { "must": [ { "range": { "last_on_time": { "gte": "'.$updatedtime.'", "lte": "'.$now.'", "boost": 2.0 } } }, { "match": { "device_id": '.$meter_id.' } } ] } }, "aggs": { "runtime": { "terms": { "script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay" }, "aggs": { "tops": { "top_hits": { "size": 100000 } } } } } }';
			$tbl_id = fb_fetch_id("genset_activity"); // "350409";
			$result = $this->get_query_agg_result($tbl_id, $qry);
			$aggs = $result['aggs'];
			if(isset($aggs))
			{
				$key =0;
				foreach($aggs as $arr)
				{
					if($arr['key']>0)
					$key+=$arr['key'];
				}
			//	echo '<br> Key'.$key;
				$totalMinutes = $key/60;
				$hours = round($totalMinutes / 60);
				$crunning_hrs = $hours;
				$running_hours = $old_running_hrs + $crunning_hrs;
			}
			$remaining_hours = $next_service_hour - $running_hours;
			if($remaining_hours<0)
				$remaining_hours = 0;
			
			//fb_pr($result);
			//exit;
			
			
		//exit;	
	        echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_1" style="position: relative; opacity: 1; left: 0px; top: 0px;">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">B-check time</p>                
                <div class="row">
                  <div class="col-sm-12">Remaining days : '.$days.' days</div>                  
                </div>
				<br>
				<div class="row">
                  <div class="col-sm-12">Remaining hours : '.$remaining_hours.' hours</div>                  
                </div>
                
              </div>
            </div>
          </div>
        </div>'; 	
	       	
	}
	
	public function widget_SinglePower($row_id,$meter_id){
		$power = $this->getsinglewattsValue($meter_id);
		$r = $power[0];
		$b = $power[1];
		$y = $power[2];
		$ll = $power[3];
		
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">POWER (KW)</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rp_update">'.round($r,5).' </span></div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bp_update">'.round($b,5).' </span></div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yp_update">'.round($y,5).' </span></div>
                </div>
                <div class="m-t-6 m-l-20">TP : <span class="widget-span odometer" id="tot_power">'.round($ll,5).'</span></div>
              </div>
            </div>
          </div>
        </div>';		
	}

	public function widget_Frequency($row_id,$meter_id){
		$result = $this->getFrequency($meter_id);
		if($result["status"] == "success" && !(empty($result['data']))){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-warning p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer" id="freq">'.$result['data'].'</h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>';
    	}else{
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-danger p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer" id="freq">0</h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>';    		
    	}
	}

	public function widget_Current($row_id,$meter_id){
		$A = $this->getAmpValue($meter_id);
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Current</p>
                <h2 class="color-white odometer" id="total-current">'.$A.'</h2>
                <p class="m-b-0">A</p>
              </div>
            </div>
          </div>
        </div>'; 		
	}

	public function widget_PowerFactor($row_id,$meter_id){
	 $PF= $this->usePowerfactor($meter_id);
	echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-danger  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power Factor</p>
                <h2 class="color-white odometer" id="powerfactor">'.$PF.'</h2>
                <p class="m-b-0">PF</p>
              </div>
            </div>
          </div>
        </div>';
	}

	public function widget_gaugeAmp($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-amp" style="height: 250px;"></div>
            </div>
          </div>
        </div>';	
	}

	public function widget_gaugeVoltage($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-voltage" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}

	public function widget_gaugesingleVoltage($row_id){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
	          <div class="card drag-widget">
	            <div class="card-title">
	            </div>
	            <div class="card-content">
	              <div id="gauge-voltage-single" style="height: 250px;"></div>
	            </div>
	          </div>
	        </div>';
	}					

	public function widget_consumption($row_id){
		echo '<div class="col-md-12 livegraph_consumption" id="recordsArray_'.$row_id.'">
          <div class="card ">
            <div class="card-title text-center">
              Consumption 
            </div>
            <div class="card-content">
            <button type="button" class="btn btn-primary btn-sm m-r-20 pull-right" data-toggle="modal" data-target="#livegraph-modal">Live Graph</button>
            <div class="graph-preloader">
        		<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
				</svg>
    		</div>
              <div id="linechart" style="height: 400px"></div>
            </div>
          </div>
        </div>';
	}

	public function widget_FrequencyGauge($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-frequency" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}	

	public function getmeter_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}

	public function activemeter_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-status-filter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}

	public function getFrequency($meter_id){
        $tbl_id = fb_fetch_id("power-meter"); // "350412";
        $from = 0;
        $size = 1;
        $orderfld = "createdtime";
        $orderdir = "desc";

        $this->CI->load->library('parser');
        $this->CI->parser->set_delimiters("__","__");
        $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
        $query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

        $result = $this->get_query_result($tbl_id, $query_str);
        //print_r($result); exit();
        if($result["status"]=="success"){
            $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["fre"] : array();
            $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                 "data" => $src);
                return $msg;
        }else{
            $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                return $msg;
        }

      }

    public function getRBY_Voltage($meter_id)
    {
        
        $result = $this->gettotWatts($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rv=  $result['data']['rv'];
			$bv= $result['data']['bv'];
			$yv = $result['data']['yv'];
			$tpv = $result['data']['tpv'];
        	$arrPower = array();
        	array_push($arrPower, $rv,$bv,$yv,$tpv);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }     

    public function getRBY_Amp($meter_id)
    {
        
        $result = $this->gettotWatts($meter_id);
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rc=  $result['data']['rc'];
			$bc= $result['data']['bc'];
			$yc = $result['data']['yc'];
			$tc = $result['data']['tc'];
        	$arrPower = array();
        	array_push($arrPower, $rc,$bc,$yc,$tc);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }  
    public function widget_ViewVoltage($row_id,$meter_id){
		$vol = $this->getRBY_Voltage($meter_id);     	
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">VOLTAGE</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rv_update">'.$vol[0].' </span> V</div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bv_update">'.$vol[1].' </span> V</div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yv_update">'.$vol[2].' </span> V</div>
                </div>
                <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer" id="avgtot3">'.$vol[3].'</span> V</div>
              </div>
            </div>
          </div>
        </div>';
     }
    public function widget_ViewAmp($row_id,$meter_id){
		$vol = $this->getRBY_Amp($meter_id);     	
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">Amp</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rc_update">'.round($vol[0],2).' </span> A</div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bc_update">'.round($vol[1],2).' </span> A</div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yc_update">'.round($vol[2],2).' </span> A</div>
                </div>
                <div class="m-t-6 m-l-20">TA : <span class="widget-span odometer" id="total-current">'.round($vol[3],2).'</span> A</div>
              </div>
            </div>
          </div>
        </div>';    	
    }

	public function widget_gaugeKVA($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-kva" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}	    
    
	public function get_recordsByValue($val,$meter_id){

		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 10000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		if($val=="1d"){
			$range = "now-1d";
		}elseif ($val=="7d") {
			$range = "now-7d";
		}elseif($val=="1m"){
			$range = "now-1M";
		}elseif ($val=="1y") {
			$range = "now-1y";
		}
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"lowv" => $range , "topv" => "now","range_fld" => "createdtime",'meter_id'=>$meter_id);

		 $query_str = $this->CI->parser->parse('query/query-range', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
	public function get_KWH(){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "createdtime";
			$orderdir = "desc";

			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->CI->parser->parse('query/query-per-day', $qpms, true);
			$result = $this->get_query_aggs_result($tbl_id, $query_str);
			
			if($result["status"]=="success"){
				
               
			   $msg = array("status" => "success", "message" => "Search result got it","aggs"=>$result['result_set']['aggregations'] );
			  
			   return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }
	}
	
	public function get_query_aggs_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $aresult);
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	}
	
	public function get_query_agg_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		//$tbl_id = $this->tables[$table_name];	
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   //fb_pr($aresult);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $agg = isset($aresult["aggregations"]["runtime"]["buckets"]) ? $aresult["aggregations"]["runtime"]["buckets"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $result_set,"aggs"=>$agg );
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }

	 public function getDGkva($meter_id){
	 	    $tbl_id = fb_fetch_id("meters"); // "350414";
            $from = 0;
            $size = 1;
            $orderfld = "createdtime";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
			$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["kva"] : array();
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

	 }

	public function get_alertEmails(){
		$tbl_id = fb_fetch_id("alert_notify"); // "350402";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}	 


public function recordFilters_records($min_date,$max_date,$from,$per_page,$filter_type,$meter_id,$hr){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";

		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("val" => $filter_type, "size" => $per_page, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"min_date"=>$min_date,"max_date"=>$max_date,"meter_id"=>$meter_id,"hr"=>$hr);
		
		if($hr){
			$query_str = $this->CI->parser->parse('query/query-per-hour', $qpms, true);
			$result = $this->get_query_agg_result($tbl_id, $query_str);
			$aggs = $result['aggs'];
			//print_r($aggs); exit();
		}else{
			$query_str = $this->CI->parser->parse('query/query-per-hour', $qpms, true);
			$result = $this->get_query_result($tbl_id, $query_str);
		}
		//print_r($result); exit();
		if($result["status"]=="success"){
			if (!empty($aggs)){
			//print_r($agg);
			$data = array();
			$aggregation_data = array();
				foreach($aggs as $row){
					$rhts = $row['tops']['hits']['hits'];
					foreach($rhts as $rhtk => $rhtv){
						$aggregation_data[] = $rhtv;
					}
					// $aggregation_data[] = $row['tops']['hits']['hits'][0];
				}
				$data["result_set"] = $aggregation_data;
			}
			if(!empty($data)){
				$src = isset($data["result_set"]) ? $data["result_set"] : array();
			}else{
				$src = isset($result["result_set"]) ? $result["result_set"] : array();
			}
			//print_r($src); exit();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				"data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}	

    function getkwh($meter_id){
    	//$id = $this->input->post('meter_id');
        $rec_id = fb_fetch_id("power-meter"); // "350412";
        $data = array();
		$result = $this->get_kwh_bystatus($meter_id);

		if($result["status"] == "success"){	
			$totalKWH= $result['data']["kwh"];
			$data = array('kwh'=>round($totalKWH,2));				
		}

		return $data;
    }   

	function getBcheck($meter_id){

		$tbl_id = fb_fetch_id("bcheck_settings"); // "350416";
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}		
	}

	 public function getmeterName($meter_id){
	 	    $tbl_id = fb_fetch_id("meters"); // "350414";
            $from = 0;
            $size = 1;
            $orderfld = "createdtime";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id" => $meter_id);
			$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["name"] : array();
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

	 }

	function getfuelSettings($meter_id){

		$tbl_id = fb_fetch_id("dgfuel_setting"); // "350417";
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
		$query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$_id = isset($result["result_set"][0]["_id"]) ? $result["result_set"][0]["_id"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src,"id"=>$_id);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}		
	}	

  public function dgKVAdivider($id){

  	if(!$id)
    	$id=$this->input->post('meter_id');
    	$kvaData = $this->getDGkva($id);
    	//print_r($kvaData['data']);
    	$kva= $kvaData['data'];

    	//% of kva
    	$qtr = (25/100)*$kva;
    	$half = (50/100)*$kva;
    	$threefour = (75/100)*$kva;
    	$full = (100/100)*$kva;

    	$arrKva = array(0=>$qtr,1=>$half,2=>$threefour,3=>$full);

    	//print_r($arrKva);
    	return $arrKva;
  }

  public function dgFuel($id,$minDate,$maxDate){
		if($this->CI->fb_rest->isloggedin()){
			$tbl_id = "power-meter";
			$from = 0;
			$size = "10000";
			$filter_type ="1";
			$orderfld = "createdtime";
			$orderdir = "desc";
			$min_date = $minDate;
			$max_date= $maxDate;
			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");

			$kvaList=$this->dgKVAdivider($id);

			//print_r($kvaList);exit();
			$kva_qtr = $kvaList[0];
			$kva_half = $kvaList[1];
			$kva_threefour = $kvaList[2];
			$kva_full = $kvaList[3];

			//fuel settings

			$fuelSettings = $this->getfuelSettings($id);
			$fuelQtr = $fuelSettings['data']['twentyfive'];
			$fuelHalf = $fuelSettings['data']['fifty'];
			$fuelThreefour = $fuelSettings['data']['seventyfive'];
			$fuelFull = $fuelSettings['data']['hundred'];
			$fuelExceed = $fuelSettings['data']['hundredmore'];
		

			$fuel1 = $this->dgfuelQuery($id,$minDate,$maxDate,0,$kva_qtr);
			$fuel2 = $this->dgfuelQuery($id,$minDate,$maxDate,$kva_qtr,$kva_half); 
			$fuel3 = $this->dgfuelQuery($id,$minDate,$maxDate,$kva_half,$kva_threefour);
			$fuel4 = $this->dgfuelQuery($id,$minDate,$maxDate,$kva_threefour,$kva_full);
			$fuel5 = $this->dgfuelQuery($id,$minDate,$maxDate,$kva_full,'');

			$consumed1=0;
			$consumed2=0;
			$consumed3=0;
			$consumed4=0;
			$consumed5=0;

			if($fuel1>0 && $fuel1<60){
				//echo $fuel1."<br>";
				$consumed1 = ($fuel1/60)*$fuelQtr;
			}else if($fuel1>60){
				//echo $fuel1."<br>";
				$sec = $fuel1 * 13;
				$fuel1 = ($sec/60) + $fuel1;
				//echo $fuel1."<br>";
				$consumed1 = ($fuel1/60) * $fuelQtr;
			}

			if($fuel2>0 && $fuel2<60){
				$consumed2 = ($fuel2/60)*$fuelHalf;
			}else if($fuel2>60){
				//echo $fuel2."<br>";
				$sec = $fuel2 * 13;
				$fuel2 = ($sec/60) + $fuel2;
				$consumed2 = ($fuel2/60) * $fuelHalf;
			}

			if($fuel3>0 && $fuel3<60){
				$consumed3 = ($fuel3/60)*$fuelThreefour;
			}else if($fuel3>60){
				//echo $fuel3."<br>";
				$sec = $fuel3 * 13;
				$fuel3 = ($sec/60) + $fuel3;				
				$consumed3 = ($fuel3/60) * $fuelThreefour;
			}	

			if($fuel4>0 && $fuel4<60){
				$consumed4 = ($fuel4/60)*$fuelFull;
			}else if($fuel4>60){
				//echo $fuel4."<br>";
				$sec = $fuel4 * 13;
				$fuel4 = ($sec/60) + $fuel4;				
				$consumed4 = ($fuel4/60) * $fuelFull;
			}	

			if($fuel5>0 && $fuel5<60){
				$consumed5 = ($fuel5/60)*$fuelExceed;
			}else if($fuel5>60){
				//echo $fuel5."<br>";
				$sec = $fuel5 * 13;
				$fuel5 = ($sec/60) + $fuel5;				
				$consumed5 = ($fuel5/60) * $fuelExceed;
			}	

			return $fuelConsumed = $consumed1+$consumed2+$consumed3+$consumed4+$consumed5;
		}

  }
  public function dgfuelQuery($meter_id,$minDate,$maxDate,$minVal,$maxVal){

			$tbl_id = "power-meter";
			$from = 0;
			$size = "10000";
			$filter_type ="1";
			$orderfld = "createdtime";
			$orderdir = "desc";
			$qpms = array("val" => $filter_type, "size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"min_date"=>$minDate,"max_date"=>$maxDate,"meter_id"=>$meter_id,"minval"=>$minVal,"maxval"=>$maxVal);

			$query = $this->CI->parser->parse('query/dg_fuel_total', $qpms, true);  
			
			$result = $this->CI->fb_rest->dgfuel_tot_query($tbl_id, $query);
			return $fuel = $result['total_count']; 

  }

public function alertsFilters_records($min_date,$max_date,$from,$per_page){
		$tbl_id = fb_fetch_id("g_notifications"); // "351200";

		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $per_page, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"min_date"=>$min_date,"max_date"=>$max_date);
		 $query_str = $this->CI->parser->parse('query/query-alert', $qpms, true);  
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result); exit();
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				"data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  

	public function getMeters(){
		$tbl_id = fb_fetch_id("meters"); // "350414";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		$presult_data = array();
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			for($i=0;$i<count($src);$i++){
				$meterId = $src[$i]['_source']['meter_id'];
				$meterName = $src[$i]['_source']['name'];
				$presult_data[$meterId] = $meterName;
			}
			//print_r($presult_data); exit();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $presult_data);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}	

	public function dghoursFilters_records($min_date,$max_date,$meter_id,$from,$per_page){
		// $tbl_id = "350409";
		$tbl_id = fb_fetch_id("genset_activity");
		$orderfld = "last_on_time";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $per_page, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"min_date"=>$min_date,"max_date"=>$max_date,"meter_id"=>$meter_id);
		 $query_str = $this->CI->parser->parse('query/dg_runninghrs', $qpms, true);   
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result); exit();
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				"data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	} 

	public function getnewRecords($meter_id,$time,$type,$calltype){
		if($calltype=="reports")
			$tbl_id = fb_fetch_id("power-meter"); // "350412";
		else
			$tbl_id = fb_fetch_id("genset_activity"); 
		// $tbl_id = "350409";
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		if($calltype=="reports")
			$qpms = array("size" => 1, "from" => 0,"time"=>$time,"meter_id"=>$meter_id,"val"=>$type,"calltype"=>$calltype);
		else
			$qpms = array("size" => 1, "from" => 0,"time"=>$time,"meter_id"=>$meter_id,"val"=>$type,"calltype"=>$calltype);

		$query_str = $this->CI->parser->parse('query/newrecord-total', $qpms, true); 
		$result = $this->get_query_result($tbl_id, $query_str); 
		return $result;
	}	

	public function get_kwh_bystatus($meter_id){
		$tbl_id = fb_fetch_id("power-meter"); 
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		 $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$meter_id);
         $query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}

    function getkva($meter_id){
    	//$id = $this->input->post('meter_id');
        $rec_id = fb_fetch_id("power-meter"); // "350412";
        $data = array();
		$result = $this->get_kwh_bystatus($meter_id);

		if($result["status"] == "success"){	
			$totalkva= $result['data']["kva"];
			$data = array('kva'=>round($totalkva,2));				
		}

		return $data;
    }  

    public function kwhFilters_records($min_date,$max_date,$meter_id,$from,$per_page){
		// $tbl_id = "350409";
		$tbl_id = fb_fetch_id("kwh_log");
		$orderfld = "last_on_time";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $per_page, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"min_date"=>$min_date,"max_date"=>$max_date,"meter_id"=>$meter_id);
		 $query_str = $this->CI->parser->parse('query/kwh', $qpms, true);   
		
		$result = $this->get_query_result($tbl_id, $query_str);
		//print_r($result); exit();
		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				"data"=>$src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  	

	public function currentDGstatus($id){
      	$rec_id = fb_fetch_id("power-meter"); // "350412";
		$result = $this->getDG($id);
		//print_r($result); exit();
		 if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			
			$time= $result['data']['createdtime'];

			
			$lastCreatedtime = $this->CI->session->userdata('createdtime');

			$this->CI->session->set_userdata('createdtime', $result['data']['createdtime']);

        	$arrPower = array();
        	$arrPower= array_merge(array('status' =>$status) ,array('time'=>$time));
        	if($status==0 && $lastCreatedtime!=$time){
        		return "EB";
        	}
        	elseif ($status==1 && $lastCreatedtime!=$time) {
        		return "DG";
        	}else{
        		return "OFF";
        	}	
      }    
  }

  public function currentEBstatus($id){
      	$rec_id = fb_fetch_id("power-meter"); // "350412";
		$result = $this->getDG($id);
		//print_r($result); exit();
		 if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			
			$time= $result['data']['createdtime'];

			
			$lastCreatedtime = $this->CI->session->userdata('ebcreatedtime');

			$this->CI->session->set_userdata('ebcreatedtime', $result['data']['createdtime']);

        	$arrPower = array();
        	$arrPower= array_merge(array('status' =>$status) ,array('time'=>$time));
        	
        	if($status==0 && $lastCreatedtime!=$time){
        		return "EB";
        	}
        	elseif ($status==1 && $lastCreatedtime!=$time) {
        		return "DG";
        	}else{
        		return "OFF";
        	}	
      }    
  }

	public function getnotes_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
//		$this->CI->load->library('parser');
	//	$this->CI->parser->set_delimiters("__","__");
		//$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		//$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		$useremail = $this->CI->session->userdata("email");
		$query_str = '{  "query": { "match":{"email":"'.$useremail.'"} },  "size" : '.$size.',  "from": '.$from.',  "sort": { "'.$orderfld.'" : {"order" : "'.$orderdir.'"} } }';
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}
	
}
