<?php
/**
 * SendCommandUsingPropertyApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Boodskap API
 *
 * Boodskap IoT Platform API
 *
 * OpenAPI spec version: 2.0.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Swagger\Client\ApiException;
use Swagger\Client\Configuration;
use Swagger\Client\HeaderSelector;
use Swagger\Client\ObjectSerializer;

/**
 * SendCommandUsingPropertyApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SendCommandUsingPropertyApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
    }

    /**
     * @return Configuration
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Operation sendCommandUsingProperty
     *
     * Send a Command using Property
     *
     * @param  string $atoken Auth token of the logged in user (required)
     * @param  string $device_id device id (required)
     * @param  int $command_id static command id (required)
     * @param  string $property property name (required)
     * @param  \Swagger\Client\Model\PropertyCommand $command PropertyCommand JSON object (required)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Swagger\Client\Model\CommandStatus
     */
    public function sendCommandUsingProperty($atoken, $device_id, $command_id, $property, $command)
    {
        list($response) = $this->sendCommandUsingPropertyWithHttpInfo($atoken, $device_id, $command_id, $property, $command);
        return $response;
    }

    /**
     * Operation sendCommandUsingPropertyWithHttpInfo
     *
     * Send a Command using Property
     *
     * @param  string $atoken Auth token of the logged in user (required)
     * @param  string $device_id device id (required)
     * @param  int $command_id static command id (required)
     * @param  string $property property name (required)
     * @param  \Swagger\Client\Model\PropertyCommand $command PropertyCommand JSON object (required)
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Swagger\Client\Model\CommandStatus, HTTP status code, HTTP response headers (array of strings)
     */
    public function sendCommandUsingPropertyWithHttpInfo($atoken, $device_id, $command_id, $property, $command)
    {
        $returnType = '\Swagger\Client\Model\CommandStatus';
        $request = $this->sendCommandUsingPropertyRequest($atoken, $device_id, $command_id, $property, $command);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if ($returnType !== 'string') {
                    $content = json_decode($content);
                }
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\CommandStatus',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 417:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Swagger\Client\Model\ApiError',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation sendCommandUsingPropertyAsync
     *
     * Send a Command using Property
     *
     * @param  string $atoken Auth token of the logged in user (required)
     * @param  string $device_id device id (required)
     * @param  int $command_id static command id (required)
     * @param  string $property property name (required)
     * @param  \Swagger\Client\Model\PropertyCommand $command PropertyCommand JSON object (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendCommandUsingPropertyAsync($atoken, $device_id, $command_id, $property, $command)
    {
        return $this->sendCommandUsingPropertyAsyncWithHttpInfo($atoken, $device_id, $command_id, $property, $command)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation sendCommandUsingPropertyAsyncWithHttpInfo
     *
     * Send a Command using Property
     *
     * @param  string $atoken Auth token of the logged in user (required)
     * @param  string $device_id device id (required)
     * @param  int $command_id static command id (required)
     * @param  string $property property name (required)
     * @param  \Swagger\Client\Model\PropertyCommand $command PropertyCommand JSON object (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function sendCommandUsingPropertyAsyncWithHttpInfo($atoken, $device_id, $command_id, $property, $command)
    {
        $returnType = '\Swagger\Client\Model\CommandStatus';
        $request = $this->sendCommandUsingPropertyRequest($atoken, $device_id, $command_id, $property, $command);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = $responseBody->getContents();
                        if ($returnType !== 'string') {
                            $content = json_decode($content);
                        }
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'sendCommandUsingProperty'
     *
     * @param  string $atoken Auth token of the logged in user (required)
     * @param  string $device_id device id (required)
     * @param  int $command_id static command id (required)
     * @param  string $property property name (required)
     * @param  \Swagger\Client\Model\PropertyCommand $command PropertyCommand JSON object (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function sendCommandUsingPropertyRequest($atoken, $device_id, $command_id, $property, $command)
    {
        // verify the required parameter 'atoken' is set
        if ($atoken === null) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $atoken when calling sendCommandUsingProperty'
            );
        }
        // verify the required parameter 'device_id' is set
        if ($device_id === null) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $device_id when calling sendCommandUsingProperty'
            );
        }
        // verify the required parameter 'command_id' is set
        if ($command_id === null) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $command_id when calling sendCommandUsingProperty'
            );
        }
        // verify the required parameter 'property' is set
        if ($property === null) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $property when calling sendCommandUsingProperty'
            );
        }
        // verify the required parameter 'command' is set
        if ($command === null) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $command when calling sendCommandUsingProperty'
            );
        }

        $resourcePath = '/command/property/send/{atoken}/{deviceId}/{commandId}/{property}';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;


        // path params
        if ($atoken !== null) {
            $resourcePath = str_replace(
                '{' . 'atoken' . '}',
                ObjectSerializer::toPathValue($atoken),
                $resourcePath
            );
        }
        // path params
        if ($device_id !== null) {
            $resourcePath = str_replace(
                '{' . 'deviceId' . '}',
                ObjectSerializer::toPathValue($device_id),
                $resourcePath
            );
        }
        // path params
        if ($command_id !== null) {
            $resourcePath = str_replace(
                '{' . 'commandId' . '}',
                ObjectSerializer::toPathValue($command_id),
                $resourcePath
            );
        }
        // path params
        if ($property !== null) {
            $resourcePath = str_replace(
                '{' . 'property' . '}',
                ObjectSerializer::toPathValue($property),
                $resourcePath
            );
        }

        // body params
        $_tempBody = null;
        if (isset($command)) {
            $_tempBody = $command;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            $httpBody = $_tempBody;
            // \stdClass has no __toString(), so we should encode it manually
            if ($httpBody instanceof \stdClass && $headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($httpBody);
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $multipartContents[] = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\build_query($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\build_query($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption()
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
