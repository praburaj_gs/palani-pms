# Command

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **string** |  | 
**device_ids** | **string[]** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


