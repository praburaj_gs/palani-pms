# OTABatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**state** | **string** |  | 
**name** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**to_model** | **string** |  | 
**to_version** | **string** |  | 
**created_at** | **int** |  | 
**expire_at** | **int** |  | 
**finished_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


