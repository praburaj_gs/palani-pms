# DomainLicense

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**nodes** | **int** |  | [optional] 
**cores** | **int** |  | [optional] 
**devices** | **int** |  | [optional] 
**message_per_day** | **int** |  | [optional] 
**features** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


