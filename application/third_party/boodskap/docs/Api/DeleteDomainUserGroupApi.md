# Swagger\Client\DeleteDomainUserGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDomainUserGroup**](DeleteDomainUserGroupApi.md#deleteDomainUserGroup) | **DELETE** /domain/user/group/delete/{atoken}/{gid} | Delete Domain User Group


# **deleteDomainUserGroup**
> \Swagger\Client\Model\Success deleteDomainUserGroup($atoken, $gid)

Delete Domain User Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteDomainUserGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group Id

try {
    $result = $apiInstance->deleteDomainUserGroup($atoken, $gid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteDomainUserGroupApi->deleteDomainUserGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group Id |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

