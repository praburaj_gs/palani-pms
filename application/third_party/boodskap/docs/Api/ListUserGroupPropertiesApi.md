# Swagger\Client\ListUserGroupPropertiesApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listUserGroupProperties**](ListUserGroupPropertiesApi.md#listUserGroupProperties) | **GET** /user/group/property/list/{atoken}/{ouid}/{gid}/{pageSize} | List User Group Properties


# **listUserGroupProperties**
> \Swagger\Client\Model\UserGroupProperty[] listUserGroupProperties($atoken, $ouid, $gid, $page_size, $direction, $name)

List User Group Properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListUserGroupPropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$gid = 56; // int | Group id
$page_size = 56; // int | Maximum number of properties to be listed
$direction = "direction_example"; // string | If direction is specified, **name** is required
$name = "name_example"; // string | Last or First property name of the previous list operation, **required** if **direction** is specified

try {
    $result = $apiInstance->listUserGroupProperties($atoken, $ouid, $gid, $page_size, $direction, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListUserGroupPropertiesApi->listUserGroupProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **gid** | **int**| Group id |
 **page_size** | **int**| Maximum number of properties to be listed |
 **direction** | **string**| If direction is specified, **name** is required | [optional]
 **name** | **string**| Last or First property name of the previous list operation, **required** if **direction** is specified | [optional]

### Return type

[**\Swagger\Client\Model\UserGroupProperty[]**](../Model/UserGroupProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

