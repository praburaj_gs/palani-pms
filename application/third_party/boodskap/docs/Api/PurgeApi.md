# Swagger\Client\PurgeApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purge**](PurgeApi.md#purge) | **POST** /elastic/purge/query/{atoken}/{type} | Purge


# **purge**
> \Swagger\Client\Model\SearchResult purge($atoken, $type, $query, $spec_id)

Purge

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurgeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | 
$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
$spec_id = 789; // int | Required if **type** is one of **[ message | record ]**

try {
    $result = $apiInstance->purge($atoken, $type, $query, $spec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurgeApi->purge: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**|  |
 **query** | [**\Swagger\Client\Model\SearchQuery**](../Model/SearchQuery.md)| SearchQuery JSON |
 **spec_id** | **int**| Required if **type** is one of **[ message | record ]** | [optional]

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

