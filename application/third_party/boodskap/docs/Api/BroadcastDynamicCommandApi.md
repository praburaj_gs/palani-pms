# Swagger\Client\BroadcastDynamicCommandApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**broadcastDynamicCommand**](BroadcastDynamicCommandApi.md#broadcastDynamicCommand) | **POST** /command/broadcast/{atoken}/{commandId} | Broadcast a Dynamic command to multiple devices


# **broadcastDynamicCommand**
> \Swagger\Client\Model\CommandStatus[] broadcastDynamicCommand($atoken, $command_id, $command)

Broadcast a Dynamic command to multiple devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BroadcastDynamicCommandApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$command_id = 56; // int | Command Identifier
$command = new \Swagger\Client\Model\BroadcastCommand(); // \Swagger\Client\Model\BroadcastCommand | BroadcastCommand JSON object

try {
    $result = $apiInstance->broadcastDynamicCommand($atoken, $command_id, $command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BroadcastDynamicCommandApi->broadcastDynamicCommand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **command_id** | **int**| Command Identifier |
 **command** | [**\Swagger\Client\Model\BroadcastCommand**](../Model/BroadcastCommand.md)| BroadcastCommand JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus[]**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

