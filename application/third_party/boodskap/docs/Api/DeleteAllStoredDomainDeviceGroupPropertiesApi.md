# Swagger\Client\DeleteAllStoredDomainDeviceGroupPropertiesApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAllDomainDeviceGroupProperties**](DeleteAllStoredDomainDeviceGroupPropertiesApi.md#deleteAllDomainDeviceGroupProperties) | **DELETE** /domain/device/group/property/deleteall/{atoken}/{gid} | Delete All Stored Domain Device Group Properties


# **deleteAllDomainDeviceGroupProperties**
> \Swagger\Client\Model\Success deleteAllDomainDeviceGroupProperties($atoken, $gid)

Delete All Stored Domain Device Group Properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteAllStoredDomainDeviceGroupPropertiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | 

try {
    $result = $apiInstance->deleteAllDomainDeviceGroupProperties($atoken, $gid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteAllStoredDomainDeviceGroupPropertiesApi->deleteAllDomainDeviceGroupProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

