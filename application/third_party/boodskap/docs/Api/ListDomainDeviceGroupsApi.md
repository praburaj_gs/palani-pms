# Swagger\Client\ListDomainDeviceGroupsApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listDomainDeviceGroups**](ListDomainDeviceGroupsApi.md#listDomainDeviceGroups) | **GET** /domain/device/group/list/{atoken}/{pageSize} | List Domain Device Groups


# **listDomainDeviceGroups**
> \Swagger\Client\Model\DomainDeviceGroup[] listDomainDeviceGroups($atoken, $page_size, $direction, $gid)

List Domain Device Groups

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListDomainDeviceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$page_size = 56; // int | Maximum number of groups to be listed
$direction = "direction_example"; // string | If direction is specified, **gid** is required
$gid = 56; // int | Last or First group id of the previous list operation, **required** if **direction** is specified

try {
    $result = $apiInstance->listDomainDeviceGroups($atoken, $page_size, $direction, $gid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListDomainDeviceGroupsApi->listDomainDeviceGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **page_size** | **int**| Maximum number of groups to be listed |
 **direction** | **string**| If direction is specified, **gid** is required | [optional]
 **gid** | **int**| Last or First group id of the previous list operation, **required** if **direction** is specified | [optional]

### Return type

[**\Swagger\Client\Model\DomainDeviceGroup[]**](../Model/DomainDeviceGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

