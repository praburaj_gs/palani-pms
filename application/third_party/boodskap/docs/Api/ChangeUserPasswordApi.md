# Swagger\Client\ChangeUserPasswordApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeUserPassword**](ChangeUserPasswordApi.md#changeUserPassword) | **POST** /user/changepass/{atoken}/{email}/{pswd} | Change password of an user


# **changeUserPassword**
> \Swagger\Client\Model\Success changeUserPassword($atoken, $email, $pswd)

Change password of an user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ChangeUserPasswordApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$email = "email_example"; // string | Email/Username of the user
$pswd = "pswd_example"; // string | New password of the user

try {
    $result = $apiInstance->changeUserPassword($atoken, $email, $pswd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChangeUserPasswordApi->changeUserPassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **email** | **string**| Email/Username of the user |
 **pswd** | **string**| New password of the user |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

