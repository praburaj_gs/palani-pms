# Swagger\Client\PurgeByTemplateApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purgeByTemplate**](PurgeByTemplateApi.md#purgeByTemplate) | **POST** /elastic/purge/template/{atoken}/{type} | Purge by Template


# **purgeByTemplate**
> \Swagger\Client\Model\SearchResult purgeByTemplate($atoken, $type, $query, $spec_id)

Purge by Template

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PurgeByTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | 
$query = new \Swagger\Client\Model\TemplateQuery(); // \Swagger\Client\Model\TemplateQuery | TemplateQuery JSON
$spec_id = 789; // int | Required if **type** is one of **[ message | record ]**

try {
    $result = $apiInstance->purgeByTemplate($atoken, $type, $query, $spec_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurgeByTemplateApi->purgeByTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**|  |
 **query** | [**\Swagger\Client\Model\TemplateQuery**](../Model/TemplateQuery.md)| TemplateQuery JSON |
 **spec_id** | **int**| Required if **type** is one of **[ message | record ]** | [optional]

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

