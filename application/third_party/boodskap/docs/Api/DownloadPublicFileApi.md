# Swagger\Client\DownloadPublicFileApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**downloadPublicFile**](DownloadPublicFileApi.md#downloadPublicFile) | **GET** /files/public/download/{id} | Download Public file


# **downloadPublicFile**
> \SplFileObject downloadPublicFile($id)

Download Public file

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DownloadPublicFileApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | File unique UUID

try {
    $result = $apiInstance->downloadPublicFile($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DownloadPublicFileApi->downloadPublicFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| File unique UUID |

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

