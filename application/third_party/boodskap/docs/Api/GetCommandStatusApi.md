# Swagger\Client\GetCommandStatusApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCommandStatus**](GetCommandStatusApi.md#getCommandStatus) | **GET** /command/status/{atoken}/{deviceId}/{corrId} | Get Command Status


# **getCommandStatus**
> \Swagger\Client\Model\CommandStatus getCommandStatus($atoken, $device_id, $corr_id)

Get Command Status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GetCommandStatusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$device_id = "device_id_example"; // string | device id
$corr_id = 789; // int | Correlation ID

try {
    $result = $apiInstance->getCommandStatus($atoken, $device_id, $corr_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GetCommandStatusApi->getCommandStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **device_id** | **string**| device id |
 **corr_id** | **int**| Correlation ID |

### Return type

[**\Swagger\Client\Model\CommandStatus**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

