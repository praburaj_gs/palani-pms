# Swagger\Client\ExecuteScriptApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**executeScript**](ExecuteScriptApi.md#executeScript) | **POST** /script/execute/{atoken} | Execute boodskap console script


# **executeScript**
> \Swagger\Client\Model\ScriptResult executeScript($atoken, $script)

Execute boodskap console script

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ExecuteScriptApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$script = new \Swagger\Client\Model\Script(); // \Swagger\Client\Model\Script | Script JSON

try {
    $result = $apiInstance->executeScript($atoken, $script);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExecuteScriptApi->executeScript: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **script** | [**\Swagger\Client\Model\Script**](../Model/Script.md)| Script JSON |

### Return type

[**\Swagger\Client\Model\ScriptResult**](../Model/ScriptResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

