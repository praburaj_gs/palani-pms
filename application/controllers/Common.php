<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller {
	
	protected $CI;
	public function __construct()
	{
        parent::__construct();
        $this->CI =& get_instance();
		require_once( APPPATH . 'third_party/boodskap/vendor/autoload.php');

		if(!$this->fb_rest->isloggedin())
		{
			redirect("/login");
		}
	}
	
	public function index()
	{
		echo "this is common controller";
	}
	
	public function check_duplicate(){
		$search = $this->input->get_post("search", true);
		$fld_name = $this->input->get_post("fld_name", true);
		$table_name = $this->input->get_post("table_name", true);
		$rkey = $this->input->get_post("rkey", true);
		
		$rst = $this->fb_rest->check_duplicate($table_name,$fld_name,$search,$rkey);
		echo json_encode($rst);
		exit;
	}
    
	public function updatemsg_status(){
		$rkey = $this->input->post("rkey");
		//fb_pr($rkey);
		$table_name = "alerts";
		$from = 0;
		$size = 100;
		$orderfld = "updatedtime";
		$orderdir = "desc";
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir, "rkey" => $rkey);
		$query_str = $this->parser->parse('query/update_state', $qpms, true);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
		//print_r($result);
		//echo json_encode($result,true);
	     if($result["status"] == "success"){
			$rst = $result["result_set"];
			foreach($rst as $row){
				$idata = $row["_source"];
				if($idata["alert_state"] == "true"){
				$idata["alert_state"] = "false";
				}
			$this->fb_rest->update_record($table_name, $idata, $row["_id"]);
			}
		} 
		
	} 
	public function get_details(){
		
		$value = $this->input->post("rid");
		if($value==0)
		{
			$table_name = "alert_settings";
		}
		else{
			$table_name = "alert_settings_dg";
		}
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->parser->parse('query/query-simple', $qpms, true);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
		//print_r($result); exit;
		//echo json_encode($result,true);
	     if($result["status"] == "success"){
			$rst = $result["result_set"];
			 $this->output
			->set_content_type('application/json')
			->set_output(json_encode($rst[0]['_source']));
		} 
		
	}

	public function get_power_ById($id){
		$tbl_id = fb_fetch_id("power-meter"); // "350412";
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";

		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$id);
		 $query_str = $this->CI->parser->parse('query/query-by-meter', $qpms, true);
		
		$result = $this->iot_rest->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}

	public function getSingle_meter(){
		$resultArr= array();
		$id = $this->input->get('id');
		$kva= $this->iot_rest->getkva($id);
		$result = $this->get_power_ById($id);
		//$resultArr = array_merge($result['data'] , array("kva" => $kva['kva']));
		//print_r($result); exit();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result['data']));			
	}		
	
	
}		