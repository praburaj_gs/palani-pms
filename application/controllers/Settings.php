<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->table="alert_settings";
		
	}

	public function index()
	{
		/*if($this->fb_rest->isloggedin()){
			$this->load->view('settings_content');
		}	*/		
		
		  if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("settings")) {
				$table_name=$this->table;
				$data = array();
				$table_name = "alert_settings_dg";
				$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
					"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
				$msg2  = $this->fb_rest->getlist_record($params);
				
				$table_name = "alert_settings";
				$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
					"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
				$msg  = $this->fb_rest->getlist_record($params);
				
				//fb_pr($msg);
				$table_name1 = "alert_notify";
				$params1 =  array("page_no" => 1, "per_page" =>10, "uri_segment" => "2",
					"search" => "", "sort_fld" => "createdtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name1);
				$msg1 = $this->fb_rest->getlist_record($params1);
				
				if($msg["status"] == "success" && $msg1['status']== 'success' && $msg2['status']== 'success')
				{
					$data["result_set"] = $msg["result_set"];
					$data["result_set1"] = $msg1["result_set"];
					$data["dgresult_set"] = $msg2["result_set"];
					//fb_pr($data);
					$fuleData=$this->fb_rest->getfuelSettings();

					$data['fuel_set'] = $fuleData;
					//fb_pr($data); exit();
					$this->load->view('settings_content', $data);
				}
				else{
					$this->load->view("layout/error", $data);
				}
			} else {
				$this->load->view('alert/permission');
			}
		} else{
			redirect('/login');
		} 
	}
	
	public function create(){
		$table_name=$this->table;
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_settings*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		//print_r($form_data); exit();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_success',"Success, EB Settings are updated");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('c_failed',"Oops,something went wrong,Please try again");
		redirect('/settings');
		}
	}
    
	public function create_genset(){
		$table_name="alert_settings_dg";
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_settings_dg*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		//print_r($form_data); exit();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_dg_success',"Success, DG Settings are updated");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('c_dg_failed',"Oops,something went wrong,Please try again");
		redirect('/settings');
		}
	}
	
	public function add_record(){
		$table_name="alert_notify";
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_notify*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('a_success',"Success, notification settings added successfully");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('a_failed',"Oops!,something went wrong,Please try again");
			redirect('/settings');
		}
	}
	
	
	function delete(){
		// echo "welcome";
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"Success, deleted notification settings");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('delete_failed',"Oops,something went wrong,Please try again");
			redirect('/settings');
		}
	}
	
	function edit(){
		$data = array();
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		//fb_pr($record);
		if($record["status"] == "success")
		 {	
		 	$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("settings_edit_content", $data);
		 }
		
	}
	function update(){
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$form_data = $this->input->post();
		$form_data['updatedtime']=now();	
		//$built_date = $this->input->post("built_date", true);
		
		$rkey = $this->input->post("rkey");

		$oresult = $this->fb_rest->get_record($table_name, $rkey);
		$orecord = $oresult["result_set"];

		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('update_success',"Settings updated successfully");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('update_failed',"Oops! there is something wrong, try again");
		redirect('/settings');
		}
	}

	function fuelSettings(){

		$table_name="dgfuel_setting";
		//$tbl_id= 350401;
		fb_clear_cache(array("*dgfuel_setting*"));
		$form_data['twentyfive'] = $this->input->post('qtr_ltr');
		$form_data['fifty'] = $this->input->post('half_ltr');
		$form_data['seventyfive'] = $this->input->post('threefour_ltr');
		$form_data['hundred'] = $this->input->post('full_ltr');
		$form_data['hundredmore'] = $this->input->post('fullover_ltr');
		$form_data['meter_id'] = $this->input->post('fuel_meterid');
		$form_data['createdtime']=time();
		$form_data['updatedtime']=time();
		//print_r($form_data); exit();
		$updateId = $this->input->post('updateId');
		if($updateId=="")
		$result = $this->fb_rest->create_record($table_name,$form_data);
		else
		$result = $this->fb_rest->update_record($table_name,$form_data,$updateId);
//		print_r($result);
		if($result['status']=="success"){
			echo "success";
		}else{
			echo "failed";
		}
	}

	function getfuelSettings(){
		$table_name="dgfuel_setting";
		fb_clear_cache(array("*dgfuel_setting*"));
		$id = $this->input->post('meter_id');
		$fuelSettings = $this->iot_rest->getfuelSettings($id); 

		$arrData = array_merge(array("data"=>$fuelSettings['data']), array("_id"=>$fuelSettings['id']));
		//print_r($arrData); exit();

		if($fuelSettings['status']=="success"){

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrData));
		}
	}

	
}
