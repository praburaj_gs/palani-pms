<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alerts extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$data = array();
			
			
			$table_name = "alerts";
			$table_name = "alerts";
				$from = 0;
				$size = 10000;
				$orderfld = "createdtime";
				$orderdir = "desc";
				$this->load->library('parser');
				$this->parser->set_delimiters("__","__");
				$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
				$query_str = $this->parser->parse('query/get_unread_alert', $qpms, true);
				$result = $this->fb_rest->get_query_result($table_name, $query_str);
		
  		    //$this->load->view('include/header');
			$this->load->view('include/left_menu');
			 
			//fb_pr($msg);
			if($result["status"] == "success")
			{
				$data["total_count"] = $result["total_count"];
				$data["page_links"] = $result["page_links"];
				$data["result_set"] = $result["result_set"];
				$this->load->view("include/header", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	
	}
	public function list_record(){
	$table_name = "alerts";
	$this->fb_rest->test_list_record($table_name);
	}
	public function list_all_alerts(){
		if($this->fb_rest->isloggedin()){
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');

				$data = array();
				$page_no = $this->uri->segment('3'); 
				$per_page = $this->input->get_post("no_items", true);
				$search = $this->input->get_post("search", true);
				$sort_fld = $this->input->get_post("sort_fld", true);
				$sort_dir = $this->input->get_post("sort_dir", true);
				$min_date = $this->input->get_post("min_date", true);
				$max_date = $this->input->get_post("max_date", true);
				
				$minDate= strtotime($min_date) * 1000; 
				$maxDate= strtotime($max_date) * 1000;
				$page_burl = site_url("/alerts/list_all_alerts");
				$this->table="g_notifications";
				$table_name = $this->table;
				$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "3",
				"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$minDate,"max_date"=>$maxDate);
				
				$data["sort_fld"] = $sort_fld;
				$data["sort_dir"] = $sort_dir;
				$data["search"] = $search;
				$data["per_page"] = $per_page;
				
				$sort_columns = array("createdtime", "ntype", "nmsg");
				//echo fb_text("feed_stock_distribution_list"); 
				$hstr = array("createdtime" =>"Date - Time","meter" => "Meter", "ntype" => "Alert Type", "nmsg" => "Alert Message","action" => "Action");
				
				$theader = "";
				
				foreach($hstr as $hk => $hv)
				{
					if(in_array($hk, $sort_columns)){
						$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
						$pstr = (!empty($per_page)) ? $per_page : "10";
						$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
						$srt_str = http_build_query($srt_params);
						$srt_url = site_url("/alerts/list_all_alerts?".$srt_str);
						$cdir_icon = "";
						if(!empty($sort_fld)){
							$cdir_icon = ($hk == $sort_fld) ? 
							(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
						}
						$thstr = $hv.$cdir_icon;
						$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
						$theader .= $thtml."\n";
					}else{
						$theader .= "<th>$hv</th>\n";
					}
				}
				
				$data["theader"] = $theader;
				$msg  = $this->fb_rest->alert_list($params);
		
				if($msg["status"] == "success")
				{
					$data["page_links"] = $msg["page_links"];
					$data["result_set"] = $msg["result_set"];
					$this->load->view("alerts_content", $data);
				}else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
				
			}else{
				redirect('/login');
			}
	}
	function delete(){
		$table_name="alerts";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			//$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			redirect('/alerts/list_all_alerts');
		}else{
			//$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('/alerts/list_all_alerts');
		}
	}
	function view_delete(){
		$this->load->view('include/header');
		$this->load->view('include/left-sidebar');
		$this->load->view("view_delete");
		$this->load->view('include/footer');
	}

	public function download(){
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();

			//fb_pr($result); exit;
	        $this->load->view('include/header');
	        $this->load->view('include/left-sidebar');

			$this->parser->set_delimiters("__","__");
			$rec_id = fb_fetch_id("g_notifications"); // "351200";


			$pno = $this->uri->segment(4, 0);
			
			$from = ($pno <= 1) ? "0" : ( $pno - 1 ) ;
			$from = 1;
			
			$min_date = $this->input->get_post("min_date",true);  
			$max_date = $this->input->get_post("max_date",true);
			$hr = $this->input->get_post("hr",true);

			$minDate= strtotime($min_date) * 1000; 
			$maxDate= strtotime($max_date) * 1000;

			$download = $this->input->get_post("download"); 


			$data=array();
			$presult_data = array();
		
				$tot_cnt = $this->fb_rest->get_mtotal_alerts($rec_id); 

				if($tot_cnt>=10000){
					$tot_cnt= 9999;
				}
				else if($download=="pdf"){
					$tot_cnt = 8000;
				}
				else{
					$tot_cnt = $tot_cnt;
				}

		
				$result = $this->iot_rest->alertsFilters_records($minDate,$maxDate,$from,$tot_cnt);
				
				$meterList = $this->iot_rest->getMeters();
				$meterNames = $meterList['data'];
				if($result["status"] == "success"){
					for($i=0;$i<count($result['data']);$i++)
					{
						$datetime= fb_convert_jsdate($result['data'][$i]['_source']['createdtime']);
 						$custom_pms = json_decode($result['data'][$i]['_source']['custom_pms']);
                         $meter_id = $custom_pms->meter_id;
                         $meter_name = $meterNames[$meter_id];
						$presult_data[$i]=array_merge(array('createdtime'=>$datetime),array('meter'=>$meter_name),array('nmsg'=>$result['data'][$i]['_source']['nmsg']));
					}

					   $data["status"] = "success";
					   $data["result_set"] = $presult_data;
					   $data["title"] = "Download Alerts Reports";
					   $data['download_file'] = $filename;
					   $data['format'] = $download;
					   $data['min_date'] = $min_date;
					   $data['max_date'] = $max_date;

		    		 $this->load->view("download_alerts", $data);
			}
			  
			else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view("include/footer");
	}else{
		redirect('/login');
	}
}	
}
