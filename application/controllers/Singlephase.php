<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Singlephase extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
		if($this->fb_rest->isloggedin()){
			$this->load->view('singlephase');			
		}else{
			redirect('/login');
		}
	}
    
    public function formulaPower($phase){
        $PF = 0.705;
        $A = 2;
        $V = 440;
        if($phase==1){
            $kW = round($PF * $A * $V / 1000,2); 
        }else{
            $kW = round(pow(3,1/3) * $PF * $A * $V / 1000,2); 
        }
        
        return $kW;
    }
    
    public function formulaKVA($phase){
        $PF = 0.705;
        $A = 2;
        $V = 440;
        if($phase==1){
            $kVA = round(($V * $phase) /1000,2);
        }else{
            $kVA = round(($V * $phase * 1.732) /1000,2);
        }
        
        return $kVA;
    }    
    	
}
