<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	private $role_copt;

	function __construct()
    {
		parent::__construct();
		$this->load->library("form_validation");
		$this->load->helper("form");
		$this->role_copt = array("" => "--Select--", "manager" => "Manager", "supervisor" => "Supervisor", "technician" => "Technician");
	}
	
	public function manage_users(){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("user_management")) {
				$data = array();
				$udetails = array(
					'e_role' => true,
					'api_key' => $this->fb_rest->getApiKey()
				);
				$aresult = $this->fb_rest->send_api_request("user/list_users", $udetails);
				$data["uresult"] = array();
				$data["add_user"] = "false";
				if($aresult["status"]=="success"){
					$data["uresult"] = $aresult["result"];
					$data["add_user"] = $aresult["add_user"];
				}
				$this->load->view('user/manage-users', $data);
			} else {
				$this->load->view('alert/permission');
			}
		}else{
			redirect('/login');
		}
	}
	
	public function manage_permissions(){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("user_management")) {
				$data = array();
				$udetails = array(
					'e_role' => true,
					'api_key' => $this->fb_rest->getApiKey()
				);
				$apudetail = array();
				$aresult = $this->fb_rest->send_api_request("user/list_permissions", $udetails);
				$data["pgroups"] = $groups = $aresult["pgroups"];
				$data["modules_list"] = $aresult["modules_list"];
				$data["permissions_list"] = $permissions_list = $aresult["permissions_list"];
				$amplist = array();
				foreach($aresult["permissions_list"] as $pmrow){
					$amplist[$pmrow['module']][] = $pmrow;
				}
				$data["amplist"] = $amplist;
				$mng_per_page = $this->input->post("mng_per_page", true);
				if(!empty($mng_per_page)){
					$upm = $this->input->post("upm");
					
					foreach($groups as $group){
						$group_id = $group["role_id"];
						$rln = $group["role_name"];
						$gpms = isset($upm[$rln]) ? $upm[$rln] : array();
						if(!empty($gpms)){
							$spms = implode(",", array_keys($gpms) );
						}
						$udata = array("role_id" => $group_id, "role_permissions" => $spms, "updatedtime" => time());
						$apudetail[$group_id] = $udata;
					}
					if(!empty($apudetail)){
						$pdetails = json_encode($apudetail);
						$updet = array_merge($udetails, array("pdetails" => $pdetails) );
						$aresult = $this->fb_rest->send_api_request("user/update_permission", $updet);
						fb_clear_cache(array("*fetch_permissions*"));
						if ( $aresult["status"] == "success" )
						{
							$data["success_msg"] = "Permissions have been updated successfully.";
						}
					}
					
				}
				
				$cpm_rst = $this->fb_rest->send_api_request("user/get_urolepms", $udetails);
				$acpm = array();
				if($cpm_rst["status"]=="success"){
					$ccpm = $cpm_rst["result"];
					foreach($ccpm as $ccpm_r){
						 $rpms = explode("," , $ccpm_r["role_permissions"]);
						 foreach($rpms as $rpm){
							$acpm[$ccpm_r["role_name"]] [$rpm] = $rpm;
						 }
					}
				}
				$data["acpm"] = $acpm;
				
				$this->load->view('user/manage-permissions', $data);
			} else {
				$this->load->view('alert/permission');
			}
		}else{
			redirect('/login');
		}
	}
	
	public function add_user(){
		if($this->fb_rest->isloggedin()){
			$data = array();
			$this->form_validation->set_rules('firstname', 'firstname', 'required|min_length[2]|max_length[50]');
			$this->form_validation->set_rules('lastname', 'lastname', 'max_length[50]');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[50]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
			$this->form_validation->set_rules('mobile', 'Mobile', 'regex_match[/^[0-9]{10}$/]');
			$this->form_validation->set_rules('user_role', 'User Role', 'required');
			
			$data["role_copt"] = $this->role_copt;
			
			if ($this->form_validation->run() !== FALSE) {
				$udetails = array (
				   "firstname" => $this->input->post("firstname", true),
				   "lastname" => $this->input->post("lastname", true),
				   "email" => $this->input->post("email", true),
				   "password" => $this->input->post("password", true),
				   "mobile" => $this->input->post("mobile", true),
				   "user_role" => $this->input->post("user_role", true),
				   "api_key" => $this->fb_rest->getApiKey()
				);
				$aresult = $this->fb_rest->send_api_request("user/adduser", $udetails);
				if($aresult["status"]=="success"){
					$this->session->set_flashdata('success_msg', 'User has been created successfully.');
					redirect("user/manage_users");
				}else{
					$data["error_msg"] = "API has some issues. You can try after some times.";
					$this->load->view('user/user-form',$data);
				}
			}else{
				$this->load->view('user/user-form',$data);
			}
			
			
		}else{
			redirect('/login');
		}
	}
	
	
	public function edit_user(){
		$data = array();

		if($this->fb_rest->isloggedin()){
			$udetails = array(
				'e_role' => true,
				'api_key' => $this->fb_rest->getApiKey(),
				'email' => $this->input->get('email', true)
			);
			$aresult = $this->fb_rest->send_api_request("user/get_user", $udetails);
			if($aresult["status"]=="success"){
				$data["user_row"] = $user_row = $aresult["user_row"];
				$data["cparams"] = json_decode($user_row["custom_params"], true);
				$data["role_copt"] = $this->role_copt;
				
				$this->form_validation->set_rules('firstname', 'firstname', 'required|min_length[2]|max_length[50]');
				$this->form_validation->set_rules('lastname', 'lastname', 'max_length[50]');
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('password', 'Password', 'min_length[8]|max_length[50]');
				$this->form_validation->set_rules('passconf', 'Password Confirmation', 'matches[password]');
				$this->form_validation->set_rules('mobile', 'Mobile', 'regex_match[/^[0-9]{10}$/]');
				$this->form_validation->set_rules('user_role', 'User Role', 'required');
				
				if ($this->form_validation->run() !== FALSE) {
					$udetails = array (
					   "firstname" => $this->input->post("firstname", true),
					   "lastname" => $this->input->post("lastname", true),
					   "email" => $this->input->post("email", true),
					   "password" => $this->input->post("password", true),
					   "mobile" => $this->input->post("mobile", true),
					   "user_role" => $this->input->post("user_role", true),
					   "api_key" => $this->fb_rest->getApiKey(),
					   "old_role" => $user_row["role"]
					);
					$aresult = $this->fb_rest->send_api_request("user/edituser", $udetails);
					if($aresult["status"]=="success"){
						$this->session->set_flashdata('success_msg', 'User has been updated successfully.');
						redirect("user/manage_users");
					}else{
						$data["error_msg"] = "API has some issues. You can try after some times.";
						$this->load->view('user/user-form',$data);
					}
				}else{
					$this->load->view('user/edit-user-form', $data);
				}
				
			}
		} else {
				redirect('/login');
		}
	}
	
	public function delete_user(){
		if($this->fb_rest->isloggedin()){
			$udetails = array(
					'api_key' => $this->fb_rest->getApiKey(),
					'email' => $this->input->get('email', true)
				);
			$aresult = $this->fb_rest->send_api_request("user/delete_user", $udetails);
			
			if($aresult["status"]=="success"){
				$this->session->set_flashdata('success_msg', 'User has been deleted successfully.');
			} else {
				$this->session->set_flashdata('error_msg', 'API has some issues.');
			}
			redirect("user/manage_users");
		} else {
				redirect('/login');
		}
		
	}
	
	
}

