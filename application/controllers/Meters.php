<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meters extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->session->keep_flashdata('update_success');
		$this->session->keep_flashdata('update_failed');				
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_dg")) {
				$data=array();
				$presult_data = array();
				$rec_id = fb_fetch_id("meters"); // "350414";
				$result = $this->iot_rest->getmeter_list($rec_id);
				//print_r($result); exit();
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if($result["status"] == "success"){
					
					
					$data["presult_data"] = $result["data"];
					$this->load->view('meters',$data);	
				}
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}
	}

	public function add_meter(){
		
		$table_name = "meters";
    	$config['upload_path'] = './meter-images/';
		$config['allowed_types'] = 'gif|jpg|png'; 

        $this->load->library('upload', $config);

		$this->upload->do_upload('meter_image');

        /*if($this->upload->do_upload('meter_image')){
			$data = $this->upload->data();
     		$meter_image = $data['file_name'];
		}else{
			$error = array('error' => $this->upload->display_errors());
   			log_message('info', json_encode($error));
		}*/		

		$data = $this->upload->data();
     	$meter_image = $data['file_name'];

     	 $action = $this->input->post("action");
     	 $rid = $this->input->post("rid");
		 $meter_id = $this->input->post("meter_id");
		 $meter_name = $this->input->post("meter_name");
		 $meter_desc = $this->input->post("meter_desc");
		 $running_hrs = $this->input->post("running_hrs");
		 $kva = $this->input->post("kva");

     	 if($action=="update" && $rid){
     	 	$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['name']=$meter_name;
     	 	$form_data['description'] = $meter_desc;
     	 	$form_data['running_hrs'] = $running_hrs;
     	 	if($meter_image)
     	 	$form_data['image'] = $meter_image;
     	 	//print_r($form_data);  exit();
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rid);
     	 }else{
			
			$idata = array("meter_id" => $meter_id, 
			"name" => $meter_name, 
			"description" => $meter_desc,
			"image" => $meter_image,
			"status" => "true",
			"kva" => $kva,
			"running_hrs"=>$running_hrs,
			"createdtime" => time(), 
			"updatedtime" => time());
			//print_r($idata); exit();
			$result = $this->fb_rest->create_record($table_name, $idata);     	 	
     	 }

			if($result['status']=="success"){
				$this->session->set_flashdata('meter_success','meter added/updated successfully');
				redirect('/meters');
			}else{
				$this->session->set_flashdata('meter_failed','please try again later');
				redirect('/meters');
			}

	}	

	public function delete(){
		$table_name="meters";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"meter deleted");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('delete_failed',"issue deleting meter");
			redirect('/meters');
		}
	}

	public function updateStatus($rid,$status){
		$table_name="meters";
		$form_data =  array();
		if ($status=="false") {
			$form_data['status']="false";	
		}else{
			$form_data['status']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			if($status=="false")
				$this->session->set_flashdata('update_success',"Meter has been de-activated successfully");
			else
				$this->session->set_flashdata('update_success',"Meter has been activated successfully");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('update_failed',"meter status update failed");
			redirect('/meters');
		}
	}	

	public function addBcheck(){
		$table_name="bcheck_settings";
		//$tbl_id= 350401;
		$date = strtotime($this->input->post('next_date')) *1000;
		$hours=$this->input->post('next_hours');
		$meter_id= $this->input->post('meter_id');

		$idata = array("meter_id" => $meter_id, 
			"date" => $date, 
			"hours" => $hours,
			"createdtime" => now(), 
			"updatedtime" => now());
		$result = $this->fb_rest->create_record($table_name,$idata);
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($result));
			
		
	}	

	public function getBcheck(){
	
		$meter_id = $this->input->post('meter_id');
		$result = $this->iot_rest->getBcheck($meter_id);
		 if($result["status"] == "success" && !(empty($result['data']))){
			$result['date'] =  fb_convert_date_time_format($result['data']['date']);
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($result));
			}else{
				$result['date'] =  0;
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($result));
			}
	}	

	public function bcheckAlert($meter_id){

		$result = $this->iot_rest->getBcheck($meter_id);
		$date =  fb_convert_jsdate($result['data']['date']);
		$alertdate= date("m/d/Y h:i:s a",strtotime($date." -3 day"));
		//echo "Date ".$result['data']['date']."<br>";
		$today =  date("m/d/Y h:i:s a",now());
		//$minDate = 
		if($today>$alertdate){
			echo "Trigger alert";
		}else{
			echo "RIP";
		}
		
	}		
}
?>