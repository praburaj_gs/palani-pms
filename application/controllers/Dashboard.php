<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		//$this->output->enable_profiler(TRUE);
	}


	public function index()
	{
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("dashboard")) {
				$query = $this->db->query('SELECT * FROM dashboard ORDER BY position ASC');
				$data['result'] = $query->result();
				$presult_data = array();
				$rec_id = fb_fetch_id("meters"); // "350414";
				$result = $this->iot_rest->activemeter_list($rec_id);
				//print_r($result);
				if($result["status"] == "success"){
					$data["presult_data"] = $result["data"];
				}

				$this->load->view('dashboard_view',$data);
			} else {
				$this->load->view('alert/permission');
			}	
		}else{
			redirect('/login');
		}	

	}

	public function view($id=null){
				
		if($this->fb_rest->isloggedin()){

			if($this->fb_rest->has_accessable("dashboard_individual")) {
				if($id){
					$dataArr= array();
					$query = $this->db->query('SELECT * FROM dashboard ORDER BY position ASC');
					$data['result'] = $query->result();
					//tot voltage
					$totVolt= $this->getVoltValue($id);
					//kva value
					$kvaValue = $this->getKVA($id);
					//amp value
					$ampValue = $this->getAmpValue($id);
					//frequncy value
					$freValue = $this->getFreqValue($id);
					//dg kva max
					$dgMax = $this->dgKVA_Max($id);
					//dg load
					$dgLoad = $this->dgKVA_Load($id);

					//dgStatus
					$dgStatus = $this->currentDGstatus($id);
					//print_r($dgStatus); exit();

					$dataArr = array_merge(array('result' => $data['result']),array('meter_id'=>$id),array('totVolt'=>$totVolt),array('kvaVal'=>$kvaValue),array('ampVal'=>$ampValue),array('freVal'=>$freValue),array('dgMax'=>$dgMax),array('dgLoad'=>$dgLoad),array('dgStatus'=>$dgStatus));
					//print_r($dataArr); exit();
					$this->load->view('dashboard',$dataArr);
				}else{
					$this->load->view('include/header');
					$this->load->view('include/left-sidebar');
					$this->load->view("layout/no-meter");
					$this->load->view('include/footer');
				}
			} else {
					$this->load->view('alert/permission');
			}	
			
		}else{
			redirect('/login');
		}		
	
	}

    public function singlePower(){
    	$power = $this->getsinglewattsValue(); 
        return $power;      
    }
    
    public function formulaKVA(){
    	$id = $this->input->post('meter_id');

        $A = $this->getAmpValue($id);
        $V = $this->getVoltValue($id);
        $kVA = $A * $V * 1.732 ; 

        echo round($kVA/1000,2);
    }

    public function getKVA($id){
        $totalkva = $this->iot_rest->getkva($id);
        //print_r($totalkva); exit();
        return $totalkva['kva'];  	
    }

    public function getFreqValue($id){
    	$result= $this->iot_rest->getFrequency($id);
    	$data = $result['data'];
    	if($result["status"]=="success"){
    	  return $data;
    	}else{
    		return 0;
    	}
    }
    
	
	  public function kvaAjax(){
	  	$phase = $this->input->post('phase');
        $PF = $this->getPowerfactor();
        $A = $this->getAmpValue();
        $V = $this->getVoltValue();

        if($phase==1){
            $kVA = round(($A * $V ) /1000,2);
        }else{
            $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 
        }
        
        echo $kVA;
    }
	public function get_message(){
		$rec_id = fb_fetch_id("power-meter"); // "350412";
		$meter_id = $this->input->get('meter_id');
		$result = $this->iot_rest->get_power_detail($meter_id);
				
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
	
	
	public function get_last_message(){
		$msg_id = "350405";
		$apiInstance = new Swagger\Client\Api\ListDeviceMessagesApi(
			// If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
			// This is optional, `GuzzleHttp\Client` will be used as default.
			new GuzzleHttp\Client()
		);
		$atoken = $this->fb_rest->get_fbuser_data("user_token"); // string | Auth token of the logged in user
		$page_size = 1; // int | Maximum number of messages to be listed
		$direction = ""; // string | If direction is specified, **muid** is required
		$muid = $msg_id; // string | Last or First message uuid of the previous list operation, **required** if **direction** is specified

		try {
			$result = $apiInstance->listDeviceMessages($atoken, $page_size, $direction, $muid);
			$mobj = isset($result["0"]) ? $result["0"] : "";
			if(!empty($mobj)){
				$mdata = $mobj->getData();
				$nuuid = $mobj->getNodeUuid();
				fb_pr($result);
				fb_pr($mdata);
				echo "Node uuid: $nuuid <br/>";
				
			}else{
				echo "Empty Message";
			}
			
		} catch (Exception $e) {
			echo 'Exception when calling ListDeviceMessagesApi->listDeviceMessages: ', $e->getMessage(), PHP_EOL;
		}
	}
	
	public function get_new_message(){
		$msg_id = fb_fetch_id("power-meter"); // "350412";
		$result = $this->iot_rest->get_message($msg_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
    
    public function getVolt(){
    	$meter_id = $this->input->post('meter_id');
        $result = $this->iot_rest->get_totVolt($meter_id);

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getVoltValue($id){
        $result = $this->iot_rest->get_totVolt($id);

        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }  
    
     public function getVoltSingle(){
        $result = $this->iot_rest->get_singleVolt();
       if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }   
    
    
     public function getVoltSinglegauge(){
        $result = $this->iot_rest->get_singleVolt();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getAmp(){
    	$meter_id = $this->input->post('meter_id');
        $result = $this->iot_rest->get_totAmp($meter_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
     public function getAmpValue($id){
        $result = $this->iot_rest->get_totAmp($id);
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getPowerfactor($id){
        $result = $this->iot_rest->getPowerfactor($id);
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getsinglewattsValue()
    {
        
        $result = $this->iot_rest->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rp=  $result['data']['rp'];
			$bp= $result['data']['bp'];
			$yp = $result['data']['yp'];
        	$arrPower = array();
        	array_push($arrPower, $rp,$bp,$yp);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }

    public function gettotPower()
    {
        
        $result = $this->iot_rest->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
			$tp = $result['data']['tp'];
        	return $tp;
		}else{
			return 0;
		}
    }
    
    public function voltampGraph(){
        	$id = $this->input->post('meter_id');
            $data=array();
			$presult_data = array();
			$rec_id = fb_fetch_id("power-meter"); // "350412";
			$result = $this->iot_rest->get_power_list($id);
			//fb_pr($result);
			//print_r($result);
			if($result["status"] == "success"){
			$pdata = $result["data"];
			
             $this->output
			->set_content_type('application/json')
			->set_output(json_encode($pdata));
			
			}
    }
    
    public function voltampMsg(){
        $id = $this->input->post('meter_id');
        $rec_id = fb_fetch_id("power-meter"); // "350412";
		$result = $this->iot_rest->get_power_detail($id);
				
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
        
    }
    
	public function test_message()
	{
	$result = array();
	$record = array();
	$msg_id = fb_fetch_id("power-meter"); // "350412";
	$result = $this->iot_rest->get_message($msg_id);
	$row = $result['data'];

	if ($row['gen'] == 0)
		{
		$id = $this->fb_rest->get_total_count("alerts");
		$table_name = "alert_settings";
		$params = array(
			"per_page" => 1,
			"sort_fld" => "createdtime",
			"sort_dir" => "desc",
			"table_name" => $table_name
		);
		$msg = $this->fb_rest->list_record($params);
		if ($msg['status'] == "success")
			{
			$record = $msg['result_set'];
			foreach($record as $rcd)
				{
				$src = $rcd['_source'];
				}
			}

		if ($row['tv'] > $src['spmin'])
			{
			if (($row['rv'] > $src['rspmin'] && $row['bv'] > $src['bspmin']) || ($row['rv'] > $src['rspmin']))
				{
				if ($row['rv'] > $src['rspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['bv'] > $src['bspmin'] && $row['yv'] > $src['yspmin']) || ($row['yv'] > $src['yspmin']))
				{
				if ($row['yv'] > $src['yspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['bv'] > $src['bspmin'] && $row['bv'] > $src['rspmin']) || ($row['bv'] > $src['bspmin']))
				{
				if ($row['bv'] > $src['bspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			}

		if ($row['tv'] < $src['spmax'])
			{
			if (($row['rv'] < $src['rspmax'] && $row['bv'] < $src['bspmax']) || ($row['rv'] < $src['rspmax']))
				{
				if ($row['rv'] < $src['rspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['bv'] < $src['bspmax'] && $row['yv'] < $src['yspmax']) || ($row['yv'] < $src['yspmax']))
				{
				if ($row['yv'] < $src['yspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['bv'] < $src['bspmax'] && $row['rv'] < $src['rspmax']) || ($row['bv'] < $src['bspmax']))
				{
				if ($row['bv'] < $src['bspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			}

		if ($row['tpv'] > $src['tpmin'])
			{
			if (($row['rbv'] > $src['rtpmin'] && $row['byv'] > $src['btpmin']) || ($row['rbv'] > $src['rtpmin']))
				{
				if ($row['rbv'] > $src['rtpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['byv'] > $src['btpmin'] && $row['yrv'] > $src['ytpmin']) || ($row['yrv'] > $src['ytpmin']))
				{
				if ($row['yrv'] > $src['tpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			elseif (($row['byv'] > $src['btpmin'] && $row['rbv'] > $src['ttpmin']) || ($row['byv'] > $src['btpmin']))
				{
				if ($row['byv'] > $src['btpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				}
			}

		if ($row['tpv'] < $src['tpmax'])
			{
			if (($row['rbv'] < $src['rtpmax'] && $row['byv'] < $src['btpmax']) || ($row['rbv'] < $src['rtpmax']))
				{
				if ($row['rbv'] < $src['rtpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					// fb_pr($result);

					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] < $src['btpmax'] && $row['yrv'] < $src['ytpmax']) || ($row['yrv'] < $src['ytpmax']))
				{
				if ($row['yrv'] < $src['ytpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] < $src['btpmax'] && $row['rbv'] < $src['rtpmax']) || ($row['byv'] < $src['btpmax']))
				{
				if ($row['byv'] < $src['btpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tc'] > $src['tcpmin'])
			{
			if (($row['rc'] > $src['rcpmin'] && $row['bc'] > $src['bcpmin']) || ($row['rc'] > $src['rcpmin']))
				{
				if ($row['rc'] > $src['rcpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] > $src['bcpmin'] && $row['yc'] > $src['ycpmin']) || ($row['yc'] > $src['ycpmin']))
				{
				if ($row['yc'] > $src['ycpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] > $src['bcpmin'] && $row['rc'] > $src['rcpmin']) || ($row['bc'] > $src['bcpmin']))
				{
				if ($row['bc'] > $src['bcpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tc'] < $src['tcpmax'])
			{
			if (($row['rc'] < $src['rcpmax'] && $row['bc'] < $src['bcpmax']) || ($row['rc'] < $src['rcpmax']))
				{
				if ($row['rc'] < $src['rcpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] < $src['bcpmax'] && $row['yc'] < $src['ycpmax']) || ($row['yc'] < $src['ycpmax']))
				{
				if ($row['yc'] < $src['ycpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] < $src['bcpmax'] && $row['rc'] < $src['rcpmax']) || ($row['bc'] < $src['bcpmax']))
				{
				if ($row['bc'] < $src['bcpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tp'] > $src['tppmin'])
			{
			if (($row['rp'] > $src['rppmin'] && $row['bp'] > $src['bppmin']) || ($row['rp'] > $src['rppmin']))
				{
				if ($row['rp'] > $src['rppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				}
			elseif (($row['bp'] > $src['bppmin'] && $row['yp'] > $src['yppmin']) || ($row['yp'] > $src['yppmin']))
				{
				if ($row['yp'] > $src['yppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] > $src['bppmin'] && $row['rp'] > $src['rppmin']) || ($row['bp'] > $src['bppmin']))
				{
				if ($row['bp'] > $src['bppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tp'] < $src['tppmax'])
			{
			if (($row['rp'] < $src['rppmax'] && $row['bp'] < $src['bppmax']) || ($row['rp'] < $src['rppmax']))
				{
				if ($row['rp'] < $src['rppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				}
			elseif (($row['bp'] < $src['bppmax'] && $row['yp'] < $src['yppmax']) || ($row['yp'] < $src['yppmax']))
				{
				if ($row['yp'] < $src['yppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] < $src['bppmax'] && $row['rp'] < $src['rppmax']) || ($row['bp'] < $src['bppmax']))
				{
				if ($row['bp'] < $src['bppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['fre'] > $src['fremin'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "High Frequency",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['fre'] < $src['fremax'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "High Frequency",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['pf'] > $src['pfmin'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "Problem Power Factor ",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['pf'] < $src['pfmax'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "Problem Power Factor ",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}
		}
	  else
		{
		$id = $this->fb_rest->get_total_count("alerts");
		$table_name = "alert_settings_dg";
		$params = array(
			"per_page" => 1,
			"sort_fld" => "createdtime",
			"sort_dir" => "desc",
			"table_name" => $table_name
		);
		$msg = $this->fb_rest->list_record($params);
		if ($msg['status'] == "success")
			{
			$record = $msg['result_set'];
			foreach($record as $rcd)
				{
				$src = $rcd['_source'];
				}
			}

		if ($row['tv'] > $src['dgspmin'])
			{
			if (($row['rv'] > $src['dgrspmin'] && $row['bv'] > $src['dgbspmin']) || ($row['rv'] > $src['dgrspmin']))
				{
				if ($row['rv'] > $src['dgrspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bv'] > $src['dgbspmin'] && $row['yv'] > $src['dgyspmin']) || ($row['yv'] > $src['dgyspmin']))
				{
				if ($row['yv'] > $src['dgyspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bv'] > $src['dgbspmin'] && $row['rv'] > $src['dgrspmin']) || ($row['bv'] > $src['dgbspmin']))
				{
				if ($row['bv'] > $src['dgbspmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				}
			}

		if ($row['tv'] < $src['dgspmax'])
			{
			if (($row['rv'] < $src['dgrspmax'] && $row['bv'] < $src['dgbspmax']) || ($row['rv'] < $src['dgrspmax']))
				{
				if ($row['rv'] < $src['dgrspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bv'] < $src['dgbspmax'] && $row['yv'] < $src['dgyspmax']) || ($row['yv'] < $src['dgyspmax']))
				{
				if ($row['yv'] < $src['dgyspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bv'] < $src['dgbspmax'] && $row['rv'] < $src['dgrspmax']) || ($row['bv'] < $src['dgbspmax']))
				{
				if ($row['bv'] < $src['dgbspmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tpv'] > $src['dgtpmin'])
			{
			if (($row['rbv'] > $src['dgrtpmin'] && $row['byv'] > $src['dgbtpmin']) || ($row['rbv'] > $src['dgrtpmin']))
				{
				if ($row['rbv'] > $src['dgrtpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] > $src['dgbtpmin'] && $row['yrv'] > $src['dgytpmin']) || ($row['yrv'] > $src['dgytpmin']))
				{
				if ($row['yrv'] > $src['dgtpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] > $src['dgbtpmin'] && $row['rbv'] > $src['dgttpmin']) || ($row['byv'] > $src['dgbtpmin']))
				{
				if ($row['byv'] > $src['dgbtpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tpv'] < $src['dgtpmax'])
			{
			if (($row['rbv'] < $src['dgrtpmax'] && $row['byv'] < $src['dgbtpmax']) || ($row['rbv'] < $src['dgrtpmax']))
				{
				if ($row['rbv'] < $src['dgrtpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] < $src['dgbtpmax'] && $row['yrv'] < $src['dgytpmax']) || ($row['yrv'] < $src['dgytpmax']))
				{
				if ($row['yrv'] < $src['dgytpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['byv'] < $src['dgbtpmax'] && $row['rbv'] < $src['dgrtpmax']) || ($row['byv'] < $src['dgbtpmax']))
				{
				if ($row['byv'] < $src['dgbtpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Voltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tc'] > $src['dgtcpmin'])
			{
			if (($row['rc'] > $src['dgrcpmin'] && $row['bc'] > $src['dgbcpmin']) || ($row['rc'] > $src['dgrcpmin']))
				{
				if ($row['rc'] > $src['dgrcpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] > $src['dgbcpmin'] && $row['yc'] > $src['dgycpmin']) || ($row['yc'] > $src['dgycpmin']))
				{
				if ($row['yc'] > $src['dgycpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] > $src['dgbcpmin'] && $row['rc'] > $src['dgrcpmin']) || ($row['bc'] > $src['dgbcpmin']))
				{
				if ($row['bc'] > $src['dgbcpmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tc'] < $src['dgtcpmax'])
			{
			if (($row['rc'] < $src['dgrcpmax'] && $row['bc'] < $src['dgbcpmax']) || ($row['rc'] < $src['dgrcpmax']))
				{
				if ($row['rc'] < $src['dgrcpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] < $src['dgbcpmax'] && $row['yc'] < $src['dgycpmax']) || ($row['yc'] < $src['dgycpmax']))
				{
				if ($row['yc'] < $src['dgycpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/*fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bc'] < $src['dgbcpmax'] && $row['rc'] < $src['dgrcpmax']) || ($row['bc'] < $src['dgbcpmax']))
				{
				if ($row['bc'] < $src['dgbcpmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Coltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tp'] > $src['dgtppmin'])
			{
			if (($row['rp'] > $src['dgrppmin'] && $row['bp'] > $src['dgbppmin']) || ($row['rp'] > $src['dgrppmin']))
				{
				if ($row['rp'] > $src['dgrppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] > $src['dgbppmin'] && $row['yp'] > $src['dgyppmin']) || ($row['yp'] > $src['dgyppmin']))
				{
				if ($row['yp'] > $src['dgyppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] > $src['dgbppmin'] && $row['rp'] > $src['dgrppmin']) || ($row['bp'] > $src['dgbppmin']))
				{
				if ($row['bp'] > $src['dgbppmin'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['tp'] < $src['dgtppmax'])
			{
			if (($row['rp'] < $src['dgrppmax'] && $row['bp'] < $src['dgbppmax']) || ($row['rp'] < $src['dgrppmax']))
				{
				if ($row['rp'] < $src['dgrppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] < $src['dgbppmax'] && $row['yp'] < $src['dgyppmax']) || ($row['yp'] < $src['dgyppmax']))
				{
				if ($row['yp'] < $src['dgyppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			elseif (($row['bp'] < $src['dgbppmax'] && $row['rp'] < $src['dgrppmax']) || ($row['bp'] < $src['dgbppmax']))
				{
				if ($row['bp'] < $src['dgbppmax'])
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "R Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				  else
					{
					$table_name = "alerts";
					$idata = array(
						"alert_id" => $id + 1,
						"alert_state" => "true",
						"msg" => "B and Y Single Phase Poltage is Low",
						"createdtime" => now() ,
						"updatedtime" => now()
					);
					$result = $this->fb_rest->create_record($table_name, $idata);
					$this->send_mail($idata['msg']);
					/* fb_pr($result); */
					}
				}
			}

		if ($row['fre'] > $src['dgfremin'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "High Frequency",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['fre'] < $src['dgfremax'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "High Frequency",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['pf'] > $src['dgpfmin'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "Problem Power Factor",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}

		if ($row['pf'] < $src['dgpfmax'])
			{
			$table_name = "alerts";
			$idata = array(
				"alert_id" => $id + 1,
				"alert_type" => "warning",
				"alert_state" => "true",
				"msg" => "Problem Power Factor",
				"createdtime" => now() ,
				"updatedtime" => now()
			);
			$result = $this->fb_rest->create_record($table_name, $idata);
			$this->send_mail($idata['msg']);
			// fb_pr($result);

			}
		}
	}
	
	public function send_mail($meter_id,$message) { 

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'lalasarjun@gmail.com',
			'smtp_pass' => 'sourashtraclg',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$Useremails=$this->iot_rest->get_alertEmails();
		//print_r($Useremails['data']); exit();
		if(count($Useremails['data'])>0){
			foreach($Useremails['data'] as $row){	
				$source = $row['_source'];
				$toemail = $source['email'];
				$this->email->from('lalasarjun@gmail.com', 'JeyanthiLal');
		        $this->email->to($toemail); 

		        $this->email->subject('Power management alert for'.$meter_id);
		        $this->email->message($message);

				$result = $this->email->send();
		 	}
		}
        //echo $result;
      } 

      public function genset(){

		if($this->fb_rest->isloggedin()){
			$this->load->view('dashboard');			
		}else{
			redirect('/login');
		}

      }

      public function getDG_status($id=null){
      	$rec_id = fb_fetch_id("power-meter"); // "350412";
      	if(!$id)
      	$id= $this->input->post('meter_id');
		$result = $this->iot_rest->getDG($id);
		 if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			$time= $result['data']['createdtime'];
        	$arrPower = array();
        	$arrPower= array_merge(array('status' =>$status) ,array('time'=>$time));
        	//return $arrPower;
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrPower));
		}else{
			return 0;
		}

      }
	
	public function getdg_reports($id) {
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
		
			$result = $this->iot_rest->getdg_record($id);
			//fb_pr($result); exit;
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
            $aggregation_data = array();
			if($result["status"] == "success"){
				
				$data["title"] = "DG Reports";
				//$real_data = $result['aggs'];
				/*foreach($real_data as $row){
					$aggregation_data[] = $row['tops']['hits']['hits'];
					
				}*/
				$data['meter_id'] =$id;
				$data["presult_data"] = $result['data'];
				$this->load->view('reports',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	public function geteb_reports($id) {
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			
			$result = $this->iot_rest->geteb_record($id);
			//fb_pr($result); exit;
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"] == "success"){
				$data["title"] = "EB Reports";
				//$real_data = $result['aggs'];
				/*foreach($real_data as $row){
					$aggregation_data[] = $row['tops']['hits']['hits'];
					
				}*/
				//fb_pr($aggregation_data);exit;
				$data["presult_data"] = $result['data'];
				$this->load->view('reports',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}

	public function dragdrop(){
		$action = $this->input->post('action');
		$updateRecordsArray = $this->input->post('recordsArray');
		$listingCounter = 1; 
		foreach ($updateRecordsArray as $recordIDValue) {

			$this->db->set('position', $listingCounter);
			$this->db->where('id', $recordIDValue);
			$update = $this->db->update('dashboard'); 

	        $listingCounter += 1;

     }
	}


	public function get_meters(){
		$rec_id = fb_fetch_id("meters"); // "350414";
		$result = $this->iot_rest->getmeter_list($rec_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}	
	
	public function get_dg_running_hrs(){

		if($this->fb_rest->isloggedin()){
			
			if($this->fb_rest->has_accessable("dg_running")) {
			//$table_name = "genset_activity";
			$data=array();
			//$from = 0;
			//$size = 10;
			$orderfld = "last_on_time";
			$orderdir = "desc";
			$data = array();
			$id = $this->uri->segment('3');
			$page_no = $this->uri->segment('4'); 
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$min_date = $this->input->get_post("min_date", true);
			$max_date = $this->input->get_post("max_date", true);

			$page_burl = site_url("/dashboard/get_dg_running_hrs/".$id);

			if($min_date && $max_date){
					$minDate = strtoUTC($min_date) * 1000; 
					$maxDate= strtoUTC($max_date) * 1000;
			}

			/*$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"meter_id"=>$id,"min_date"=>$minDate,"max_date"=>$maxDate);
			
			echo $query_str = $this->parser->parse('query/dg_runninghrs', $qpms, true);  exit();

			//$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
			$result = $this->fb_rest->get_query_result($table_name, $query_str);*/
			//print_r($result); exit();

			$page_burl = site_url("/dashboard/get_dg_running_hrs/".$id);
			$this->table="genset_activity";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "4",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$minDate,"max_date"=>$maxDate,"meter_id"=>$id);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("last_on_time", "last_off_time");
			//echo fb_text("feed_stock_distribution_list"); 
			$hstr = array("last_on_time" =>"Start Time", "last_off_time" => "End time",
			"device_name" => "Device Name","running_time" =>"Running Time <small>(HH:MM:SS)</small>","running_hours"=>"Fuel consumption in ltr (approximate)");
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/dashboard/get_dg_running_hrs/".$id."/?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->runninghr_list($params);
			//print_r($msg); exit();
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');

			if($msg["status"] == "success")
			{
				$data["title"] = "DG Running Hours";
				$data['meter_id'] =$id;

				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("dg_running", $data);
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
			
			} else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}

	}

	public function getFrequency(){
		$id = $this->input->post('meter_id');
		$result= $this->iot_rest->getFrequency($id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));		
	}

	public function getrecord_byValue(){

		$id = $this->input->post('meter_id');
		
		$result= $this->iot_rest->get_recordsByValue('1y',$id);

		$data = $result['data'];
		$dataCount = count($data);
		if($result["status"]=="success" && $dataCount>0){
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
		}else{
			echo "Search failed";
		}

	}
	public function get_eb_running_hrs(){
		if($this->fb_rest->isloggedin()){
			$table_name = "eb_status";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "status_on";
			$orderdir = "desc";

			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->parser->parse('query/eb_script_fields', $qpms, true);

	

			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"]=="success"){
				$aggs["aggs"] = $result["aggs"];
				foreach($aggs as $row){
					
					foreach($row as $result){
						$runtime=$result["key"];
						$src = $result['tops']['hits']['hits'];
						foreach($src as $res){
							$temp = $res["_source"];
							$temp["rtime"]=$runtime;
							$source[] = $temp; 
							
						} 
						
					}
					
				}
				$result_set[] = $source;
				
				//fb_pr($result_set); exit;
				$data["aggs"] = $result_set;
				$this->load->view('dg_running',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	public function test(){
		
		$table_name = "genset_activity";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";

			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->parser->parse('query/script_fields', $qpms, true);
			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
		if($result["status"]=="success"){
			$aggs["aggs"] = $result["aggs"];
			foreach($aggs as $row){
				
				foreach($row as $result){
					$runtime=$result["key"];
					
				}
				echo $no_of_hrs = gmdate('H:i:s',$runtime);
			}
			return $no_of_hrs;
		}
		else{
			$this->load->view("layout/error", $data);
		}
    }

    public function dgKVA_Load($id=null){
    	//$amp = 180X1000/1.73X415;
    	if(!$id)
    	$id=$this->input->post('meter_id');
    	$kvaData = $this->iot_rest->getDGkva($id);
    	//print_r($kvaData['data']);
    	$kva= $kvaData['data'];
    	$voltage = $this->getVoltValue($id);
    	$amp = ($kva*1000)/(1.73*$voltage);
    	return round($amp,2);
    }

	public function dgKVA_LoadAjax($id=null){
    	//$amp = 180X1000/1.73X415;
    	if(!$id)
    	$id=$this->input->post('meter_id');
    	$kvaData = $this->iot_rest->getDGkva($id);
    	//print_r($kvaData['data']);
    	$kva= $kvaData['data'];
    	$voltage = $this->getVoltValue($id);
    	$amp = ($kva*1000)/(1.73*$voltage);
    	echo round($amp,2);
    }    

    public function dgKVA_Max($id){
    	$percentage = 80;
		$totalAmp = $this->dgKVA_Load($id);

		$maxAmp = ($percentage / 100) * $totalAmp;
		return $maxAmp;
    }

    public function dgKVA_MaxAjax($id=null){
    	$percentage = 80;
    	if(!$id)
    	$id=$this->input->post('meter_id');
		$totalAmp = $this->dgKVA_Load($id);

		$maxAmp = ($percentage / 100) * $totalAmp;
		echo $maxAmp;
    }  

    public function dgKVA_Ajax($id=null){
    	$percentage = 80;
    	if(!$id)
    	$id=$this->input->post('meter_id');
		$totalAmp = $this->dgKVA_Load($id);
		$result = $this->iot_rest->getDG($id);
		//$maxAmp = ($percentage / 100) * $totalAmp;
		//$maxAmp = round($maxAmp,2);

		//$A = $this->getAmpValue($id);
        //$V = $this->getVoltValue($id);
        //$kVA = round($A * $V * 1.732 / 1000,2); 

		if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			$time= $result['data']['createdtime'];
			$totalkva= $result['data']['kva'];
			$arrDG = array();
        	$arrDG= array_merge(array('status' =>$status) ,array('time'=>$time),array('kva'=>$totalkva));
			return $this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrDG));        	
		}else{

			return 0;
		}
		
    }   

    public function recordFilters(){
    	if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			$rec_id = fb_fetch_id("power-meter"); // "350412";
			$min_date= strtotime($this->input->post('min_date')) * 1000;
			$max_date= strtotime($this->input->post('max_date')) * 1000;
			$numof_records= $this->input->post('numof_records');
			$filter_type= $this->input->post('filter-type');
			$meter_id= $this->input->post('meter_id');
			$result = $this->iot_rest->recordFilters_records($min_date,$max_date,$numof_records,$filter_type,$meter_id);
			//print_r($result);
			if($result["status"] == "success"){
				for($i=0;$i<count($result['data']);$i++)
				{
					$datetime= fb_convert_jsdate($result['data'][$i]['_source']['createdtime']);

					 $hrs = strtotime($datetime);
					 $hrs=date('H', $hrs);

					if($source["kwh"]){
				 		$kwh = $source["kwh"];
					 }else{
					 	$kwh = "nil";
					 }
					$datetime= fb_convert_jsdate($result['data'][$i]['_source']['createdtime']);
					$data[$i]=array_merge(array('createdtime'=>$datetime),array('pf'=>$result['data'][$i]['_source']['pf']),array('fre'=>$result['data'][$i]['_source']['fre']),array('tv'=>$result['data'][$i]['_source']['tv']),array('tpv'=>$result['data'][$i]['_source']['tpv']),array('tc'=>$result['data'][$i]['_source']['tc']),array('tp'=>$result['data'][$i]['_source']['tp']),array('kwh'=>$kwh));

				}
				//print_r($data);
				echo json_encode($data);
			}
		}
    }



    function getkwhAjax(){
    	$id = $this->input->post('meter_id');
        $rec_id = fb_fetch_id("power-meter"); // "350412";
        $data = array();
		$result = $this->iot_rest->getkwh($id);
		//print_r($result); exit();
		if($result["kwh"]){	
			$kwh= $result["kwh"];

			$data = array('kwh'=>round($kwh,2));				
			
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
		}
    }

    public function dgHrs($meter_id){
		if($this->fb_rest->isloggedin()){
			$table_name = "genset_activity";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";
			$range = "now-1M";
			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"lowv" => $range , "topv" => "now","range_fld" => "last_on_time",'meter_id'=>$meter_id);
			
			$query_str = $this->parser->parse('query/query-range-dg', $qpms, true); 

	

			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
			//print_r($result); exit();
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"]=="success"){
				$aggs["aggs"] = $result["aggs"];
				foreach($aggs as $row){
					
					foreach($row as $result){
						$runtime=$result["key"];
						 
						 $src = $result['tops']['hits']['hits'];
						
						foreach($src as $res){
							$temp = $res["_source"];
							$temp["rtime"]=$runtime;
							$source[] = $temp; 
							
						} 
						
					}
					
				}
				$result_set[] = $source;
				
				//fb_pr($result_set); exit;
				$data["aggs"] = $result_set;
				$this->load->view('dg_running',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}    	
    }


 public function fuelLoad($id=null,$percentage){
    	if(!$id)
    	$id=$this->input->post('meter_id');
		$totalAmp = $this->dgKVA_Load($id);
		$result = $this->iot_rest->getDG($id);
		$maxAmp = ($percentage / 100) * $totalAmp;
		$maxAmp = round($maxAmp,2);
		if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			$time= $result['data']['createdtime'];
			$arrDG = array();
        	$arrDG= array_merge(array('status' =>$status) ,array('time'=>$time),array('maxAmp'=>$maxAmp));
			return $this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrDG));        	
		}else{

			return 0;
		}
		
    } 		

	public function currentDGstatus($id){
      	$rec_id = fb_fetch_id("power-meter"); // "350412";
		$result = $this->iot_rest->getDG($id);
		 if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			
			$time= $result['data']['createdtime'];

			
			$lastCreatedtime = $this->session->userdata('createdtime');

			$this->session->set_userdata('createdtime', $result['data']['createdtime']);

        	$arrPower = array();
        	$arrPower= array_merge(array('status' =>$status) ,array('time'=>$time));
        	if($status==0 && $lastCreatedtime!=$time){
        		return "EB";
        	}
        	elseif ($status==1 && $lastCreatedtime!=$time) {
        		return "DG";
        	}else{
        		return "OFF";
        	}	
      }    
  }

  	public function ebReports($id) {
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			$final_data =array();
			$result = $this->iot_rest->geteb_record($id);
			$totalData = count($result['data']);
			foreach($result['data'] as $row){
				$source = $row['_source'];

				$presult_data['createdtime']=fb_convert_jsdate($source["createdtime"]);
				$presult_data['pf']=$source["pf"];
				$presult_data['fre']=$source["fre"];
				$presult_data['tv']=$source["tv"];
				$presult_data['tpv']=$source["tpv"];
				$presult_data['tc']=$source["tc"];
				$presult_data['tp']=$source["tp"];
				
				$datetime= fb_convert_jsdate($source["createdtime"]);
				
				//$date = new DateTime($datetime);

				 $date = strtotime($datetime);
				 $hrs= date('h', $date);
				 //$mins=$date->format('i');
				 if($source["kwh"]){
				 	$kwh = $source["kwh"];
				 }else{
				 	$kwh = "nil";
				 }
				 $presult_data['kwh']= $kwh;
				 
				 $final_data[] = $presult_data;
			}

			 $json_data = array( 
                    "recordsTotal"    => intval($totalData),  
                    "data"            => $final_data   
                    );
            
			echo json_encode($json_data);
			
		}else{
			redirect('/login');
		}
	}

 public function ajaxReport($id){
	if($this->fb_rest->isloggedin()){
		$data=array();
		$presult_data = array();
	
		$result = $this->iot_rest->getdg_record($id);
		//fb_pr($result); exit;
        $this->load->view('include/header');
        $this->load->view('include/left-sidebar');
        $aggregation_data = array();
		if($result["status"] == "success"){
			
			$data["title"] = "DG Reports";
			//$real_data = $result['aggs'];
			/*foreach($real_data as $row){
				$aggregation_data[] = $row['tops']['hits']['hits'];
				
			}*/
			$data['meter_id'] =$id;
			$this->load->view('reports_ajax',$data);	
		}
		else{
			$this->load->view("layout/error", $data);
		}
        $this->load->view('include/footer');
	}else{
		redirect('/login');
	}
 }

 public function dghrs_download(){

	if($this->fb_rest->isloggedin()){
		$data=array();
		$presult_data = array();

		//fb_pr($result); exit;
        $this->load->view('include/header');
        $this->load->view('include/left-sidebar');

		$this->parser->set_delimiters("__","__");
		// $rec_id = "350409";
		$rec_id = fb_fetch_id("genset_activity");
		$pno = $this->uri->segment(4, 0);
		
		//$from = ($pno <= 1) ? "0" : ( $pno - 1 ) ;
		$from = 0;
		
		$min_date = $this->input->get_post("min_date",true);  
		$max_date = $this->input->get_post("max_date",true);
		$hr = $this->input->get_post("hr",true);

		if($min_date && $max_date){
			$minDate = strtoUTC($min_date) * 1000; 
			$maxDate= strtoUTC($max_date) * 1000;
		}
		
		$download = $this->input->get_post("download"); 

		$meter_id = $id = $this->uri->segment('3');
		$data=array();
		$presult_data = array();
			if($min_date && $max_date){
				$tot_cnt = $this->fb_rest->get_mtotal_dgrunning($rec_id,$meter_id,$minDate,$maxDate); 
			}else{
				$tot_cnt = $this->fb_rest->get_mtotal_dgrunning($rec_id,$meter_id,'',''); 
			}

			if($tot_cnt>=10000){
				$tot_cnt= 9999;
			}
			else if($download=="pdf"){
				$tot_cnt = 9999;
			}
			else{
				$tot_cnt = $tot_cnt;
			}

			$result = $this->iot_rest->dghoursFilters_records($minDate,$maxDate,$meter_id,$from,$tot_cnt);
			//print_r($result); exit();
			$meterList = $this->iot_rest->getMeters();
			$meterNames = $meterList['data'];
			if($result["status"] == "success"){
				for($i=0;$i<count($result['data']);$i++)
				{
					$source = $result['data'][$i]['_source'];
					$last_on_time= fb_convert_jsdate($source['last_on_time']);
					$last_off_time= fb_convert_jsdate($source['last_off_time']);
					$rtime= hourFormat($source["last_on_time"],$source["last_off_time"]);
					//$fuel= $source["running_hours"];
                    $meter_id =$source['device_id'];
                    $fuel =$this->iot_rest->dgFuel($source["device_id"],$source["last_on_time"],$source["last_off_time"]);
					$consumed = round($fuel,2);
			
                    $meter_name = $meterNames[$meter_id];
					$presult_data[$i]=array_merge(array('last_on_time'=>$last_on_time),array('last_off_time'=>$last_off_time),array('meter'=>$meter_name),array('rt'=>$rtime),array("fuel"=>$consumed));
				}

				//print_r($presult_data); exit();
				   $data["status"] = "success";
				   $data["result_set"] = $presult_data;
				   $data["title"] = "Download DG running Hours";
				   $data['download_file'] = $filename;
				   $data['format'] = $download;
				   $data['min_date'] = $min_date;
				   $data['max_date'] = $max_date;
				   $data['meter_id'] = $meter_id;
	    		 $this->load->view("download_dgrunning", $data);
		}
		  
		else{
			$this->load->view("layout/error", $data);
		}
		$this->load->view("include/footer");
	}else{
		redirect('/login');
	} 	
 }

 public function removecache(){
 	$table = $this->input->get_post("table",true);
 	$meter_id = $this->input->get_post("meter_id",true);
 	if($table=="genset_activity" && $meter_id){
 		fb_clear_cache(array("*genset_activity*"));
 		redirect('/dashboard/get_dg_running_hrs/'.$meter_id);
 	}else{
 		redirect('/dashboard');
 	}
 }

 public function sendCommand($state=null,$meter_id=null){
 	$state = $this->input->post("state",true);
 	$meter_id = $this->input->post("meter_id",true);

 	$send = $this->fb_rest->sendDynamicCommand($state);
 	echo json_encode($send);
  }

}
