<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('cookie'); 
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()) {
			redirect('/dashboard');
		}else{
			if(get_cookie('username')){
				$data["username"] = get_cookie('username');
				$data["password"] = get_cookie('password');
				$this->load->view('login',$data);
			}else{
				$this->load->view('login');
			}
		}
	}
	
	function loginRequest(){

		$form_data = $this->input->post();
		// or just the username:
		$username = $this->input->post("username");
		$passwd = $this->input->post("password");
		$remember = $this->input->post('remember_me');
		if ($remember) {
		// Set remember me value in session
		$usernamecookie = array(
			  'name'   => 'username',
			  'value'  => $username,
			   'expire' => '86500',
			);
		$this->input->set_cookie($usernamecookie);

		$passwordcookie = array(
			  'name'   => 'password',
			  'value'  => $passwd,
			   'expire' => '86500',
			);
		$this->input->set_cookie($passwordcookie);

		}else{
			delete_cookie('username');
			delete_cookie('password');
		}
		$result = $this->fb_rest->login($username, $passwd);

		if($result['status']=="success"){
			if(has_accessable("dashboard")){
				redirect('/dashboard');
			} else {
				redirect('/welcome');
			}
			
		}else{
			$this->session->set_flashdata('invalid_user','Invalid Username or Password, Try Again!');
			redirect('/login');
		}
	}
	
	public function logout(){
		  $udata = array(
			  "user_token" => '',
			  "domain_key" => '',
			  "api_key" => '',
			  "email" => '',
			  "first_name" => '',
			  "last_name" => '',
			  "country" => '',
			  "state" => '',
			  "city" => '',
			  "address" => '',
			  "zipcode" => '',
			  "locale" => '',
			  "timezone" => ''
		  );
		$this->session->unset_userdata($udata);
		$this->session->sess_destroy();
		redirect('/login');
	}		
	
}
