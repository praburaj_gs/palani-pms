<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class Reports extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->library('Download');
		//$this->output->enable_profiler(TRUE);
	}

	public function index()
	{
		
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();

			//fb_pr($result); exit;
	        $this->load->view('include/header');
	        $this->load->view('include/left-sidebar');
				

				$data = array();
				$id = $this->uri->segment('2');
				$page_no = $this->uri->segment('3'); 
				$per_page = $this->input->get_post("no_items", true);
				$search = $this->input->get_post("search", true);
				$sort_fld = $this->input->get_post("sort_fld", true);
				$sort_dir = $this->input->get_post("sort_dir", true);
				$min_date = $this->input->get_post("min_date", true);
				$max_date = $this->input->get_post("max_date", true);

				$page_burl = site_url("/reports/".$id);
				$this->table="power-meter";
				$table_name = $this->table;
				$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "3",
				"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$min_date,"max_date"=>$max_date,"meter_id"=>$id);
				
				$data["sort_fld"] = $sort_fld;
				$data["sort_dir"] = $sort_dir;
				$data["search"] = $search;
				$data["per_page"] = $per_page;
				
				$sort_columns = array("createdtime", "pf", "fre","tv","tpv","tc","tp","kwh");
				//echo fb_text("feed_stock_distribution_list"); 
				$hstr = array("createdtime" =>"Date- Time", "pf" => "Power Factor", "fre" => "Frequency",
				"tv" => "Total single Phase Voltage","tpv" =>"Total three Phase Voltage","tc"=>"Total current","tp"=>"Total Power","kwh"=>"Kilowatt-hours"
				);
				
				$theader = "";
				
				foreach($hstr as $hk => $hv)
				{
					if(in_array($hk, $sort_columns)){
						$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
						$pstr = (!empty($per_page)) ? $per_page : "10";
						$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
						$srt_str = http_build_query($srt_params);
						$srt_url = site_url("/reports/".$id."/?$srt_str");
						$cdir_icon = "";
						if(!empty($sort_fld)){
							$cdir_icon = ($hk == $sort_fld) ? 
							(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
						}
						$thstr = $hv.$cdir_icon;
						$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
						$theader .= $thtml."\n";
					}else{
						$theader .= "<th>$hv</th>\n";
					}
				}
				
				$data["theader"] = $theader;
				$msg  = $this->fb_rest->list_record($params);
				//print_r($msg); exit();
				if($msg["status"] == "success")
				{
					$data["title"] = "DG Reports";
					$data['meter_id'] =$id;

					$data["page_links"] = $msg["page_links"];
					$data["result_set"] = $msg["result_set"];
					$this->load->view("reports_content", $data);
				}else{
					$this->load->view("layout/error", $data);
				}

	        $this->load->view('include/footer');
		}else{
			redirect('/login');
		}	
	}


	public function ebreport()
	{
		
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("eb_report")) {
				$data=array();
				$presult_data = array();

				//fb_pr($result); exit;
				$this->load->view('include/header');
				$this->load->view('include/left-sidebar');
					

					$data = array();
					$id = $this->uri->segment('3');
					$page_no = $this->uri->segment('4'); 
					$per_page = $this->input->get_post("no_items", true);
					$search = $this->input->get_post("search", true);
					$sort_fld = $this->input->get_post("sort_fld", true);
					$sort_dir = $this->input->get_post("sort_dir", true);
					$min_date = $this->input->get_post("min_date",true); 
					$max_date = $this->input->get_post("max_date",true);
					$hr = $this->input->get_post("hr",true); 

					//$minDate= strtotime($min_date) * 1000; 
					//$maxDate= strtotime($max_date) * 1000;
					if($min_date && $max_date){
						$minDate = strtoUTC($min_date) * 1000; 
						$maxDate= strtoUTC($max_date) * 1000;
					}

					$page_burl = site_url("/reports/ebreport/".$id);
					$this->table="power-meter";
					$table_name = $this->table;


					$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "4",
					"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$minDate,"max_date"=>$maxDate,"meter_id"=>$id,"filter_type"=>"0","hr"=>$hr);
					
					$data["sort_fld"] = $sort_fld;
					$data["sort_dir"] = $sort_dir;
					$data["search"] = $search;
					$data["per_page"] = $per_page;
					
					$sort_columns = array("createdtime", "pf", "fre","tv","tpv","tc","tp","kwh");
					//echo fb_text("feed_stock_distribution_list"); 
					$hstr = array("createdtime" =>"Date- Time", "pf" => "Power Factor", "fre" => "Frequency",
					"tv" => "Total single Phase Voltage","tpv" =>"Total three Phase Voltage","tc"=>"Total current","tp"=>"Total Power","kwh"=>"Kilowatt-hours"
					);
					
					$theader = "";
					
					foreach($hstr as $hk => $hv)
					{
						if(in_array($hk, $sort_columns)){
							$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
							$pstr = (!empty($per_page)) ? $per_page : "10";
							$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
							$srt_str = http_build_query($srt_params);
							$srt_url = site_url("/reports/ebreport/".$id."/?$srt_str");
							$cdir_icon = "";
							if(!empty($sort_fld)){
								$cdir_icon = ($hk == $sort_fld) ? 
								(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
							}
							$thstr = $hv.$cdir_icon;
							$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
							$theader .= $thtml."\n";
						}else{
							$theader .= "<th>$hv</th>\n";
						}
					}
					
					$data["theader"] = $theader;
					$msg  = $this->fb_rest->list_record($params);

					//dgStatus
					//$data['dgStatus'] = $this->iot_rest->currentDGstatus($id);
					//print_r($msg); exit();
					if($msg["status"] == "success")
					{
						$data["title"] = "EB Reports";
						$data['meter_id'] =$id;

						$data["page_links"] = $msg["page_links"];
						$data["result_set"] = $msg["result_set"];
						$data["aggs"] = $msg["aggs"];
						$this->load->view("reports_content", $data);
					}else{
						$this->load->view("layout/error", $data);
					}

				$this->load->view('include/footer');
			} else {
				$this->load->view('alert/permission');
			}
		}else{
			redirect('/login');
		}	
	}

	public function dgreport()
	{
		
		if($this->fb_rest->isloggedin()){
			
			if($this->fb_rest->has_accessable("dg_report")) {
			$data=array();
			$presult_data = array();

			//fb_pr($result); exit;
	        $this->load->view('include/header');
	        $this->load->view('include/left-sidebar');
				

				$data = array();
				$id = $this->uri->segment('3');
				$page_no = $this->uri->segment('4'); 
				$per_page = $this->input->get_post("no_items", true);
				$search = $this->input->get_post("search", true);
				$sort_fld = $this->input->get_post("sort_fld", true);
				$sort_dir = $this->input->get_post("sort_dir", true);
				$min_date = $this->input->get_post("min_date", true);
				$max_date = $this->input->get_post("max_date", true);
				$hr = $this->input->get_post("hr",true); 

				$page_burl = site_url("/reports/dgreport/".$id);
				$this->table="power-meter";
				$table_name = $this->table;

				if($min_date && $max_date){
					$minDate = strtoUTC($min_date) * 1000; 
					$maxDate= strtoUTC($max_date) * 1000;
				}
	
				$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "4",
				"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$minDate,"max_date"=>$maxDate,"meter_id"=>$id,"filter_type"=>"1","hr"=>$hr);
				
				$data["sort_fld"] = $sort_fld;
				$data["sort_dir"] = $sort_dir;
				$data["search"] = $search;
				$data["per_page"] = $per_page;
				
				$sort_columns = array("createdtime", "pf", "fre","tv","tpv","tc","tp","kwh");
				//echo fb_text("feed_stock_distribution_list"); 
				$hstr = array("createdtime" =>"Date- Time", "pf" => "Power Factor", "fre" => "Frequency",
				"tv" => "Total single Phase Voltage","tpv" =>"Total three Phase Voltage","tc"=>"Total current","tp"=>"Total Power","kwh"=>"Kilowatt-hours"
				);
				
				$theader = "";
				
				foreach($hstr as $hk => $hv)
				{
					if(in_array($hk, $sort_columns)){
						$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
						$pstr = (!empty($per_page)) ? $per_page : "10";
						$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
						$srt_str = http_build_query($srt_params);
						$srt_url = site_url("/reports/dgreport/".$id."/?$srt_str");
						$cdir_icon = "";
						if(!empty($sort_fld)){
							$cdir_icon = ($hk == $sort_fld) ? 
							(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
						}
						$thstr = $hv.$cdir_icon;
						$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
						$theader .= $thtml."\n";
					}else{
						$theader .= "<th>$hv</th>\n";
					}
				}
				
				$data["theader"] = $theader;
				$msg  = $this->fb_rest->list_record($params);
				//print_r($msg); exit();
				if($msg["status"] == "success")
				{
					$data["title"] = "DG Reports";
					$data['meter_id'] =$id;

					$data["page_links"] = $msg["page_links"];
					$data["result_set"] = $msg["result_set"];
					$data["aggs"] = $msg["aggs"];
					$this->load->view("reports_content", $data);
				}else{
					$this->load->view("layout/error", $data);
				}

	        $this->load->view('include/footer');
			
			} else {
				$this->load->view('alert/permission');
			}

			
			
		}else{
			redirect('/login');
		}	
	}
	public function callbackEbreport($meter_id,$min_date=null,$max_date=null){
		//echo $meter_id; exit();
		
		$this->parser->set_delimiters("__","__");
		$rec_id = fb_fetch_id("power-meter"); // "350412";
		$meter_id = $this->uri->segment(3, 0);
		$tot_cnt = $this->fb_rest->get_mtotal_count($rec_id,$meter_id); 
		$pno = $this->uri->segment(4, 0);
		$per_page = 20;
		
		$from = ($pno <= 1) ? "0" : ( $pno - 1 ) ;
		$from = ($from * $per_page);
		
		$ppms = array("page_burl" => site_url("reports/callbackEbreport/".$meter_id), "total_rows" => $tot_cnt,
		"per_page" => $per_page, "uri_segment" => "4");
		
		$plinks = fb_generate_pagination($ppms);
		
			$data=array();
			$presult_data = array();
			if($min_date)
				$min_date= $min_date;
			else
				$min_date="";
			if($max_date)
				$max_date= $max_date;
			else
				$max_date="";
			//$numof_records= $this->input->get('numof_records');
			$filter_type= 0;

			$result = $this->iot_rest->recordFilters_records($min_date,$max_date,$from,$per_page,$filter_type,$meter_id);
			//print_r($result);
			if($result["status"] == "success"){
				for($i=0;$i<count($result['data']);$i++)
				{
					$datetime= fb_convert_jsdate($result['data'][$i]['_source']['createdtime']);

					 $hrs = strtotime($datetime);
					 $hrs=date('h', $hrs);

					if($result['data'][$i]['_source']["kwh"]){
				 		$kwh = $result['data'][$i]['_source']["kwh"];
					 }else{
					 	$kwh = "nil";
					 }
					$presult_data[$i]=array_merge(array('createdtime'=>$datetime),array('pf'=>$result['data'][$i]['_source']['pf']),array('fre'=>$result['data'][$i]['_source']['fre']),array('tv'=>$result['data'][$i]['_source']['tv']),array('tpv'=>$result['data'][$i]['_source']['tpv']),array('tc'=>$result['data'][$i]['_source']['tc']),array('tp'=>$result['data'][$i]['_source']['tp']),array('kwh'=>$kwh));

				}
				//print_r($data);
				$data["plinks"] = $plinks;
				$data["status"] = "success";
				$data["result_set"] = $presult_data;
				//print_r($data["result_set"]);
				$sdata= json_encode($data);	
				$this->output
				->set_content_type('application/json')
				->set_output($sdata);
			}

		}


		public function download(){
			
			if($this->fb_rest->isloggedin()){
				$data=array();
				$presult_data = array();

				//fb_pr($result); exit;
		        $this->load->view('include/header');
		        $this->load->view('include/left-sidebar');

				$this->parser->set_delimiters("__","__");
				$rec_id = fb_fetch_id("power-meter"); // "350412";
				$meter_id = $this->uri->segment(3, 0);
				$tot_cnt = $this->fb_rest->get_mtotal_count($rec_id,$meter_id);  
				if($tot_cnt>=10000)
					$tot_cnt= 9999;

				$pno = $this->uri->segment(4, 0);
				$per_page = 20;
				
				//$from = ($pno <= 1) ? "0" : ( $pno - 1 ) ;
				$from = 0;
				
				$min_date = $this->input->get_post("min_date",true);  
				$max_date = $this->input->get_post("max_date",true);
				$hr = $this->input->get_post("hr",true);

				if($min_date && $max_date){
					$minDate = strtoUTC($min_date) * 1000; 
					$maxDate= strtoUTC($max_date) * 1000;
				}

				//$meter_id = $this->input->get_post("meter_id");
				$getfilter_type = $this->input->get_post("filter-type"); 
				
				$download = $this->input->get_post("download"); 

				if($download=="pdf")
					$tot_cnt = 8000;
					$data=array();
					$presult_data = array();

					//$numof_records= $this->input->get('numof_records');
					if($getfilter_type)
						$filter_type= $getfilter_type;
					else
						$filter_type= "0";
					
					$result = $this->iot_rest->recordFilters_records($minDate,$maxDate,$from,$tot_cnt,$filter_type,$meter_id,$hr);
					//print_r($result); exit();
					if($result["status"] == "success"){
						for($i=0;$i<count($result['data']);$i++)
						{
							$datetime= fb_convert_jsdate($result['data'][$i]['_source']['createdtime']);

							$hrs = strtotime($datetime);
							$hrs=date('h', $hrs);

							if($result['data'][$i]['_source']["kwh"]){
							 	$kwh = $result['data'][$i]['_source']["kwh"];
							 }else{
							 	$kwh = "nil";
							 }

							$presult_data[$i]=array_merge(array('createdtime'=>$datetime),array('pf'=>$result['data'][$i]['_source']['pf']),array('fre'=>$result['data'][$i]['_source']['fre']),array('tv'=>$result['data'][$i]['_source']['tv']),array('tpv'=>$result['data'][$i]['_source']['tpv']),array('tc'=>$result['data'][$i]['_source']['tc']),array('tp'=>$result['data'][$i]['_source']['tp']),array('kva'=>$result['data'][$i]['_source']['kva']),array('kwh'=>$kwh));
						}


						$data["status"] = "success";
						$data["result_set"] = $presult_data;
						
						///$this->processDownload($presult_data,$download);

						   $data["title"] = "Download Reports";
						   $data['meter_id'] =$meter_id;
						   $data['download_file'] = $filename;
						   $data['format'] = $download;
						   $data['filter_type'] = $filter_type;
 						   $data['min_date'] = $min_date;
 						   $data['max_date'] = $max_date;
						   $data['hr'] = $hr;

			    			$this->load->view("download", $data);
				}
				  
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view("include/footer");
		}else{
			redirect('/login');
		}
	}


	public function download_file($file,$format,$page,$id){

		$filename = $file.".".$format;
		$dir = "./reports/";

		if (file_exists($dir.$filename)){
			if($format=="xls"){
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename='.$filename);
				header('Cache-Control: max-age=0');
				// If you're serving to IE 9, then the following may be needed
				header('Cache-Control: max-age=1');			
			}else if($format=="xlsx"){
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename='.$filename);
				header('Cache-Control: max-age=0');
				// If you're serving to IE 9, then the following may be needed
				header('Cache-Control: max-age=1');			
			}elseif($format=="csv"){
				header('Content-Type: application/csv');
				header('Content-Disposition: attachment; filename='.$filename);
				header('Cache-Control: max-age=0');
			}else if($format=="pdf"){
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment; filename='.$filename);
				header('Cache-Control: max-age=0');
			}
			unlink($dir.'/'.$filename);
		}else{
		 if($page==0){
		 	$rUrl = 'reports/ebreport/'.$id;
		 	redirect($rUrl);
		 }else{
		 	$rUrl = 'reports/dgreport/'.$id;
		 	redirect($rUrl);
		 }
		} 	 
	}

	public function removecache(){
	 	$table = $this->input->get_post("table",true);
	 	$meter_id = $this->input->get_post("meter_id",true);
	 	$type = $this->input->get_post("type",true);
	 	if($table=="power-meter" && $meter_id){
	 		fb_clear_cache(array("*power-meter*"));
	 		if($type=="eb")
	 			 redirect('/reports/ebreport/'.$meter_id);
	 		else if($type="dg")
	 			redirect('/reports/dgreport/'.$meter_id);
	 	}else if($table="kwh_log"){
	 		fb_clear_cache(array("*kwh_log*"));
	 			redirect('/reports/kwhreport/'.$meter_id);
	 	}else{
	 		redirect('/dashboard');
	 	}
	 }

	 public function getnewRecords(){
	 	$meter_id = $this->input->post('meter_id');
	 	$lastrecord = $this->input->post('time');
	 	$type = $this->input->post('type');
	 	$calltype = $this->input->post('calltype');
	 	$record = $this->iot_rest->getnewRecords($meter_id,$lastrecord,$type,$calltype);
	 	$sdata= json_encode($record);
		$this->output
				->set_content_type('application/json')
				->set_output($sdata);
	 }

	 public function kwhreport(){
		if($this->fb_rest->isloggedin()){
			
			if($this->fb_rest->has_accessable("kwh_report")) {
			
			//$table_name = "genset_activity";
			$data=array();
			//$from = 0;
			//$size = 10;
			$orderfld = "last_on_time";
			$orderdir = "desc";
			$data = array();
			$id = $this->uri->segment('3');
			$page_no = $this->uri->segment('4'); 
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$min_date = $this->input->get_post("min_date", true);
			$max_date = $this->input->get_post("max_date", true);

			$page_burl = site_url("/reports/kwhreport/".$id);

			if($min_date && $max_date){
					$minDate = strtoUTC($min_date) * 1000; 
					$maxDate= strtoUTC($max_date) * 1000;
			}

			$page_burl = site_url("/reports/kwhreport/".$id);
			$this->table="kwh_log";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "4",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name,"min_date"=>$minDate,"max_date"=>$maxDate,"meter_id"=>$id);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("last_on_time", "last_off_time");
			//echo fb_text("feed_stock_distribution_list"); 
			$hstr = array("last_on_time" =>"Start Time", "last_off_time" => "End time",
			"device_name" => "Device Name","type"=>"Status","duration"=>"Duration","kwh_start"=>"KWH Start","consumed"=>"Consumed","kwh_end"=>"Total");
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/reports/kwhreport/".$id."/?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->kwh_list($params);
			//print_r($msg); exit();
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');

			if($msg["status"] == "success")
			{
				$data["title"] = "KWH Reports";
				$data['meter_id'] =$id;

				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("kwh_content", $data);
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
			
			} else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}	 	
	 }

	 public function kwh_download(){

		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();

			//fb_pr($result); exit;
	        $this->load->view('include/header');
	        $this->load->view('include/left-sidebar');

			$this->parser->set_delimiters("__","__");
			// $rec_id = "350409";
			$rec_id = fb_fetch_id("kwh_log");
			$pno = $this->uri->segment(4, 0);
			
			$from = ($pno <= 1) ? "0" : ( $pno - 1 ) ;
			$from = 0;
			
			$min_date = $this->input->get_post("min_date",true);  
			$max_date = $this->input->get_post("max_date",true);
			$hr = $this->input->get_post("hr",true);

			if($min_date && $max_date){
				$minDate = strtoUTC($min_date) * 1000; 
				$maxDate= strtoUTC($max_date) * 1000;
			}
			
			$download = $this->input->get_post("download"); 

			$meter_id = $id = $this->uri->segment('3');
			$data=array();
			$presult_data = array();
				if($min_date && $max_date){
					$tot_cnt = $this->fb_rest->get_mtotal_kwh($rec_id,$meter_id,$minDate,$maxDate); 
				}else{
					$tot_cnt = $this->fb_rest->get_mtotal_kwh($rec_id,$meter_id,'',''); 
				}

				if($tot_cnt>=10000){
					$tot_cnt= 9999;
				}
				else if($download=="pdf"){
					$tot_cnt = 9999;
				}
				else{
					$tot_cnt = $tot_cnt;
				}

				$result = $this->iot_rest->kwhFilters_records($minDate,$maxDate,$meter_id,$from,$tot_cnt);
				//print_r($result);
				$meterList = $this->iot_rest->getMeters();
				 $ebstatus = $this->iot_rest->currentDGstatus($meter_id);
				$meterNames = $meterList['data'];
				if($result["status"] == "success"){
					for($i=0;$i<count($result['data']);$i++)
					{
						$source = $result['data'][$i]['_source'];
						$last_on_time= fb_convert_jsdate($source['last_on_time']);

						if(!empty($source["last_off_time"])){
                              $last_off_time= fb_convert_jsdate($source['last_off_time']);
                              $rtime= hourFormat($source["last_on_time"],$source["last_off_time"]);
                         }else{
                             if($ebstatus=="EB" || $ebstatus=="DG"){
                             $ctime=time();
                             $endtime= $ctime."000";
                             $last_off_time= fb_convert_jsdate($endtime);
                             $rtime= hourFormat($source["last_on_time"],$source["last_off_time"]);
                           	}else{
                           	 $last_off_time="OFF";
                             $rtime="NIL";
                            }
                        }

						
						 $kwh_start= $source["kwh_start"];

						 if($source["kwh_end"]){
                           $subtractedKWH = $source["kwh_end"] - $source["kwh_start"];
                           $kwh_end =  $source["kwh_start"] + $subtractedKWH;
                           $consumed = $source["kwh_end"] - $source["kwh_start"];
                          }else{
                            $ckwh = $this->iot_rest->getkwh($source["device_id"]);
                            $kwh_end = $ckwh['kwh'];
                            $consumed = $kwh_end - $source["kwh_start"];
                          }

	                 
	                    $dg_status =$source['dg_status'];
		                    if($source['dg_status']==1){
	                             $meterstatus = "DG";
	                        }else{
	                        	$meterstatus = "EB";
	                        }
						
						
	                    $meter_name = $meterNames[$meter_id];
						$presult_data[$i]=array_merge(array('last_on_time'=>$last_on_time),array('last_off_time'=>$last_off_time),array('meter'=>$meter_name),array('status'=>$meterstatus),array('duration'=>$rtime),array('kwh_start'=>$kwh_start),array("consumed"=>$consumed),array("total"=>$kwh_end));
					}

					   $data["status"] = "success";
					   $data["result_set"] = $presult_data;
					   $data["title"] = "Download KWH consumption";
					   $data['download_file'] = $filename;
					   $data['format'] = $download;
					   $data['min_date'] = $min_date;
					   $data['max_date'] = $max_date;
					   $data['meter_id'] = $meter_id;
		    		 $this->load->view("kwh_download", $data);
			}
			  
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view("include/footer");
			}else{
				redirect('/login');
			} 	
 	}

}
