<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

   public $api_url;
   public $api_key;
   
	function __construct()
    {
	  parent::__construct();
      $this->config->load('fb_boodskap', TRUE);
	  $this->api_url = $this->config->item('fb_rapi_burl', 'fb_boodskap');
	  $this->api_key = $this->config->item('fb_rapi_key', 'fb_boodskap');
	}

	public function send_alert(){
		require FCPATH.'application/third_party/boodskap/vendor/autoload.php';
		
		$fname = FCPATH.'json/notification.json';
		
		try {
			$client = new \GuzzleHttp\Client();
			$api_url = $this->api_url;
			$api_key = $this->api_key;

			// Provide an fopen resource.
			$body = fopen($fname, 'r');
			/* $r = $client->request('POST', 'http://4blabs.com/fb-rest-api/notification/send', ['body' => $body ,
			'headers' => ['X-API-KEY' => 'A08161820842103866BH']
			]); */
			
			$r = $client->request('POST', $api_url.'/notification/send', ['body' => $body ,
			'headers' => ['X-API-KEY' => $api_key]
			]);

			echo $r->getBody();

			echo "<pre>";
			print_r($r);
			echo "</pre>";
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			#guzzle repose for future use
			$response = $e->getResponse();
			$responseBodyAsString = $response->getBody()->getContents();
			echo "<pre>";
			print_r($responseBodyAsString);
			echo "</pre>";
		}


	}
	
	
}

