        var form_validation = function() {
        var e = function() {
                jQuery("#login-form").validate({
                    ignore: [],
                    errorClass: "invalid-feedback animated fadeInDown",
                    errorElement: "div",
                    errorPlacement: function(e, a) {
                        jQuery(a).parents(".form-group").append(e)
                    },
                    highlight: function(e) {
                        jQuery(e).parents(".form-group").removeClass("is-invalid").addClass("is-invalid")
                    },
                    success: function(e) {
                        jQuery(e).parents(".form-group").removeClass("is-invalid"), jQuery(e).remove()
                    },
                     rules: {
                        username: {
                            required: !0,
                            email: !0
                        },
                        password: {
                            required: !0,
                        },
            
                    },
                    messages: {
            
                        username: {
                            required: "Please enter a username",
                            email: "Please enter a valid email address",
                        },
                        password: {
                            required: "Please provide a password",
                        },
                    }
                })
            }
        return {
            init: function() {
                e(),jQuery(".js-select2").on("change", function() {
                    jQuery(this).valid()
                })
            }
        }
    }();
    
    jQuery(function() {
        form_validation.init();
        
        $(".preloader").fadeOut();
    
    });