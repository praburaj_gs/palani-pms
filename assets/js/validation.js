    $.validator.addMethod("chkduplicate", function(value, element, arg) {
        //console.log(value);
        var pms = JSON.parse(arg);
        pms["search"] = value;
        var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
        pms["rkey"] = rkey;
        var chk_url = baseUrl + "common/check_duplicate";
        var flg = false;
        $.ajax({
            type: "GET",
            data: pms,
            url: chk_url,
            async: false,
        }).done(function(resp) {
            var rdata = JSON.parse(resp);
            //console.log(resp);
            //console.log(rdata.status);
            if (rdata.status == "success") {
                console.log("success");
                flg = true;
            } else {
                console.log("fail");
                flg = false;
            }

        }).fail(function(err) {
            console.log(err);
        });
        console.log("123>>>>>>");
        //console.log(pms);
        return flg;
    }, "This field value already exists.");

$.validator.addMethod('customphone', function (value, element) {
    return this.optional(element) || /^(\+91-|\+91|0)?\d{10}$/.test(value);
}, "Please enter a valid phone number");    

 $("#devices-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            device_id: {
                required: true,
                chkduplicate: '{ "table_name": "devices", "fld_name": "device_id"}'
            },
            device_name: {
                required: true,
            },
            device_status: {
                required: true,
                
            },
            device_type: {
                required: true,
                
            },
            watts: {
                required: true,
                
            }
        },
        messages: {
            device_id: {
                required: "please enter ID",
            },
            device_name: {
                required: "Please enter the device name",
            },
            device_status: {
                required: "please select the status",
                
            },
            device_type: {
                required: "please select device type",
                
            },
            watts: {
                required: "please enter watts needed for the device",
                
            }
        }

    });  


    $("#meter-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            meter_id: {
                required: true,
                digits : true,
                chkduplicate: '{ "table_name": "meters", "fld_name": "meter_id"}'
            },
            meter_name: {
                required: true,
            },
            meter_desc: {
                required: true,
                
            },
            meter_image: {
                required: true,  
            },
            kva: {
                required : true,
                number : true,
                max: 3500
            },
            running_hrs:{
                required: true,
                digits : true
            }

        },
        messages: {
            meter_id: {
                required: "please enter ID",
                digits : "Please enter number only"
            },
            meter_name: {
                required: "Please enter the DG name",
            },
            meter_desc: {
                required: "please enter description",
                
            },
            meter_image: {
                required: "please select image",
                
            },
            kva : {
                required :"Please enter KVA of the DG",
                number : "Please enter number only",
                max: "Please enter a value less than or equal to 3500"
            },
            running_hrs :{
              required: "please enter current running hours",
              digits : "Please enter number only"  
            }
        }

    });   

    
    $("#genset-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            name: {
                required: true,
                chkduplicate: '{ "table_name": "genset_list", "fld_name": "name"}'
            },
            type: {
                required: true,
                
            },
            rpm: {
                required: true,
                number: true
            },
            minVal: {
                required: true,
                number: true
                
            },
            maxVal: {
                required: true,
                number: true
                
            }
        },
        messages: {
            name: {
                required: "please enter name",
            },
            type: {
                required: "Please enter type",
            },
            rpm: {
                required: "please enter RPM",
                
            },
            minVal: {
                required: "please enter minimum value",
                number : "Minumum value must be in number",
            },
            maxVal: {
                required: "please enter maximum value",
                number : "maximum value must be in number",
            }
        }

    });     

    $("#notify-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            name: {
                required: true
                
            },
            ph_no: {
                required: true,
                customphone: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: {
                required: "please enter Name",
            },
            ph_no: {
                required: "Please enter the Phone number",
                customphone: "Please enter a valid phone number"
            },
            email: {
                required: "please Enter Email address",
                
            }
        }

    });     


$("#alert-settings-dg-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            dgbcpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgbcpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgbppmax : {
                required: true,
                number: true,
                min: 0
            },
            dgbppmin : {
                required: true,
                number: true,
                min: 0
            },
            dgbspmax : {
                required: true,
                number: true,
                min: 0
            },
            dgbspmin : {
                required: true,
                number: true,
                min: 0
            },
            dgbtpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgbtpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgfremax : {
                required: true,
                number: true,
                min: 0
            },
            dgfremin : {
                required: true,
                number: true,
                min: 0
            },
            dgpfmax : {
                required: true,
                number: true,
                min: 0
            },
            dgpfmin : {
                required: true,
                number: true,
                min: 0
            },
            dgrcpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgrcpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgrppmax : {
                required: true,
                number: true,
                min: 0
            },
            dgrppmin : {
                required: true,
                number: true,
                min: 0
            },
            dgrspmax : {
                required: true,
                number: true,
                min: 0
            },
            dgrspmin : {
                required: true,
                number: true,
                min: 0
            },
            dgrtpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgrtpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgspmax : {
                required: true,
                number: true,
                min: 0
            },
            dgspmin : {
                required: true,
                number: true,
                min: 0
            },
            dgtcpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgtcpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgtpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgtpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgtppmax : {
                required: true,
                number: true,
                min: 0
            },
            dgtppmin : {
                required: true,
                number: true,
                min: 0
            },
            dgycpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgycpmin : {
                required: true,
                number: true,
                min: 0
            },
            dgyppmax : {
                required: true,
                number: true,
                min: 0
            },
            dgyppmin : {
                required: true,
                number: true,
                min: 0
            },
            dgyspmax : {
                required: true,
                number: true,
                min: 0
            },
            dgyspmin : {
                required: true,
                number: true,
                min: 0
            },
            dgytpmax : {
                required: true,
                number: true,
                min: 0
            },
            dgytpmin : {
                required: true,
                number: true,
                min: 0
            }
        },
        messages: {
            dgbcpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbtpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgbtpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgfremax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgfremin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgpfmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgpfmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrcpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrtpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgrtpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtcpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtpmax : {
                 required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtpmin : {
                 required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgtppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgycpmax : {
               required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgycpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgyppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgyppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgyspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgyspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgytpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            dgytpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            }
        }

    });   
    
    $("#alert-settings-form").validate({
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            bcpmax : {
                required: true,
                number: true,
                min: 0
            },
            bcpmin : {
                required: true,
                number: true,
                min: 0
            },
            bppmax : {
                required: true,
                number: true,
                min: 0
            },
            bppmin : {
                required: true,
                number: true,
                min: 0
            },
            bspmax : {
                required: true,
                number: true,
                min: 0
            },
            bspmin : {
                required: true,
                number: true,
                min: 0
            },
            btpmax : {
                required: true,
                number: true,
                min: 0
            },
            btpmin : {
                required: true,
                number: true,
                min: 0
            },
            fremax : {
                required: true,
                number: true,
                min: 0
            },
            fremin : {
                required: true,
                number: true,
                min: 0
            },
            pfmax : {
                required: true,
                number: true,
                min: 0
            },
            pfmin : {
                required: true,
                number: true,
                min: 0
            },
            rcpmax : {
                required: true,
                number: true,
                min: 0
            },
            rcpmin : {
                required: true,
                number: true,
                min: 0
            },
            rppmax : {
                required: true,
                number: true,
                min: 0
            },
            rppmin : {
                required: true,
                number: true,
                min: 0
            },
            rspmax : {
                required: true,
                number: true,
                min: 0
            },
            rspmin : {
                required: true,
                number: true,
                min: 0
            },
            rtpmax : {
                required: true,
                number: true,
                min: 0
            },
            rtpmin : {
                required: true,
                number: true,
                min: 0
            },
            spmax : {
                required: true,
                number: true,
                min: 0
            },
            spmin : {
                required: true,
                number: true,
                min: 0
            },
            tcpmax : {
                required: true,
                number: true,
                min: 0
            },
            tcpmin : {
                required: true,
                number: true,
                min: 0
            },
            tpmax : {
                required: true,
                number: true,
                min: 0
            },
            tpmin : {
                required: true,
                number: true,
                min: 0
            },
            tppmax : {
                required: true,
                number: true,
                min: 0
            },
            tppmin : {
                required: true,
                number: true,
                min: 0
            },
            ycpmax : {
                required: true,
                number: true,
                min: 0
            },
            ycpmin : {
                required: true,
                number: true,
                min: 0
            },
            yppmax : {
                required: true,
                number: true,
                min: 0
            },
            yppmin : {
                required: true,
                number: true,
                min: 0
            },
            yspmax : {
                required: true,
                number: true,
                min: 0
            },
            yspmin : {
                required: true,
                number: true,
                min: 0
            },
            ytpmax : {
                required: true,
                number: true,
                min: 0
            },
            ytpmin : {
                required: true,
                number: true,
                min: 0
            }
        },
        messages: {
            bcpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            bcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            bppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            bppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            bspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            bspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            btpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            btpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            fremax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            fremin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            pfmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            pfmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rcpmax : {
                required: "please enter a value",
                number: "Please enter number only"
            },
            rcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rtpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            rtpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            spmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            spmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            tcpmax : {
                required: "please enter a value",
                number: "Please enter number only"
            },
            tcpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            tpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            tpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            tppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            tppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            ycpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            ycpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            yppmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            yppmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            yspmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            yspmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            ytpmax : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            },
            ytpmin : {
                required: "please enter a value",
                number: "Please enter number only",
                min: "Please enter positive numbers"
            }
        }

    });   
   $('#filter-form').validate({
        rules: {
            min_date: {
                required: true,
            },
            max_date: {
                required: true,
            }

        },
        messages: {
            min_date: {
                required: "please select from date",
            },
            max_date: {
                required: "Please select to date",
            }
        }
    });

    $('#fuelsettings-form').validate({
        rules: {
            qtr_ltr: {
                required: true,
                number: true,
                min:0.1
            },
            half_ltr: {
                required: true,
                number: true,
                min:0.1
            },
            threefour_ltr: {
                number: true,
                required: true,
                min:0.1
            },
            full_ltr: {
                number: true,
                required: true,
                min:0.1
            },   
            fullover_ltr: {
                number: true,
                required: true,
                min:0.1
            }         

        },messages: {
            qtr_ltr: {
                min:"Please enter greater than 0"
            },
            half_ltr: {
                min:"Please enter greater than 0"
            },
            threefour_ltr: {
                min:"Please enter greater than 0"
            },
            full_ltr: {
                min:"Please enter greater than 0"
            },
            fullover_ltr: {
                min:"Please enter greater than 0"
            }
        },
        submitHandler: function(form) {
            console.log(form);
            var msg_url = baseUrl+'/settings/fuelSettings';
            $.ajax({
                url: msg_url,
                type: "POST",
                data: $(form).serialize(),
                success: function(response) {
                    if(response=="success"){
                        $('#fulesuccess').show();
                        /*setTimeout(function(){
                             $('#fuel-modal').modal('hide');
                         },2500);*/
                       
                    }else{
                        $('#fuelerror').show();
                         /*setTimeout(function(){
                             $('#fuel-modal').modal('hide');
                         },2500);*/
                    }
                }            
            });            
        }
    });   


    $('#bcheck-form').validate({

        rules: {
            next_date: {
                required: true,
            },
            next_hours: {
                required: true,
                number: true,
                min:0.1
            },       

        }, messages: {
            next_date: {
                required: "please select date",
            },
            next_hours: {
                required: "Please enter hours ",
                number:"Please enter numbers only",
                min:"Please enter greater than 0"
            }
        },

        submitHandler: function(form) {

            var msg_url = baseUrl+'/meters/addBcheck';
            $.ajax({
                url: msg_url,
                type: "POST",
                data: $(form).serialize(),
                success: function(response) {
                    console.log(response); 
                    if(response.status="success"){
                        $("#bcheck-error").removeClass('alert-danger').addClass('alert-success');
                        $('#bcheck-error').html("B-Check updated successfully").show();                      
                    }else{
                        $('#bcheck-error').show();
                    }
                }            
            });
        }
    });     
	
	    $('#notes-form').validate({

        rules: {
			notes_title: {
                required: true,
            },
			notes_description: {
                required: true,
            },
            notes_date: {
                required: true,
            },                   

        }, messages: {
			notes_title: {
                required: "please enter a title",
            },
			notes_description: {
                required: "please enter a description",
            },
            notes_date: {
                required: "please select date",
            },
			
			
            
        },

        
    });     