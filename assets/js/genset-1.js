var genset = function(){
    var $genset = this;
    this.generatorstate =false;
    this.consumption = 0;
    this.devices;
    this.meter_text = null;
    this.meter_range = null;
    this.current_ratio = null;
    this.max_power = null;
    this.safe_threshold = null;
    var generator_box = $(".generator__box");
    var generator_switch = $(".generator__switch");  
    var device_switch = $(".bulb__switch");
    

    
generator_switch.on("click", function(){
    //console.log($(this));
    if($genset.generatorstate==false){
        $genset.generatorstate = true;
        $genset.max_power = $(this).data('threshold');
        generator_box.addClass('generator__box-on');
        generator_switch.addClass('switch-on').text('ON');
    }
    else{
        $genset.generatorstate = false;
        generator_box.removeClass('generator__box-on');
        generator_switch.removeClass('switch-on').text('OFF');
        $genset.turnOff(device_switch);
        $genset.consumption = 0;
        $genset.ConsumptionMeter();
    }
});
    
device_switch.on("click", function(){   
        var device = $(this);
        console.log(device.text());
        if ($genset.generatorstate==true && device.text()=="OFF") {
          $genset.turnOn(device);
          return;
        }else if($genset.generatorstate==true && device.text()=="ON"){
          $genset.turnOff(device);
          return;            
        }
});    

this.turnOff = function(device){
    //console.log(device.parent('.bulb').find('.bulb__icon'));
    $genset.consumption = $genset.consumption - device.data('volt');
    $genset.ConsumptionMeter();
    device.parent('.bulb').find('.bulb__icon').removeClass('bulb__icon-on');
    device.parent('.bulb').find('.bulb__consumption').text("0 W");
    device.removeClass('switch-on').text('OFF');
}

this.turnOn = function(device){
    //console.log(device.parent('.bulb').find('.bulb__icon'));
    $genset.consumption = $genset.consumption + device.data('volt');
    $genset.ConsumptionMeter();
    device.parent('.bulb').find('.bulb__icon').addClass('bulb__icon-on');
    device.parent('.bulb').find('.bulb__consumption').text(device.data('volt')+ " W");
    device.addClass('switch-on').text('ON');
}

this.ConsumptionMeter = function(){
    $genset.meter_text = $genset.consumption+"%";
    $genset.meter_range = $genset.consumption + "%";
    $(".meter__text").text($genset.meter_text);
    $('.meter__range').css("width",$genset.meter_range);
    if($genset.consumption>=$genset.max_power)
        $('.meter__range').addClass('bg-danger');
    else
        $('.meter__range').removeClass('bg-danger');
    
}
    
}

var genSet = new genset();