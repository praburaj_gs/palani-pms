import groovy.json.*;

private static String timeConversion(int totalSeconds) {

    final int MINUTES_IN_AN_HOUR = 60;
    final int SECONDS_IN_A_MINUTE = 60;

    int seconds = totalSeconds % SECONDS_IN_A_MINUTE;
    int totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE;
    int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
    int hours = totalMinutes / MINUTES_IN_AN_HOUR;

    return hours;
}

def check_exist_notify(meter_id){

	def d = new Date();
	def d1 = d.format("d-M-Y");
	def notify_date = d1.toString();

	def from = 0;
	def size = 1000;

	def mlnestr = """
	{ "query": { "constant_score" : {
				"filter" : {
					 "bool" : {
						"must" : [
							{ "term" : { "meter_id" : "${meter_id}" } },
							{ "term" : { "notify_date" : "${notify_date}" } }
						]
					}
				}
			}
		},
		"from": ${from},
		"size": ${size}
	}
	""";

	log.info("%s", mlnestr);

	def mtr_nrst = record.search(350418,null, mlnestr);
	def mtr_nhits = mtr_nrst.hits.hits;
	def mtrn_tot = mtr_nrst.hits.total;

	log.info("%s", mtr_nrst);
	log.info("%s", mtrn_tot);

	if(mtrn_tot>0){
		log.info("Exist");
		return true;
	}else{
		log.info("Not exist");
		return false;
	}

}


def checkMtr(def mtr_id)
{
	
	def cex_nty = check_exist_notify(mtr_id);
	// start
	if(cex_nty==false){
		def cdate = new Date();
		def ctime = cdate.getTime();
		def jsonSlurper = new JsonSlurper(type: JsonParserType.INDEX_OVERLAY);
		//get date for the reminder
		def qstrBcheck = '{  "query": { "match":{"meter_id":'+mtr_id+'} },  "size" : 1,  "from": 0,  "sort": { "createdtime" : {"order" : "desc"} } }';
		def resultset = record.search(350416,null, qstrBcheck);
		def resultHits = resultset.hits.hits;
		def resultRow = resultHits.get(0);
		def strSrc = resultRow._source;

		//get bcheck hours to check

		def rowDate = jsonSlurper.parseText(strSrc.date);
		def checkDate = rowDate.toLong()*1000;
		def getupdatedDate = jsonSlurper.parseText(strSrc.updatedtime);
		def updatedDate = getupdatedDate.toLong()*1000;
		log.info("updated time %s",updatedDate);
		log.info("updated date %s",new Date( updatedDate ));

		def alertDate = new Date( checkDate )-3;
		log.info("Alert Date %s", alertDate);
		log.info("Bcheck Hrs %s", strSrc.hours);
		def alertHrs = strSrc.hours;


		//get date for the reminder
		def qstrHrs = '{ "query": { "bool": { "must": [ { "range": { "last_on_time": { "gte": "'+updatedDate+'", "lte": "now", "boost": 2.0 } } }, { "match": { "device_id": '+ mtr_id +' } } ] } }, "aggs": { "runtime": { "terms": { "script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay" }, "aggs": { "tops": { "top_hits": { "size": 100000 } } } } } }';
		//log.info("Query  %s",qstrHrs);
		def Hrsresultset = record.search(350409,null, qstrHrs);
		//log.info("Data %s",Hrsresultset);
		def HrsresultHits = Hrsresultset.hits.hits;
		def bucket = Hrsresultset.aggregations.runtime.buckets;
		//log.info("Data %s", bucket.key*.toInteger().sum());
		def runningHrs = bucket.key*.toInteger().sum();
		log.info("Running Hrs %s",timeConversion(runningHrs));
		def dg_runningHrs = timeConversion(runningHrs);


		//get user details to send email

		def userqry='{ "query": { "match_all":{} }, "size" : 1000, "from": 0, "sort": { "createdtime" : {"order" : "desc"} } }';
		def userresultset = record.search(350402,null,userqry);
		def userHits = userresultset.hits.hits;


		if(cdate>=alertDate || dg_runningHrs>=alertHrs){
			
			add_notify(mtr_id);
			
			def eparams = [__TITLE__:'Alert reminder for Bcheck'];
			def renderTemplate = template.merge("pms-bcheck", eparams);
			userHits.eachWithIndex { item, index ->
				//println item._source.ph_no
				def phone="+91"+item._source.ph_no;
				log.info("Phone %s", phone);
				//email.htmlSend('Alert reminder for Bcheck', renderTemplate, item._source.email);
			   // sms.send(renderTemplate, item._source.ph_no);
			   //sms.send("Your DG has almost reached for B-check time, Please inform the service team to check it", "+919042404780");
			}
			
		}else{
			 log.info("Result %s", 'false');
		}
		
	} else {
		log.info("Already sent the notification");
	}
	// end

}


def add_notify(mtr_id){
	def cdate = new Date();
	def ctime = cdate.getTime();
	def d = cdate;
	def d1 = d.format("d-M-Y");
	def nfields = [meter_id: mtr_id, notify_date: d1, createdtime:ctime ];
	def nrec_id = record.insert(350418, nfields);
	log.info("%s", nrec_id);

}


// Meters List

def mtrqry = '{  "query": { "match_all":{} },  "size" : 1000,  "from": 0,  "sort": { "createdtime" : {"order" : "asc"} } }';
def mtr_resultset = record.search(350414,null, mtrqry);
def mtrrstHits = mtr_resultset.hits.hits;

mtrrstHits.eachWithIndex { mtrItem, index ->
	def mtr_src = mtrItem._source;
	def cmtr_id = mtr_src.meter_id;
	log.info("%s", cmtr_id);
	checkMtr(cmtr_id);
}

