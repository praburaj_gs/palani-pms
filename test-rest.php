<?php
define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

require FCPATH.'application/third_party/boodskap/vendor/autoload.php';

$fname = $_SERVER['DOCUMENT_ROOT'].'/test/guzzle-example/json/pms.json';
try {
$client = new \GuzzleHttp\Client();

// Provide an fopen resource.
$body = fopen($fname, 'r');
$r = $client->request('POST', 'http://4blabs.com/fb-rest-api/notification/send', ['body' => $body ,
'headers' => ['X-API-KEY' => 'A08161820842103866BH']
]);

echo $r->getBody();

echo "<pre>";
print_r($r);
echo "</pre>";
} catch (GuzzleHttp\Exception\BadResponseException $e) {
	#guzzle repose for future use
	$response = $e->getResponse();
	$responseBodyAsString = $response->getBody()->getContents();
	echo "<pre>";
	print_r($responseBodyAsString);
	echo "</pre>";
}
